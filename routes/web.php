<?php

use Illuminate\Support\Facades\Route;


Route::group(['prefix' => 'admin', 'namespace' => 'admin', 'middleware' => 'admin'], function () {

    Route::get('/dashboard', 'DashboardController@index')->name('admin.dashboard');

    // START::slider
    Route::get('/slider', 'SliderController@index')->name('admin.slider');

    Route::get('/slider/add', 'SliderController@addSlider')->name('admin.slider.add');
    Route::post('/slider/add', 'SliderController@createSlider')->name('admin.slider.add.post');

    Route::get('/slider/edit/{Slider_id}', 'SliderController@editSlider')->name('admin.slider.edit');
    Route::post('/slider/edit/{Slider_id}', 'SliderController@updateSlider')->name('admin.slider.edit.post');

    Route::get('/slider/delete/{Slider_id}', 'SliderController@deleteSlider')->name('admin.slider.delete.post');
    // END::slider
    // START::WhoWeAre Desktop
    Route::get('/whoWeAre_desktop', 'WhoWeAreDeskController@index')->name('admin.whoWeAreDesk');

    Route::get('/whoWeAre_desktop/add', 'WhoWeAreDeskController@add')->name('admin.whoWeAreDesk.add');
    Route::post('/whoWeAre_desktop/add', 'WhoWeAreDeskController@create')->name('admin.whoWeAreDesk.add.post');

    Route::get('/whoWeAre_desktop/edit/{WhoWeAre_id}', 'WhoWeAreDeskController@edit')->name('admin.whoWeAreDesk.edit');
    Route::post('/whoWeAre_desktop/edit/{WhoWeAre_id}', 'WhoWeAreDeskController@update')->name('admin.whoWeAreDesk.edit.post');

    Route::get('/whoWeAre_desktop/delete/{WhoWeAre_id}', 'WhoWeAreDeskController@delete')->name('admin.whoWeAreDesk.delete.post');

    // END::WhoWeAre Desktop

    // START::university
    Route::get('/university', 'UniversityController@index')->name('admin.university');

    Route::get('/university/add', 'UniversityController@add')->name('admin.university.add');
    Route::post('/university/add', 'UniversityController@create')->name('admin.university.add.post');

    Route::get('/university/edit/{University_id}', 'UniversityController@edit')->name('admin.university.edit');
    Route::post('/university/edit/{University_id}', 'UniversityController@update')->name('admin.university.edit.post');

    Route::get('/university/delete/{University_id}', 'UniversityController@delete')->name('admin.university.delete.post');
    // END::university

    // START::blog
    Route::get('/blog', 'BlogController@index')->name('admin.blog');

    Route::get('/blog/add', 'BlogController@add')->name('admin.blog.add');
    Route::post('/blog/add', 'BlogController@create')->name('admin.blog.add.post');

    Route::get('/blog/edit/{Blog_id}', 'BlogController@edit')->name('admin.blog.edit');
    Route::post('/blog/edit/{Blog_id}', 'BlogController@update')->name('admin.blog.edit.post');

    Route::get('/blog/delete/{Blog_id}', 'BlogController@delete')->name('admin.blog.delete.post');

    Route::get('/blog/search', 'BlogController@search');

    // END::blog

 // START::program
 Route::get('/program', 'ProgramController@index')->name('admin.program');

 Route::get('/program/add', 'ProgramController@add')->name('admin.program.add');
 Route::post('/program/add', 'ProgramController@create')->name('admin.program.add.post');

 Route::get('/program/edit/{Program_id}', 'ProgramController@edit')->name('admin.program.edit');
 Route::post('/program/edit/{Program_id}', 'ProgramController@update')->name('admin.program.edit.post');

 Route::get('/program/delete/{Program_id}', 'ProgramController@delete')->name('admin.program.delete.post');

 Route::get('/program/search', 'ProgramController@search');

 // END::program


    // START::WhoWeAre
    Route::get('/whoWeAre', 'aboutUs\WhoWeAreController@index')->name('admin.whoWeAre');

    Route::get('/whoWeAre/add', 'aboutUs\WhoWeAreController@add')->name('admin.whoWeAre.add');
    Route::post('/whoWeAre/add', 'aboutUs\WhoWeAreController@create')->name('admin.whoWeAre.add.post');

    Route::get('/whoWeAre/edit/{WhoWeAre_id}', 'aboutUs\WhoWeAreController@edit')->name('admin.whoWeAre.edit');
    Route::post('/whoWeAre/edit/{WhoWeAre_id}', 'aboutUs\WhoWeAreController@update')->name('admin.whoWeAre.edit.post');

    Route::get('/whoWeAre/delete/{WhoWeAre_id}', 'aboutUs\WhoWeAreController@delete')->name('admin.whoWeAre.delete.post');
    // END::WhoWeAre


    // START::visionAndMission
    Route::get('/visionAndMission', 'aboutUs\VisionAndMissionController@index')->name('admin.visionAndMission');

    Route::get('/visionAndMission/add', 'aboutUs\VisionAndMissionController@add')->name('admin.visionAndMission.add');
    Route::post('/visionAndMission/add', 'aboutUs\VisionAndMissionController@create')->name('admin.visionAndMission.add.post');

    Route::get('/visionAndMission/edit/{VisionAndMission_id}', 'aboutUs\VisionAndMissionController@edit')->name('admin.visionAndMission.edit');
    Route::post('/visionAndMission/edit/{VisionAndMission_id}', 'aboutUs\VisionAndMissionController@update')->name('admin.visionAndMission.edit.post');

    Route::get('/visionAndMission/delete/{VisionAndMission_id}', 'aboutUs\VisionAndMissionController@delete')->name('admin.visionAndMission.delete.post');
    // END::visionAndMission


    // START::howWeCanHelp
    Route::get('/howWeCanHelp', 'aboutUs\HowWeCanHelpController@index')->name('admin.howWeCanHelp');

    Route::get('/howWeCanHelp/add', 'aboutUs\HowWeCanHelpController@add')->name('admin.howWeCanHelp.add');
    Route::post('/howWeCanHelp/add', 'aboutUs\HowWeCanHelpController@create')->name('admin.howWeCanHelp.add.post');

    Route::get('/howWeCanHelp/edit/{HowWeCanHelp_id}', 'aboutUs\HowWeCanHelpController@edit')->name('admin.howWeCanHelp.edit');
    Route::post('/howWeCanHelp/edit/{HowWeCanHelp_id}', 'aboutUs\HowWeCanHelpController@update')->name('admin.howWeCanHelp.edit.post');

    Route::get('/howWeCanHelp/delete/{HowWeCanHelp_id}', 'aboutUs\HowWeCanHelpController@delete')->name('admin.howWeCanHelp.delete.post');
    // END::howWeCanHelp


    // START::contactUs
    Route::get('/contactUs', 'aboutUs\ContactUsController@index')->name('admin.ContactUs');

    Route::get('/contactUs/delete/{contactUs_id}', 'aboutUs\ContactUsController@delete')->name('admin.ContactUs.delete.post');

    Route::get('/details/contactUs', 'aboutUs\ContactUsController@details')->name('admin.ContactUs.details');

    // END::contactUs

    // START::howWeCanHelp
    Route::get('/newsLetter', 'NewsletterController@index')->name('admin.Newsletter');

    Route::get('/newsLetter/delete/{newsLetter_id}', 'NewsletterController@delete')->name('admin.Newsletter.delete');

    // END::howWeCanHelp


    // START::testimonial
    Route::get('/testimonial', 'TestimonialsController@index')->name('admin.testimonial');

    Route::get('/testimonial/add', 'TestimonialsController@add')->name('admin.testimonial.add');
    Route::post('/testimonial/add', 'TestimonialsController@create')->name('admin.testimonial.add.post');

    Route::get('/testimonial/edit/{Testimonial_id}', 'TestimonialsController@edit')->name('admin.testimonial.edit');
    Route::post('/testimonial/edit/{Testimonial_id}', 'TestimonialsController@update')->name('admin.testimonial.edit.post');

    Route::get('/testimonial/delete/{Testimonial_id}', 'TestimonialsController@delete')->name('admin.testimonial.delete.post');
    // END::testimonial


    // START::admisstionProcess
    Route::get('/admisstionProcess', 'AdmisstionProcessController@index')->name('admin.admisstionProcess');

    Route::get('/admisstionProcess/add', 'AdmisstionProcessController@add')->name('admin.admisstionProcess.add');
    Route::post('/admisstionProcess/add', 'AdmisstionProcessController@create')->name('admin.admisstionProcess.add.post');

    Route::get('/admisstionProcess/edit/{AdmisstionProcess_id}', 'AdmisstionProcessController@edit')->name('admin.admisstionProcess.edit');
    Route::post('/admisstionProcess/edit/{AdmisstionProcess_id}', 'AdmisstionProcessController@update')->name('admin.admisstionProcess.edit.post');

    Route::get('/admisstionProcess/delete/{AdmisstionProcess_id}', 'AdmisstionProcessController@delete')->name('admin.admisstionProcess.delete.post');
    // END::admisstionProcess


    Route::group(['prefix' => 'livingIran', 'namespace' => 'livingIran'], function () {

        // START::aboutIran
        Route::get('/aboutIran', 'AboutIranController@index')->name('admin.aboutIran');

        Route::get('/aboutIran/add', 'AboutIranController@add')->name('admin.aboutIran.add');
        Route::post('/aboutIran/add', 'AboutIranController@create')->name('admin.aboutIran.add.post');

        Route::get('/aboutIran/edit/{AboutIran_id}', 'AboutIranController@edit')->name('admin.aboutIran.edit');
        Route::post('/aboutIran/edit/{AboutIran_id}', 'AboutIranController@update')->name('admin.aboutIran.edit.post');

        Route::get('/aboutIran/delete/{AboutIran_id}', 'AboutIranController@delete')->name('admin.aboutIran.delete.post');
        // END::aboutIran


        // START::tourismIran
        Route::get('/tourismIran', 'TourismIranController@index')->name('admin.tourismIran');

        Route::get('/tourismIran/add', 'TourismIranController@add')->name('admin.tourismIran.add');
        Route::post('/tourismIran/add', 'TourismIranController@create')->name('admin.tourismIran.add.post');

        Route::get('/tourismIran/edit/{TourismIran_id}', 'TourismIranController@edit')->name('admin.tourismIran.edit');
        Route::post('/tourismIran/edit/{TourismIran_id}', 'TourismIranController@update')->name('admin.tourismIran.edit.post');

        Route::get('/tourismIran/delete/{TourismIran_id}', 'TourismIranController@delete')->name('admin.tourismIran.delete.post');
        // END::tourismIran


        // START::culture
        Route::get('/culture', 'CultureController@index')->name('admin.culture');

        Route::get('/culture/add', 'CultureController@add')->name('admin.culture.add');
        Route::post('/culture/add', 'CultureController@create')->name('admin.culture.add.post');

        Route::get('/culture/edit/{Culture_id}', 'CultureController@edit')->name('admin.culture.edit');
        Route::post('/culture/edit/{Culture_id}', 'CultureController@update')->name('admin.culture.edit.post');

        Route::get('/culture/delete/{Culture_id}', 'CultureController@delete')->name('admin.culture.delete.post');
        // END::culture

    });
    Route::group(['prefix' => 'studyingIran', 'namespace' => 'studyingIran'], function () {

        // START::admisstionRequirments
        Route::get('/admisstionRequirments', 'admisstionRequirmentsController@index')->name('admin.admisstionRequirments');

        Route::get('/admisstionRequirments/add', 'admisstionRequirmentsController@add')->name('admin.admisstionRequirments.add');
        Route::post('/admisstionRequirments/add', 'admisstionRequirmentsController@create')->name('admin.admisstionRequirments.add.post');

        Route::get('/admisstionRequirments/edit/{AdmisstionRequirments_id}', 'admisstionRequirmentsController@edit')->name('admin.admisstionRequirments.edit');
        Route::post('/admisstionRequirments/edit/{AdmisstionRequirments_id}', 'admisstionRequirmentsController@update')->name('admin.admisstionRequirments.edit.post');

        Route::get('/admisstionRequirments/delete/{AdmisstionRequirments_id}', 'admisstionRequirmentsController@delete')->name('admin.admisstionRequirments.delete.post');
        // END::admisstionRequirments

        // START::educationSystem
        Route::get('/educationSystem', 'educationSystemController@index')->name('admin.educationSystem');

        Route::get('/educationSystem/add', 'educationSystemController@add')->name('admin.educationSystem.add');
        Route::post('/educationSystem/add', 'educationSystemController@create')->name('admin.educationSystem.add.post');

        Route::get('/educationSystem/edit/{EducationSystem_id}', 'educationSystemController@edit')->name('admin.educationSystem.edit');
        Route::post('/educationSystem/edit/{EducationSystem_id}', 'educationSystemController@update')->name('admin.educationSystem.edit.post');

        Route::get('/educationSystem/delete/{EducationSystem_id}', 'educationSystemController@delete')->name('admin.educationSystem.delete.post');
        // END::educationSystem

        // START::studyingIran
        Route::get('/studyingIran', 'StudyingIranController@index')->name('admin.studyingIran');

        Route::get('/studyingIran/add', 'StudyingIranController@add')->name('admin.studyingIran.add');
        Route::post('/studyingIran/add', 'StudyingIranController@create')->name('admin.studyingIran.add.post');

        Route::get('/studyingIran/edit/{studyingIran_id}', 'StudyingIranController@edit')->name('admin.studyingIran.edit');
        Route::post('/studyingIran/edit/{studyingIran_id}', 'StudyingIranController@update')->name('admin.studyingIran.edit.post');

        Route::get('/studyingIran/delete/{studyingIran_id}', 'StudyingIranController@delete')->name('admin.studyingIran.delete.post');
        // End::studyingIran

        // START::whyStudying
        
        Route::get('/whyStudying', 'whyStudyingController@index')->name('admin.whyStudying');

        Route::get('/whyStudying/add', 'whyStudyingController@add')->name('admin.whyStudying.add');
        Route::post('/whyStudying/add', 'whyStudyingController@create')->name('admin.whyStudying.add.post');

        Route::get('/whyStudying/edit/{WhyStudying_id}', 'whyStudyingController@edit')->name('admin.whyStudying.edit');
        Route::post('/whyStudying/edit/{WhyStudying_id}', 'whyStudyingController@update')->name('admin.whyStudying.edit.post');

        Route::get('/whyStudying/delete/{WhyStudying_id}', 'whyStudyingController@delete')->name('admin.whyStudying.delete.post');
        // END::whyStudying
    });

    // START::services
    Route::get('/service', 'ServicesController@index')->name('admin.service');

    Route::get('/service/add', 'ServicesController@add')->name('admin.service.add');
    Route::post('/service/add', 'ServicesController@create')->name('admin.service.add.post');

    Route::get('/service/edit/{Service_id}', 'ServicesController@edit')->name('admin.service.edit');
    Route::post('/service/edit/{Service_id}', 'ServicesController@update')->name('admin.service.edit.post');

    Route::get('/service/delete/{Service_id}', 'ServicesController@delete')->name('admin.service.delete.post');


    // END::services

    //START:: About us
    Route::get('/about/Details/{AboutDetail_id}', 'aboutDetailsController@index')->name('admin.aboutDetails');
    Route::post('/about/Details/{AboutDetail_id}', 'aboutDetailsController@create')->name('admin.aboutDetails.post');


    // END::About us
});


Route::group(['namespace' => 'home'], function () {


    Route::post('/newsletter', 'NewsletterController@newsletter')->name('home.newsletter');

    Route::get('/services/{service_id}', 'ServicesController@singleService')->name('home.singleService');


    Route::get('/', 'IndexController@index')->name('home.index');
    Route::get('/about', 'AboutUsController@index')->name('home.about');


    Route::get('/howWeCanHelp', 'HelpController@index')->name('home.howWeHelpYou');
    Route::get('/whoWeAre', 'WhoWeAreController@index')->name('home.whoWeAre');
    Route::get('/visionAndMision', 'VAndMController@index')->name('home.visionAndMision');


    Route::get('/contact', 'ContactUsController@index')->name('home.contact');
    Route::post('/contact', 'ContactUsController@createContactMember');

    Route::get('/blog', 'BlogController@index')->name('home.Blog');
    Route::get('/blog/{blog_title}', 'BlogController@singleBlog')->name('home.singleBlog');


    Route::get('/programs/{Program_title}', 'ProgramController@detail')->name('home.singleProgram');
    Route::get('/programs', 'ProgramController@index')->name('home.program');

    Route::get('/testimonials', 'TestimonialsController@index')->name('home.testimonials');
    Route::get('/student/{Testimonial_title}', 'TestimonialsController@student')->name('home.student');


    Route::group(['prefix' => 'livingIran', 'namespace' => 'livingIran'], function () {
        Route::get('/aboutiran', 'AboutIranController@index')->name('home.aboutiran');
        Route::get('/tourism', 'TourismIranController@index')->name('home.tourismiran');
        Route::get('/culture', 'CultureController@index')->name('home.culture');
    });


    Route::get('/University_details{University_title}', 'UniversityController@detail')->name('home.singleUniversity');
    Route::get('/university', 'UniversityController@index')->name('home.University');

    Route::get('/admissionProcess', 'AdmissionController@index')->name('home.admisstionProcess');

    Route::group(['prefix' => 'studyingIran', 'namespace' => 'studyingIran'], function () {
        Route::get('/AdmisstionRequirments', 'AdmisstionRequirmentsController@index')->name('home.AdmisstionRequirments');
        Route::get('/EducationSystem', 'EducationSystemController@index')->name('home.EducationSystem');
        Route::get('/WhyStudyinginIran', 'WhyStudyingController@index')->name('home.WhyStudying');

    });


});
//Start::login
Route::group(['namespace' => 'Auth'], function () {
    Route::get('/login', 'LoginController@index')->name('loginPage');
    Route::post('/dologin', 'LoginController@doLogin')->name('doLogin');

    Route::get('/logout', 'LoginController@logout')->name('logout');
});
 //End::login

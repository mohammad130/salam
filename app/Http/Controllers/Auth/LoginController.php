<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\User;
use Illuminate\Support\Facades\Auth;

class LoginController extends Controller
{
    public function index()
    {
        return view('Auth.layout.main');
    }
    public function doLogin(Request $request)
    {
        $remember = $request->has('remember-me');
        if (Auth::attempt(['name' => $request->input('username'),'password'=> $request->input('password')],$remember)){
            return redirect('/admin/dashboard');
        }
        return redirect()->back()->with('message', 'loginError()');

    }

    public function logout()
    {
        Auth::logout();
        return redirect('/login');
    }
}

<?php

namespace App\Http\Controllers\admin;

use App\Http\Controllers\Controller;

use App\Models\Blog;
use Illuminate\Http\Request;
use Symfony\Component\HttpFoundation\File\File;

class BlogController extends Controller
{
    public function index()
    {
        $blogs= Blog::paginate(10);
        return view('admin.site.blog.all',compact('blogs'));
    }

    public function add()
    {
        return view('admin.site.blog.add');
    }

    public function create(Request $request)
    {
        $Create_Blog = [
            'title' => request()->input('title'),
            'shortDescription' => request()->input('shortDescription'),
            'description' => request()->input('description'),
        ];
        $imageinput = request()->file('imageBlog');
        if ($imageinput!="") {
            $new_image_about_name = request()->file('imageBlog')->getClientOriginalName();
            $result = request()->file('imageBlog')->move(public_path('Management/images/Blog'),$new_image_about_name);
        }
        if ($imageinput!="" && $result instanceof File){
            $Create_Blog['image'] ="/Management/images/Blog/".request()->file('imageBlog')->getClientOriginalName();
            $new_Blog_object = Blog::create($Create_Blog);
            if ($new_Blog_object) {
                return redirect()->route('admin.blog')->with('message', 'successAdd()');
            }
        }
        else{
            $new_Blog_object = Blog::create($Create_Blog);
            if ($new_Blog_object) {
                return redirect()->route('admin.blog')->with('message', 'successAdd()');
            }
        }
    }
    public function edit($Blog_id)
    {
        if ($Blog_id && ctype_digit($Blog_id)) {
            $BlogItem = Blog::find($Blog_id);
            if ($BlogItem && $BlogItem instanceof Blog) {
                return view('admin.site.blog.edit',compact('BlogItem'));
            }
        }
    }

    public function update($Blog_id)
    {
        $blog_data = [
            'title' => request()->input('title'),
            'shortDescription' => request()->input('shortDescription'),
            'description' => request()->input('description'),
        ];
        $imageinput = request()->file('imageBlog');
        if ($imageinput!="") {
            $new_image_blog_name = request()->file('imageBlog')->getClientOriginalName();
            $resultUZ = request()->file('imageBlog')->move(public_path('Management/images/Blog'),$new_image_blog_name);
        }
        if ($imageinput!="" && $resultUZ instanceof File){
            $blog_data['image'] ="/Management/images/Blog/".request()->file('imageBlog')->getClientOriginalName();
            $blog = Blog::find($Blog_id);
            $blog->update($blog_data);
            if ($blog) {
                return redirect()->route('admin.blog')->with('message', 'successEdit()');
            }
        }
        else{
            $blog = Blog::find($Blog_id);
            $blog->update($blog_data);
            if ($blog) {
                return redirect()->route('admin.blog')->with('message', 'successEdit()');
            }
        }
    }

    public function delete(Request $request)
    {
        $blog = Blog::find($request->Blog_id);
        $blog->delete();
        return back()->with('message', 'successDelete()');
    }
    public function search(){
        $searchValue = $_GET["searchInput"];
        $currentValue = Blog::where('title', $searchValue)->get();
        return $currentValue;
    }

}

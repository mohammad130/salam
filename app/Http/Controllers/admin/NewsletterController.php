<?php

namespace App\Http\Controllers\admin;

use App\Http\Controllers\Controller;
use App\Models\Newsletter;
use Illuminate\Http\Request;

class NewsletterController extends Controller
{
    public function index()
    {
        $newsletterInfs= Newsletter::all();
        return view('admin.site.newsLetter.all',compact('newsletterInfs'));
    }

    public function delete(Request $request)
    {
        $contactUs = Newsletter::find($request->newsLetter_id);
        $contactUs->delete();
        return back();
    }

}

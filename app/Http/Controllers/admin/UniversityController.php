<?php

namespace App\Http\Controllers\admin;

use App\Http\Controllers\Controller;
use App\Models\University;
use Illuminate\Http\Request;
use Symfony\Component\HttpFoundation\File\File;

class UniversityController extends Controller
{
    public function index()
    {
        $universities= University::all();
        return view('admin.site.university.all',compact('universities'));
    }
    public function add()
    {
        return view('admin.site.university.add');
    }

    public function create(Request $request)
    {
        $Create_University = [
            'title' => request()->input('title'),
            'shortDescription' => request()->input('shortDescription'),
            'description' => request()->input('description'),
            'video_link' => request()->input('video_link'),
        ];
        $imageinput = request()->file('imageInp');
        if ($imageinput!="") {
            $new_image_about_name = request()->file('imageInp')->getClientOriginalName();
            $result = request()->file('imageInp')->move(public_path('Management/images/University'),$new_image_about_name);
        }
        if ($imageinput!="" && $result instanceof File){
            $Create_University['image'] ="/Management/images/University/".request()->file('imageInp')->getClientOriginalName();
            $new_University_object = University::create($Create_University);
            if ($new_University_object) {
                return redirect()->route('admin.university')->with('message', 'successAdd()');
            }
        }
        else{
            $new_University_object = University::create($Create_University);
            if ($new_University_object) {
                return redirect()->route('admin.university')->with('message', 'successAdd()');
            }
        }
    }
    public function edit($University_id)
    {
        if ($University_id && ctype_digit($University_id)) {
            $UniversityItem = University::find($University_id);
            if ($UniversityItem && $UniversityItem instanceof University) {
                return view('admin.site.university.edit',compact('UniversityItem'));
            }
        }
    }

    public function update($University_id)
    {
        $university_data = [
            'title' => request()->input('title'),
            'shortDescription' => request()->input('shortDescription'),
            'description' => request()->input('description'),
            'video_link' => request()->input('video_link'),
        ];
        $imageinput = request()->file('imageInp');
        if ($imageinput!="") {
            $new_image_university_name = request()->file('imageInp')->getClientOriginalName();
            $resultUZ = request()->file('imageInp')->move(public_path('Management/images/University'),$new_image_university_name);
        }
        if ($imageinput!="" && $resultUZ instanceof File){
            $university_data['image'] ="/Management/images/University/".request()->file('imageInp')->getClientOriginalName();
            $university = University::find($University_id);
            $university->update($university_data);
            if ($university) {
                return redirect()->route('admin.university')->with('message', 'successEdit()');
            }
        }
        else{
            $university = University::find($University_id);
            $university->update($university_data);
            if ($university) {
                return redirect()->route('admin.university')->with('message', 'successEdit()');
            }
        }
    }

    public function delete(Request $request)
    {
        $university = University::find($request->University_id);
        $university->delete();
        return back()->with('message', 'successDelete()');
    }

}

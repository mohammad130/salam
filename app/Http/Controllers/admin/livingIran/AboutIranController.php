<?php

namespace App\Http\Controllers\admin\livingIran;

use App\Http\Controllers\Controller;

use App\Models\AboutIran;
use Illuminate\Http\Request;
use Symfony\Component\HttpFoundation\File\File;

class AboutIranController extends Controller
{
    public function index()
    {
        $AboutIranInfs= AboutIran::all();
        return view('admin.site.livingIran.aboutIran.all',compact('AboutIranInfs'));
    }

    public function add()
    {
        return view('admin.site.livingIran.aboutIran.add');
    }

    public function create(Request $request)
    {
        $Create_AboutIranInf = [
            'title' => request()->input('title'),
            'shortDescription' => request()->input('shortDescription'),
            'description' => request()->input('description'),
            'video_link' => request()->input('video_link'),
        ];
        $imageinput = request()->file('imageInp');
        if ($imageinput!="") {
            $new_image_about_name = request()->file('imageInp')->getClientOriginalName();
            $result = request()->file('imageInp')->move(public_path('Management/images/livingIran/AboutIran'),$new_image_about_name);
        }
        if ($imageinput!="" && $result instanceof File){
            $Create_AboutIranInf['image'] ="/Management/images/livingIran/AboutIran/".request()->file('imageInp')->getClientOriginalName();
            $new_object = AboutIran::create($Create_AboutIranInf);
            if ($new_object) {
                return redirect()->route('admin.aboutIran')->with('message', 'successAdd()');
            }
        }
        else{
            $new_object = AboutIran::create($Create_AboutIranInf);
            if ($new_object) {
                return redirect()->route('admin.aboutIran')->with('message', 'successAdd()');
            }
        }
    }
    public function edit($aboutIran_id)
    {
        if ($aboutIran_id && ctype_digit($aboutIran_id)) {
            $aboutIranItem = AboutIran::find($aboutIran_id);
            if ($aboutIranItem && $aboutIranItem instanceof AboutIran) {
                return view('admin.site.livingIran.aboutIran.edit',compact('aboutIranItem'));
            }
        }
    }

    public function update($aboutIran_id)
    {
        $aboutIran_data = [
            'title' => request()->input('title'),
            'shortDescription' => request()->input('shortDescription'),
            'description' => request()->input('description'),
            'video_link' => request()->input('video_link'),
        ];
        $imageinput = request()->file('imageInp');
        if ($imageinput!="") {
            $new_image_name = request()->file('imageInp')->getClientOriginalName();
            $resultUZ = request()->file('imageInp')->move(public_path('Management/images/livingIran/AboutIran'),$new_image_name);
        }
        if ($imageinput!="" && $resultUZ instanceof File){
            $aboutIran_data['image'] ="/Management/images/livingIran/AboutIran/".request()->file('imageInp')->getClientOriginalName();
            $aboutIran = AboutIran::find($aboutIran_id);
            $aboutIran->update($aboutIran_data);
            if ($aboutIran) {
                return redirect()->route('admin.aboutIran')->with('message', 'successEdit()');
            }
        }
        else{
            $aboutIran = AboutIran::find($aboutIran_id);
            $aboutIran->update($aboutIran_data);
            if ($aboutIran) {
                return redirect()->route('admin.aboutIran')->with('message', 'successEdit()');
            }
        }
    }

    public function delete(Request $request)
    {
        $aboutIran = AboutIran::find($request->AboutIran_id);
        $aboutIran->delete();
        return back()->with('message', 'successDelete()');
    }
}

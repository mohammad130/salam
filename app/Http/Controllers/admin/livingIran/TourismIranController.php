<?php

namespace App\Http\Controllers\admin\livingIran;

use App\Http\Controllers\Controller;
use App\Models\TourismIran;
use Illuminate\Http\Request;
use Symfony\Component\HttpFoundation\File\File;

class TourismIranController extends Controller
{
    public function index()
    {
        $TourismIranInfs= TourismIran::all();
        return view('admin.site.livingIran.tourismIran.all',compact('TourismIranInfs'));
    }

    public function add()
    {
        return view('admin.site.livingIran.tourismIran.add');
    }

    public function create(Request $request)
    {
        $Create_TourismIranInf = [
            'title' => request()->input('title'),
            'shortDescription' => request()->input('shortDescription'),
            'description' => request()->input('description'),
        ];
        $imageinput = request()->file('imageInp');
        if ($imageinput!="") {
            $new_image_about_name = request()->file('imageInp')->getClientOriginalName();
            $result = request()->file('imageInp')->move(public_path('Management/images/livingIran/TourismIran'),$new_image_about_name);
        }
        if ($imageinput!="" && $result instanceof File){
            $Create_TourismIranInf['image'] ="/Management/images/livingIran/TourismIran/".request()->file('imageInp')->getClientOriginalName();
            $new_object = TourismIran::create($Create_TourismIranInf);
            if ($new_object) {
                return redirect()->route('admin.tourismIran')->with('message', 'successAdd()');
            }
        }
        else{
            $new_object = TourismIran::create($Create_TourismIranInf);
            if ($new_object) {
                return redirect()->route('admin.tourismIran')->with('message', 'successAdd()');
            }
        }
    }
    public function edit($tourismIran_id)
    {
        if ($tourismIran_id && ctype_digit($tourismIran_id)) {
            $tourismIranItem = TourismIran::find($tourismIran_id);
            if ($tourismIranItem && $tourismIranItem instanceof TourismIran) {
                return view('admin.site.livingIran.tourismIran.edit',compact('tourismIranItem'));
            }
        }
    }

    public function update($tourismIran_id)
    {
        $tourismIran_data = [
            'title' => request()->input('title'),
            'shortDescription' => request()->input('shortDescription'),
            'description' => request()->input('description'),
        ];
        $imageinput = request()->file('imageInp');
        if ($imageinput!="") {
            $new_image_name = request()->file('imageInp')->getClientOriginalName();
            $resultUZ = request()->file('imageInp')->move(public_path('Management/images/livingIran/TourismIran'),$new_image_name);
        }
        if ($imageinput!="" && $resultUZ instanceof File){
            $tourismIran_data['image'] ="/Management/images/livingIran/TourismIran/".request()->file('imageInp')->getClientOriginalName();
            $tourismIran = TourismIran::find($tourismIran_id);
            $tourismIran->update($tourismIran_data);
            if ($tourismIran) {
                return redirect()->route('admin.tourismIran')->with('message', 'successEdit()');
            }
        }
        else{
            $tourismIran = TourismIran::find($tourismIran_id);
            $tourismIran->update($tourismIran_data);
            if ($tourismIran) {
                return redirect()->route('admin.tourismIran')->with('message', 'successEdit()');
            }
        }
    }

    public function delete(Request $request)
    {
        $tourismIran = TourismIran::find($request->TourismIran_id);
        $tourismIran->delete();
        return back()->with('message', 'successDelete()');
    }
}

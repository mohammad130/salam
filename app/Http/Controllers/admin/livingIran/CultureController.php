<?php

namespace App\Http\Controllers\admin\livingIran;

use App\Http\Controllers\Controller;
use App\Models\Culture;
use Illuminate\Http\Request;
use Symfony\Component\HttpFoundation\File\File;

class CultureController extends Controller
{
    public function index()
    {
        $cultureInfs= Culture::all();
        return view('admin.site.livingIran.culture.all',compact('cultureInfs'));
    }

    public function add()
    {
        return view('admin.site.livingIran.culture.add');
    }

    public function create(Request $request)
    {
        $Create_CultureInf = [
            'title' => request()->input('title'),
            'shortDescription' => request()->input('shortDescription'),
            'description' => request()->input('description'),
        ];
        $imageinput = request()->file('imageInp');
        if ($imageinput!="") {
            $new_image_about_name = request()->file('imageInp')->getClientOriginalName();
            $result = request()->file('imageInp')->move(public_path('Management/images/livingIran/Culture'),$new_image_about_name);
        }
        if ($imageinput!="" && $result instanceof File){
            $Create_CultureInf['image'] ="/Management/images/livingIran/Culture/".request()->file('imageInp')->getClientOriginalName();
            $new_object = Culture::create($Create_CultureInf);
            if ($new_object) {
                return redirect()->route('admin.culture')->with('message', 'successAdd()');
            }
        }
        else{
            $new_object = Culture::create($Create_CultureInf);
            if ($new_object) {
                return redirect()->route('admin.culture')->with('message', 'successAdd()');
            }
        }
    }
    public function edit($culture_id)
    {
        if ($culture_id && ctype_digit($culture_id)) {
            $cultureItem = Culture::find($culture_id);
            if ($cultureItem && $cultureItem instanceof Culture) {
                return view('admin.site.livingIran.culture.edit',compact('cultureItem'));
            }
        }
    }

    public function update($culture_id)
    {
        $culture_data = [
            'title' => request()->input('title'),
            'shortDescription' => request()->input('shortDescription'),
            'description' => request()->input('description'),
        ];
        $imageinput = request()->file('imageInp');
        if ($imageinput!="") {
            $new_image_name = request()->file('imageInp')->getClientOriginalName();
            $resultUZ = request()->file('imageInp')->move(public_path('Management/images/livingIran/Culture'),$new_image_name);
        }
        if ($imageinput!="" && $resultUZ instanceof File){
            $culture_data['image'] ="/Management/images/livingIran/Culture/".request()->file('imageInp')->getClientOriginalName();
            $culture = Culture::find($culture_id);
            $culture->update($culture_data);
            if ($culture) {
                return redirect()->route('admin.culture')->with('message', 'successEdit()');
            }
        }
        else{
            $culture = Culture::find($culture_id);
            $culture->update($culture_id);
            if ($culture) {
                return redirect()->route('admin.culture')->with('message', 'successEdit()');
            }
        }
    }

    public function delete(Request $request)
    {
        $culture = Culture::find($request->Culture_id);
        $culture->delete();
        return back()->with('message', 'successDelete()');
    }
}

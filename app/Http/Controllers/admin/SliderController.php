<?php

namespace App\Http\Controllers\admin;

use App\Http\Controllers\Controller;
use App\Models\Slider;
use Illuminate\Http\Request;
use Symfony\Component\HttpFoundation\File\File;

class SliderController extends Controller
{
    public function index()
    {
        $sliders= Slider::paginate(10);
        return view('admin.site.slider.all',compact('sliders'));
    }

    public function addSlider()
    {
        return view('admin.site.slider.add');
    }

    public function createSlider(Request $request)
    {
        $Create_Slider = [
            'title' => request()->input('title'),
            'shortDescription' => request()->input('shortDescription'),
            'description' => request()->input('description'),
        ];
        $imageinput = request()->file('imageslider');
        if ($imageinput!="") {
            $new_image_about_name = request()->file('imageslider')->getClientOriginalName();
            $result = request()->file('imageslider')->move(public_path('Management/images/Slider'),$new_image_about_name);
        }
        if ($imageinput!="" && $result instanceof File){
            $Create_Slider['image'] ="/Management/images/Slider/".request()->file('imageslider')->getClientOriginalName();
            $new_Slider_object = Slider::create($Create_Slider);
            if ($new_Slider_object) {
                return redirect()->route('admin.slider')->with('message', 'successAdd()');
            }
        }
        else{
            $new_Slider_object = Slider::create($Create_Slider);
            if ($new_Slider_object) {
                return redirect()->route('admin.slider')->with('message', 'successAdd()');
            }
        }
    }
    public function editSlider($Slider_id)
    {
        if ($Slider_id && ctype_digit($Slider_id)) {
            $SlideItem = Slider::find($Slider_id);
            if ($SlideItem && $SlideItem instanceof Slider) {
                return view('admin.site.slider.edit',compact('SlideItem'));
            }
        }
    }

    public function updateSlider($Slider_id)
    {
        $slider_data = [
            'title' => request()->input('title'),
            'shortDescription' => request()->input('shortDescription'),
            'description' => request()->input('description'),
        ];
        $imageinput = request()->file('imageslider');
        if ($imageinput!="") {
            $new_image_slider_name = request()->file('imageslider')->getClientOriginalName();
            $resultUZ = request()->file('imageslider')->move(public_path('Management/images/Slider'),$new_image_slider_name);
        }
        if ($imageinput!="" && $resultUZ instanceof File){
            $slider_data['image'] ="/Management/images/Slider/".request()->file('imageslider')->getClientOriginalName();
            $slide = Slider::find($Slider_id);
            $slide->update($slider_data);
            if ($slide) {
                return redirect()->route('admin.slider')->with('message', 'successEdit()');
            }
        }
        else{
            $slide = Slider::find($Slider_id);
            $slide->update($slider_data);
            if ($slide) {
                return redirect()->route('admin.slider')->with('message', 'successEdit()');
            }
        }
    }

    public function deleteSlider(Request $request)
    {
        $slide = Slider::find($request->Slider_id);
        $slide->delete();
        return back()->with('message', 'successDelete()');
    }


}

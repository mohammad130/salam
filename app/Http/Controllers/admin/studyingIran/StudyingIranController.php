<?php

namespace App\Http\Controllers\admin\studyingIran;

use App\Http\Controllers\Controller;
use App\Models\Studyingiran;
use Illuminate\Http\Request;
use Symfony\Component\HttpFoundation\File\File;

class StudyingIranController extends Controller
{
    public function index()
    {
        $studyingIranInfs= Studyingiran::all();
        return view('admin.site.studyingIran.studyingIran.all',compact('studyingIranInfs'));
    }

    public function add()
    {
        return view('admin.site.studyingIran.studyingIran.add');
    }

    public function create(Request $request)
    {
        $Create_StudyingIranInf = [
            'title' => request()->input('title'),
            'shortDescription' => request()->input('shortDescription'),
            'description' => request()->input('description'),
        ];
        $imageinput = request()->file('imageInp');
        if ($imageinput!="") {
            $new_image_about_name = request()->file('imageInp')->getClientOriginalName();
            $result = request()->file('imageInp')->move(public_path('Management/images/studyingIran/studyingIran'),$new_image_about_name);
        }
        if ($imageinput!="" && $result instanceof File){
            $Create_StudyingIranInf['image'] ="/Management/images/studyingIran/studyingIran/".request()->file('imageInp')->getClientOriginalName();
            $new_object = Studyingiran::create($Create_StudyingIranInf);
            if ($new_object) {
                return redirect()->route('admin.studyingIran')->with('message', 'successAdd()');
            }
        }
        else{
            $new_object = Studyingiran::create($Create_StudyingIranInf);
            if ($new_object) {
                return redirect()->route('admin.studyingIran')->with('message', 'successAdd()');
            }
        }
    }
    public function edit($studyingIran_id)
    {
        if ($studyingIran_id && ctype_digit($studyingIran_id)) {
            $studyingIranItem = Studyingiran::find($studyingIran_id);
            if ($studyingIranItem && $studyingIranItem instanceof Studyingiran) {
                return view('admin.site.studyingIran.studyingIran.edit',compact('studyingIranItem'));
            }
        }
    }

    public function update($studyingIran_id)
    {
        $studyingIran_data = [
            'title' => request()->input('title'),
            'shortDescription' => request()->input('shortDescription'),
            'description' => request()->input('description'),
        ];
        $imageinput = request()->file('imageInp');
        if ($imageinput!="") {
            $new_image_name = request()->file('imageInp')->getClientOriginalName();
            $resultUZ = request()->file('imageInp')->move(public_path('Management/images/studyingIran/studyingIran'),$new_image_name);
        }
        if ($imageinput!="" && $resultUZ instanceof File){
            $studyingIran_data['image'] ="/Management/images/studyingIran/studyingIran/".request()->file('imageInp')->getClientOriginalName();
            $studyingIran = Studyingiran::find($studyingIran_id);
            $studyingIran->update($studyingIran_data);
            if ($studyingIran) {
                return redirect()->route('admin.studyingIran')->with('message', 'successEdit()');
            }
        }
        else{
            $studyingIran = Studyingiran::find($studyingIran_id);
            $studyingIran->update($studyingIran_data);
            if ($studyingIran) {
                return redirect()->route('admin.studyingIran')->with('message', 'successEdit()');
            }
        }
    }

    public function delete(Request $request)
    {
        $studyingIran = Studyingiran::find($request->studyingIran_id);
        $studyingIran->delete();
        return back()->with('message', 'successDelete()');
    }
}

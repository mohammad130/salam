<?php

namespace App\Http\Controllers\admin\studyingIran;

use App\Http\Controllers\Controller;
use App\Models\AdmisstionRequirement;
use Illuminate\Http\Request;
use Symfony\Component\HttpFoundation\File\File;

class admisstionRequirmentsController extends Controller
{
    public function index()
    {
        $AdmisstionRequirementInfs= AdmisstionRequirement::all();
        return view('admin.site.studyingIran.admisstionRequirements.all',compact('AdmisstionRequirementInfs'));
    }

    public function add()
    {
        return view('admin.site.studyingIran.admisstionRequirements.add');
    }

    public function create(Request $request)
    {
        $Create_AdmisstionRequirementInf = [
            'title' => request()->input('title'),
            'shortDescription' => request()->input('shortDescription'),
            'description' => request()->input('description'),
            'video_link' => request()->input('video_link'),
        ];
        $imageinput = request()->file('imageInp');
        if ($imageinput!="") {
            $new_image_about_name = request()->file('imageInp')->getClientOriginalName();
            $result = request()->file('imageInp')->move(public_path('Management/images/studyingIran/admisstionRequirement'),$new_image_about_name);
        }
        if ($imageinput!="" && $result instanceof File){
            $Create_AdmisstionRequirementInf['image'] ="/Management/images/studyingIran/admisstionRequirement/".request()->file('imageInp')->getClientOriginalName();
            $new_object = AdmisstionRequirement::create($Create_AdmisstionRequirementInf);
            if ($new_object) {
                return redirect()->route('admin.admisstionRequirments')->with('message', 'successAdd()');
            }
        }
        else{
            $new_object = AdmisstionRequirement::create($Create_AdmisstionRequirementInf);
            if ($new_object) {
                return redirect()->route('admin.admisstionRequirments')->with('message', 'successAdd()');
            }
        }
    }
    public function edit($admisstionRequirement_id)
    {
        if ($admisstionRequirement_id && ctype_digit($admisstionRequirement_id)) {
            $admisstionRequirementItem = AdmisstionRequirement::find($admisstionRequirement_id);
            if ($admisstionRequirementItem && $admisstionRequirementItem instanceof AdmisstionRequirement) {
                return view('admin.site.studyingIran.admisstionRequirements.edit',compact('admisstionRequirementItem'));
            }
        }
    }

    public function update($admisstionRequirement_id)
    {
        $admisstionRequirement_data = [
            'title' => request()->input('title'),
            'shortDescription' => request()->input('shortDescription'),
            'description' => request()->input('description'),
            'video_link' => request()->input('video_link'),
        ];
        $imageinput = request()->file('imageInp');
        if ($imageinput!="") {
            $new_image_name = request()->file('imageInp')->getClientOriginalName();
            $resultUZ = request()->file('imageInp')->move(public_path('Management/images/studyingIran/admisstionRequirement'),$new_image_name);
        }
        if ($imageinput!="" && $resultUZ instanceof File){
            $admisstionRequirement_data['image'] ="/Management/images/studyingIran/admisstionRequirement/".request()->file('imageInp')->getClientOriginalName();
            $admisstionRequirement = AdmisstionRequirement::find($admisstionRequirement_id);
            $admisstionRequirement->update($admisstionRequirement_data);
            if ($admisstionRequirement) {
                return redirect()->route('admin.admisstionRequirments')->with('message', 'successEdit()');
            }
        }
        else{
            $admisstionRequirement = AdmisstionRequirement::find($admisstionRequirement_id);
            $admisstionRequirement->update($admisstionRequirement_data);
            if ($admisstionRequirement) {
                return redirect()->route('admin.admisstionRequirments')->with('message', 'successEdit()');
            }
        }
    }

    public function delete(Request $request)
    {
        $admisstionRequirement = AdmisstionRequirement::find($request->AdmisstionRequirments_id);
        $admisstionRequirement->delete();
        return back()->with('message', 'successDelete()');
    }
}

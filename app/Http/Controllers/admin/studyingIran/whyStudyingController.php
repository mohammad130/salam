<?php

namespace App\Http\Controllers\admin\studyingIran;

use App\Http\Controllers\Controller;
use App\Models\WhyStudying;
use Illuminate\Http\Request;
use Symfony\Component\HttpFoundation\File\File;

class whyStudyingController extends Controller
{
    public function index()
    {
        $whyStudyingInfs= WhyStudying::all();
        return view('admin.site.studyingIran.whyStudying.all',compact('whyStudyingInfs'));
    }

    public function add()
    {
        return view('admin.site.studyingIran.whyStudying.add');
    }

    public function create(Request $request)
    {
        $Create_WhyStudyingInf = [
            'title' => request()->input('title'),
            'shortDescription' => request()->input('shortDescription'),
            'description' => request()->input('description'),
        ];
        $imageinput = request()->file('imageInp');
        if ($imageinput!="") {
            $new_image_about_name = request()->file('imageInp')->getClientOriginalName();
            $result = request()->file('imageInp')->move(public_path('Management/images/studyingIran/whyStudying'),$new_image_about_name);
        }
        if ($imageinput!="" && $result instanceof File){
            $Create_WhyStudyingInf['image'] ="/Management/images/studyingIran/whyStudying/".request()->file('imageInp')->getClientOriginalName();
            $new_object = WhyStudying::create($Create_WhyStudyingInf);
            if ($new_object) {
                return redirect()->route('admin.whyStudying')->with('message', 'successAdd()');
            }
        }
        else{
            $new_object = WhyStudying::create($Create_WhyStudyingInf);
            if ($new_object) {
                return redirect()->route('admin.whyStudying')->with('message', 'successAdd()');
            }
        }
    }
    public function edit($whyStudying_id)
    {
        if ($whyStudying_id && ctype_digit($whyStudying_id)) {
            $whyStudyingItem = WhyStudying::find($whyStudying_id);
            if ($whyStudyingItem && $whyStudyingItem instanceof WhyStudying) {
                return view('admin.site.studyingIran.whyStudying.edit',compact('whyStudyingItem'));
            }
        }
    }

    public function update($whyStudying_id)
    {
        $whyStudying_data = [
            'title' => request()->input('title'),
            'shortDescription' => request()->input('shortDescription'),
            'description' => request()->input('description'),
        ];
        $imageinput = request()->file('imageInp');
        if ($imageinput!="") {
            $new_image_name = request()->file('imageInp')->getClientOriginalName();
            $resultUZ = request()->file('imageInp')->move(public_path('Management/images/studyingIran/whyStudying'),$new_image_name);
        }
        if ($imageinput!="" && $resultUZ instanceof File){
            $whyStudying_data['image'] ="/Management/images/studyingIran/whyStudying/".request()->file('imageInp')->getClientOriginalName();
            $whyStudying = WhyStudying::find($whyStudying_id);
            $whyStudying->update($whyStudying_data);
            if ($whyStudying) {
                return redirect()->route('admin.whyStudying')->with('message', 'successEdit()');
            }
        }
        else{
            $whyStudying = WhyStudying::find($whyStudying_id);
            $whyStudying->update($whyStudying_data);
            if ($whyStudying) {
                return redirect()->route('admin.whyStudying')->with('message', 'successEdit()');
            }
        }
    }

    public function delete(Request $request)
    {
        $whyStudying = WhyStudying::find($request->WhyStudying_id);
        $whyStudying->delete();
        return back()->with('message', 'successDelete()');
    }
}

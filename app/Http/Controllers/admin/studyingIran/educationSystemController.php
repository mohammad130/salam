<?php

namespace App\Http\Controllers\admin\studyingIran;

use App\Http\Controllers\Controller;
use App\Models\EducationSystem;
use Illuminate\Http\Request;
use Symfony\Component\HttpFoundation\File\File;

class educationSystemController extends Controller
{
    public function index()
    {
        $EducationSystemInfs= EducationSystem::all();
        return view('admin.site.studyingIran.educationSystem.all',compact('EducationSystemInfs'));
    }

    public function add()
    {
        return view('admin.site.studyingIran.educationSystem.add');
    }

    public function create(Request $request)
    {
        $Create_EducationSystemInf = [
            'title' => request()->input('title'),
            'shortDescription' => request()->input('shortDescription'),
            'description' => request()->input('description'),
            'video_link' => request()->input('video_link'),
        ];
        $imageinput = request()->file('imageInp');
        if ($imageinput!="") {
            $new_image_about_name = request()->file('imageInp')->getClientOriginalName();
            $result = request()->file('imageInp')->move(public_path('Management/images/studyingIran/educationSystem'),$new_image_about_name);
        }
        if ($imageinput!="" && $result instanceof File){
            $Create_EducationSystemInf['image'] ="/Management/images/studyingIran/educationSystem/".request()->file('imageInp')->getClientOriginalName();
            $new_object = EducationSystem::create($Create_EducationSystemInf);
            if ($new_object) {
                return redirect()->route('admin.educationSystem')->with('message', 'successAdd()');
            }
        }
        else{
            $new_object = EducationSystem::create($Create_EducationSystemInf);
            if ($new_object) {
                return redirect()->route('admin.educationSystem')->with('message', 'successAdd()');
            }
        }
    }
    public function edit($educationSystem_id)
    {
        if ($educationSystem_id && ctype_digit($educationSystem_id)) {
            $educationSystemItem = EducationSystem::find($educationSystem_id);
            if ($educationSystemItem && $educationSystemItem instanceof EducationSystem) {
                return view('admin.site.studyingIran.educationSystem.edit',compact('educationSystemItem'));
            }
        }
    }

    public function update($educationSystem_id)
    {
        $educationSystem_data = [
            'title' => request()->input('title'),
            'shortDescription' => request()->input('shortDescription'),
            'description' => request()->input('description'),
            'video_link' => request()->input('video_link'),
        ];
        $imageinput = request()->file('imageInp');
        if ($imageinput!="") {
            $new_image_name = request()->file('imageInp')->getClientOriginalName();
            $resultUZ = request()->file('imageInp')->move(public_path('Management/images/studyingIran/educationSystem'),$new_image_name);
        }
        if ($imageinput!="" && $resultUZ instanceof File){
            $educationSystem_data['image'] ="/Management/images/studyingIran/educationSystem/".request()->file('imageInp')->getClientOriginalName();
            $educationSystem = EducationSystem::find($educationSystem_id);
            $educationSystem->update($educationSystem_data);
            if ($educationSystem) {
                return redirect()->route('admin.educationSystem')->with('message', 'successEdit()');
            }
        }
        else{
            $educationSystem = EducationSystem::find($educationSystem_id);
            $educationSystem->update($educationSystem_data);
            if ($educationSystem) {
                return redirect()->route('admin.educationSystem')->with('message', 'successEdit()');
            }
        }
    }

    public function delete(Request $request)
    {
        $educationSystem = EducationSystem::find($request->EducationSystem_id);
        $educationSystem->delete();
        return back()->with('message', 'successDelete()');
    }
}

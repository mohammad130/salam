<?php

namespace App\Http\Controllers\admin;

use App\Http\Controllers\Controller;
use App\Models\WhoWeAreDesk;
use Illuminate\Http\Request;
use Symfony\Component\HttpFoundation\File\File;

class WhoWeAreDeskController extends Controller
{
    public function index()
    {
        $whoWeAreInfs= WhoWeAreDesk::paginate(10);
        return view('admin.site.whoWeAreDesk.all',compact('whoWeAreInfs'));
    }

    public function add()
    {
        return view('admin.site.whoWeAreDesk.add');
    }

    public function create(Request $request)
    {
        $Create_whoWeAreInf = [
            'text' => request()->input('text'),
        ];
        
            $new_object = WhoWeAreDesk::create($Create_whoWeAreInf);
            if ($new_object) {
                return redirect()->route('admin.whoWeAreDesk')->with('message', 'successAdd()');
            }
    }
    public function edit($WhoWeAre_id)
    {
        if ($WhoWeAre_id && ctype_digit($WhoWeAre_id)) {
            $WhoWeAreItem = WhoWeAreDesk::find($WhoWeAre_id);
            if ($WhoWeAreItem && $WhoWeAreItem instanceof WhoWeAreDesk) {
                return view('admin.site.whoWeAreDesk.edit',compact('WhoWeAreItem'));
            }
        }
    }

    public function update($WhoWeAre_id)
    {
        $WhoWeAre_data = [
            'text' => request()->input('text'),
        ];
            $WhoWeAre = WhoWeAreDesk::find($WhoWeAre_id);
            $WhoWeAre->update($WhoWeAre_data);
            if ($WhoWeAre) {
                return redirect()->route('admin.whoWeAreDesk')->with('message', 'successEdit()');
            }
    }

    public function delete(Request $request)
    {
        $WhoWeAre = WhoWeAreDesk::find($request->WhoWeAre_id);
        $WhoWeAre->delete();
        return back()->with('message', 'successDelete()');
    }
}

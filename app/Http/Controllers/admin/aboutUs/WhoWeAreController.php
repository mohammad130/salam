<?php

namespace App\Http\Controllers\admin\aboutUs;

use App\Http\Controllers\Controller;

use App\Models\WhoWeAre;
use Illuminate\Http\Request;
use Symfony\Component\HttpFoundation\File\File;

class WhoWeAreController extends Controller
{
    public function index()
    {
        $whoWeAreInfs= WhoWeAre::paginate(10);
        return view('admin.site.aboutUs.whoWeAre.all',compact('whoWeAreInfs'));
    }

    public function add()
    {
        return view('admin.site.aboutUs.whoWeAre.add');
    }

    public function create(Request $request)
    {
        $Create_whoWeAreInf = [
            'title' => request()->input('title'),
            'shortDescription' => request()->input('shortDescription'),
            'description' => request()->input('description'),
            'video_link' => request()->input('video_link'),
        ];
        $imageinput = request()->file('imageInp');
        if ($imageinput!="") {
            $new_image_about_name = request()->file('imageInp')->getClientOriginalName();
            $result = request()->file('imageInp')->move(public_path('Management/images/whoWeAre'),$new_image_about_name);
        }
        if ($imageinput!="" && $result instanceof File){
            $Create_whoWeAreInf['image'] ="/Management/images/whoWeAre/".request()->file('imageInp')->getClientOriginalName();
            $new_object = WhoWeAre::create($Create_whoWeAreInf);
            if ($new_object) {
                return redirect()->route('admin.whoWeAre')->with('message', 'successAdd()');
            }
        }
        else{
            $new_object = WhoWeAre::create($Create_whoWeAreInf);
            if ($new_object) {
                return redirect()->route('admin.whoWeAre')->with('message', 'successAdd()');
            }
        }
    }
    public function edit($WhoWeAre_id)
    {
        if ($WhoWeAre_id && ctype_digit($WhoWeAre_id)) {
            $WhoWeAreItem = WhoWeAre::find($WhoWeAre_id);
            if ($WhoWeAreItem && $WhoWeAreItem instanceof WhoWeAre) {
                return view('admin.site.aboutUs.whoWeAre.edit',compact('WhoWeAreItem'));
            }
        }
    }

    public function update($WhoWeAre_id)
    {
        $WhoWeAre_data = [
            'title' => request()->input('title'),
            'shortDescription' => request()->input('shortDescription'),
            'description' => request()->input('description'),
            'video_link' => request()->input('video_link'),
        ];
        $imageinput = request()->file('imageInp');
        if ($imageinput!="") {
            $new_image_name = request()->file('imageInp')->getClientOriginalName();
            $resultUZ = request()->file('imageInp')->move(public_path('Management/images/whoWeAre'),$new_image_name);
        }
        if ($imageinput!="" && $resultUZ instanceof File){
            $WhoWeAre_data['image'] ="/Management/images/whoWeAre/".request()->file('imageInp')->getClientOriginalName();
            $WhoWeAre = WhoWeAre::find($WhoWeAre_id);
            $WhoWeAre->update($WhoWeAre_data);
            if ($WhoWeAre) {
                return redirect()->route('admin.whoWeAre')->with('message', 'successEdit()');
            }
        }
        else{
            $WhoWeAre = WhoWeAre::find($WhoWeAre_id);
            $WhoWeAre->update($WhoWeAre_data);
            if ($WhoWeAre) {
                return redirect()->route('admin.whoWeAre')->with('message', 'successEdit()');
            }
        }
    }

    public function delete(Request $request)
    {
        $WhoWeAre = WhoWeAre::find($request->WhoWeAre_id);
        $WhoWeAre->delete();
        return back()->with('message', 'successDelete()');
    }
}

<?php

namespace App\Http\Controllers\admin\aboutUs;

use App\Http\Controllers\Controller;
use App\Models\HowWeCanHelp;
use Illuminate\Http\Request;
use Symfony\Component\HttpFoundation\File\File;

class HowWeCanHelpController extends Controller
{
    public function index()
    {
        $howWeCanHelpInfs= HowWeCanHelp::all();
        return view('admin.site.aboutUs.howWeCanHelpYou.all',compact('howWeCanHelpInfs'));
    }

    public function add()
    {
        return view('admin.site.aboutUs.howWeCanHelpYou.add');
    }

    public function create(Request $request)
    {
        $Create_howWeCanHelp = [
            'title' => request()->input('title'),
            'shortDescription' => request()->input('shortDescription'),
            'description' => request()->input('description'),
            'video_link' => request()->input('video_link'),
        ];
        $imageinput = request()->file('imageInp');
        if ($imageinput!="") {
            $new_image_about_name = request()->file('imageInp')->getClientOriginalName();
            $result = request()->file('imageInp')->move(public_path('Management/images/howWeCanHelp'),$new_image_about_name);
        }
        if ($imageinput!="" && $result instanceof File){
            $Create_howWeCanHelp['image'] ="/Management/images/howWeCanHelp/".request()->file('imageInp')->getClientOriginalName();
            $new_object = HowWeCanHelp::create($Create_howWeCanHelp);
            if ($new_object) {
                return redirect()->route('admin.howWeCanHelp')->with('message', 'successAdd()');
            }
        }
        else{
            $new_object = HowWeCanHelp::create($Create_howWeCanHelp);
            if ($new_object) {
                return redirect()->route('admin.howWeCanHelp')->with('message', 'successAdd()');
            }
        }
    }
    public function edit($HowWeCanHelp_id)
    {
        if ($HowWeCanHelp_id && ctype_digit($HowWeCanHelp_id)) {
            $HowWeCanHelpItem = HowWeCanHelp::find($HowWeCanHelp_id);
            if ($HowWeCanHelpItem && $HowWeCanHelpItem instanceof HowWeCanHelp) {
                return view('admin.site.aboutUs.howWeCanHelpYou.edit',compact('HowWeCanHelpItem'));
            }
        }
    }

    public function update($HowWeCanHelp_id)
    {
        $HowWeCanHelp_data = [
            'title' => request()->input('title'),
            'shortDescription' => request()->input('shortDescription'),
            'description' => request()->input('description'),
            'video_link' => request()->input('video_link'),
        ];
        $imageinput = request()->file('imageInp');
        if ($imageinput!="") {
            $new_image_name = request()->file('imageInp')->getClientOriginalName();
            $resultUZ = request()->file('imageInp')->move(public_path('Management/images/howWeCanHelp'),$new_image_name);
        }
        if ($imageinput!="" && $resultUZ instanceof File){
            $HowWeCanHelp_data['image'] ="/Management/images/howWeCanHelp/".request()->file('imageInp')->getClientOriginalName();
            $howWeCanHelp = HowWeCanHelp::find($HowWeCanHelp_id);
            $howWeCanHelp->update($HowWeCanHelp_data);
            if ($howWeCanHelp) {
                return redirect()->route('admin.howWeCanHelp')->with('message', 'successEdit()');
            }
        }
        else{
            $howWeCanHelp = HowWeCanHelp::find($HowWeCanHelp_id);
            $howWeCanHelp->update($HowWeCanHelp_data);
            if ($howWeCanHelp) {
                return redirect()->route('admin.howWeCanHelp')->with('message', 'successEdit()');
            }
        }
    }

    public function delete(Request $request)
    {
        $howWeCanHelp = HowWeCanHelp::find($request->HowWeCanHelp_id);
        $howWeCanHelp->delete();
        return back()->with('message', 'successDelete()');
    }
}

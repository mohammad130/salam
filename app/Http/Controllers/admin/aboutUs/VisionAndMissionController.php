<?php

namespace App\Http\Controllers\admin\aboutUs;

use App\Http\Controllers\Controller;
use App\Models\VisionAndMission;
use Illuminate\Http\Request;
use Symfony\Component\HttpFoundation\File\File;

class VisionAndMissionController extends Controller
{
    public function index()
    {
        $visionAndMissionInfs= VisionAndMission::all();
        return view('admin.site.aboutUs.visionAndMission.all',compact('visionAndMissionInfs'));
    }

    public function add()
    {
        return view('admin.site.aboutUs.visionAndMission.add');
    }

    public function create(Request $request)
    {
        $Create_visionAndMission = [
            'title' => request()->input('title'),
            'shortDescription' => request()->input('shortDescription'),
            'description' => request()->input('description'),
            'video_link' => request()->input('video_link'),

        ];
        $imageinput = request()->file('imageInp');
        if ($imageinput!="") {
            $new_image_about_name = request()->file('imageInp')->getClientOriginalName();
            $result = request()->file('imageInp')->move(public_path('Management/images/visionAndMission'),$new_image_about_name);
        }
        if ($imageinput!="" && $result instanceof File){
            $Create_visionAndMission['image'] ="/Management/images/visionAndMission/".request()->file('imageInp')->getClientOriginalName();
            $new_object = VisionAndMission::create($Create_visionAndMission);
            if ($new_object) {
                return redirect()->route('admin.visionAndMission')->with('message', 'successAdd()');
            }
        }
        else{
            $new_object = VisionAndMission::create($Create_visionAndMission);
            if ($new_object) {
                return redirect()->route('admin.visionAndMission')->with('message', 'successAdd()');
            }
        }
    }
    public function edit($VisionAndMission_id)
    {
        if ($VisionAndMission_id && ctype_digit($VisionAndMission_id)) {
            $VisionAndMissionItem = VisionAndMission::find($VisionAndMission_id);
            if ($VisionAndMissionItem && $VisionAndMissionItem instanceof VisionAndMission) {
                return view('admin.site.aboutUs.visionAndMission.edit',compact('VisionAndMissionItem'));
            }
        }
    }

    public function update($VisionAndMission_id)
    {
        $visionAndMission_data = [
            'title' => request()->input('title'),
            'shortDescription' => request()->input('shortDescription'),
            'description' => request()->input('description'),
            'video_link' => request()->input('video_link'),
        ];
        $imageinput = request()->file('imageInp');
        if ($imageinput!="") {
            $new_image_name = request()->file('imageInp')->getClientOriginalName();
            $resultUZ = request()->file('imageInp')->move(public_path('Management/images/visionAndMission'),$new_image_name);
        }
        if ($imageinput!="" && $resultUZ instanceof File){
            $visionAndMission_data['image'] ="/Management/images/visionAndMission/".request()->file('imageInp')->getClientOriginalName();
            $visionAndMission = VisionAndMission::find($VisionAndMission_id);
            $visionAndMission->update($visionAndMission_data);
            if ($visionAndMission) {
                return redirect()->route('admin.visionAndMission')->with('message', 'successEdit()');
            }
        }
        else{
            $visionAndMission = VisionAndMission::find($VisionAndMission_id);
            $visionAndMission->update($visionAndMission_data);
            if ($visionAndMission) {
                return redirect()->route('admin.visionAndMission')->with('message', 'successEdit()');
            }
        }
    }

    public function delete(Request $request)
    {
        $visionAndMission = VisionAndMission::find($request->VisionAndMission_id);
        $visionAndMission->delete();
        return back()->with('message', 'successDelete()');
    }
}

<?php

namespace App\Http\Controllers\admin\aboutUs;

use App\Http\Controllers\Controller;
use App\Models\ContactUs;
use Illuminate\Http\Request;

class ContactUsController extends Controller
{
    public function index()
    {
        $contactUsInfs= ContactUs::all();
        return view('admin.site.aboutUs.contactUs.all',compact('contactUsInfs'));
    }

    public function delete(Request $request)
    {
        $contactUs = ContactUs::find($request->contactUs_id);
        $contactUs->delete();
        return back()->with('message', 'successDelete()');
    }

    public function details(){
            $userId = $_GET["userId"];
            $userItem = ContactUs::find($userId);
            return $userItem;
        }
}

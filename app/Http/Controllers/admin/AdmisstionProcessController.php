<?php

namespace App\Http\Controllers\admin;

use App\Http\Controllers\Controller;

use App\Models\AdmisstionProcess;
use Illuminate\Http\Request;
use Symfony\Component\HttpFoundation\File\File;

class AdmisstionProcessController extends Controller
{
    public function index()
    {
        $AdmisstionProcessInfs= AdmisstionProcess::all();
        return view('admin.site.admisstionProcess.all',compact('AdmisstionProcessInfs'));
    }

    public function add()
    {
        return view('admin.site.admisstionProcess.add');
    }

    public function create(Request $request)
    {
        $Create_AdmisstionProcessInf = [
            'title' => request()->input('title'),
            'shortDescription' => request()->input('shortDescription'),
            'description' => request()->input('description'),
            'video_link' => request()->input('video_link'),
        ];
        $imageinput = request()->file('imageInp');
        if ($imageinput!="") {
            $new_image_about_name = request()->file('imageInp')->getClientOriginalName();
            $result = request()->file('imageInp')->move(public_path('Management/images/AdmisstionProcess'),$new_image_about_name);
        }
        if ($imageinput!="" && $result instanceof File){
            $Create_AdmisstionProcessInf['image'] ="/Management/images/AdmisstionProcess/".request()->file('imageInp')->getClientOriginalName();
            $new_object = AdmisstionProcess::create($Create_AdmisstionProcessInf);
            if ($new_object) {
                return redirect()->route('admin.admisstionProcess')->with('message', 'successAdd()');
            }
        }
        else{
            $new_object = AdmisstionProcess::create($Create_AdmisstionProcessInf);
            if ($new_object) {
                return redirect()->route('admin.admisstionProcess')->with('message', 'successAdd()');
            }
        }
    }
    public function edit($admisstionProcess_id)
    {
        if ($admisstionProcess_id && ctype_digit($admisstionProcess_id)) {
            $admisstionProcessItem = AdmisstionProcess::find($admisstionProcess_id);
            if ($admisstionProcessItem && $admisstionProcessItem instanceof AdmisstionProcess) {
                return view('admin.site.admisstionProcess.edit',compact('admisstionProcessItem'));
            }
        }
    }

    public function update($admisstionProcess_id)
    {
        $admisstionProcess_data = [
            'title' => request()->input('title'),
            'shortDescription' => request()->input('shortDescription'),
            'description' => request()->input('description'),
            'video_link' => request()->input('video_link'),
        ];
        $imageinput = request()->file('imageInp');
        if ($imageinput!="") {
            $new_image_name = request()->file('imageInp')->getClientOriginalName();
            $resultUZ = request()->file('imageInp')->move(public_path('Management/images/AdmisstionProcess'),$new_image_name);
        }
        if ($imageinput!="" && $resultUZ instanceof File){
            $admisstionProcess_data['image'] ="/Management/images/AdmisstionProcess/".request()->file('imageInp')->getClientOriginalName();
            $admisstionProcess = AdmisstionProcess::find($admisstionProcess_id);
            $admisstionProcess->update($admisstionProcess_data);
            if ($admisstionProcess) {
                return redirect()->route('admin.admisstionProcess')->with('message', 'successEdit()');
            }
        }
        else{
            $admisstionProcess = AdmisstionProcess::find($admisstionProcess_id);
            $admisstionProcess->update($admisstionProcess_data);
            if ($admisstionProcess) {
                return redirect()->route('admin.admisstionProcess')->with('message', 'successEdit()');
            }
        }
    }

    public function delete(Request $request)
    {
        $admisstionProcess = AdmisstionProcess::find($request->AdmisstionProcess_id);
        $admisstionProcess->delete();
        return back()->with('message', 'successDelete()');
    }
}

<?php

namespace App\Http\Controllers\admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Program;
use Symfony\Component\HttpFoundation\File\File;

class ProgramController extends Controller
{
    

    public function index()
    {
        $programs= Program::all();
        return view('admin.site.program.all',compact('programs'));
    }
    public function add()
    {
        return view('admin.site.program.add');
    }

    public function create(Request $request)
    {
        $Create_Program = [
            'title' => request()->input('title'),
            'shortDescription' => request()->input('shortDescription'),
            'description' => request()->input('description'),
            'video_link' => request()->input('video_link'),
        ];
        $imageinput = request()->file('imageInp');
        if ($imageinput!="") {
            $new_image_about_name = request()->file('imageInp')->getClientOriginalName();
            $result = request()->file('imageInp')->move(public_path('Management/images/Program'),$new_image_about_name);
        }
        if ($imageinput!="" && $result instanceof File){
            $Create_Program['image'] ="/Management/images/Program/".request()->file('imageInp')->getClientOriginalName();
            $new_Program_object = Program::create($Create_Program);
            if ($new_Program_object) {
                return redirect()->route('admin.program')->with('message', 'successAdd()');
            }
        }
        else{
            $new_Program_object = Program::create($Create_Program);
            if ($new_Program_object) {
                return redirect()->route('admin.program')->with('message', 'successAdd()');
            }
        }
    }
    public function edit($Program_id)
    {
        if ($Program_id && ctype_digit($Program_id)) {
            $ProgramItem = Program::find($Program_id);
            if ($ProgramItem && $ProgramItem instanceof Program) {
                return view('admin.site.program.edit',compact('ProgramItem'));
            }
        }
    }

    public function update($Program_id)
    {
        $program_data = [
            'title' => request()->input('title'),
            'shortDescription' => request()->input('shortDescription'),
            'description' => request()->input('description'),
            'video_link' => request()->input('video_link'),
        ];
        $imageinput = request()->file('imageInp');
        if ($imageinput!="") {
            $new_image_program_name = request()->file('imageInp')->getClientOriginalName();
            $resultUZ = request()->file('imageInp')->move(public_path('Management\images\Program'),$new_image_program_name);
        }
        if ($imageinput!="" && $resultUZ instanceof File){
            $program_data['image'] ="/Management/images/Program/".request()->file('imageInp')->getClientOriginalName();
            $program = Program::find($Program_id);
            $program->update($program_data);
            if ($program) {
                return redirect()->route('admin.program')->with('message', 'successEdit()');
            }
        }
        else{
            $program = Program::find($Program_id);
            $program->update($program_data);
            if ($program) {
                return redirect()->route('admin.program')->with('message', 'successEdit()');
            }
        }
    }

    public function delete(Request $request)
    {
        $program = Program::find($request->Program_id);
        $program->delete();
        return back()->with('message', 'successDelete()');
    }



}

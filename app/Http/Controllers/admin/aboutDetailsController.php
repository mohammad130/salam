<?php

namespace App\Http\Controllers\admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\AboutDetail;

class aboutDetailsController extends Controller
{
    public function index($AboutDetail_id)
    {
        if ($AboutDetail_id && ctype_digit($AboutDetail_id)) {
            $AboutDetailItem = AboutDetail::find($AboutDetail_id);
            if ($AboutDetailItem && $AboutDetailItem instanceof AboutDetail) {
                return view('admin.site.aboutDetails.all',compact('AboutDetailItem'));
            }
        }
    }
    public function create($AboutDetail_id)
    {
        $aboutDetailInf_data = [
            'Tel' => request()->input('Tel'),
            'Email' => request()->input('Email'),
            'Office_Address' => request()->input('Office_Address'),
            'About_us' => request()->input('About_us'),
        ];
            $aboutDetail = AboutDetail::find($AboutDetail_id);
            $aboutDetail->update($aboutDetailInf_data);
            if ($aboutDetail) {
                return redirect()->route('admin.aboutDetails',1)->with('message', 'successAdd()');
            }
    }
}

<?php

namespace App\Http\Controllers\admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Services;
use Symfony\Component\HttpFoundation\File\File;

class ServicesController extends Controller
{
    public function index()
    {
        $services= Services::paginate(10);
        return view('admin.site.services.all',compact('services'));
    }

    public function add()
    {
        return view('admin.site.services.add');
    }

    public function create(Request $request)
    {
        $Create_Service = [
            'title' => request()->input('title'),
            'shortDescription' => request()->input('shortDescription'),
            'description' => request()->input('description'),
            'video_link' => request()->input('video_link'),
        ];
        $imageinput = request()->file('imageService');
        if ($imageinput!="") {
            $new_image_about_name = request()->file('imageService')->getClientOriginalName();
            $result = request()->file('imageService')->move(public_path('Management/images/Service'),$new_image_about_name);
        }
        if ($imageinput!="" && $result instanceof File){
            $Create_Service['image'] ="/Management/images/Service/".request()->file('imageService')->getClientOriginalName();
            $new_Service_object = Services::create($Create_Service);
            if ($new_Service_object) {
                return redirect()->route('admin.service')->with('message', 'successAdd()');
            }
        }
        else{
            $new_Service_object = Services::create($Create_Service);
            if ($new_Service_object) {
                return redirect()->route('admin.service')->with('message', 'successAdd()');
            }
        }
    }
    public function edit($Service_id)
    {
        if ($Service_id && ctype_digit($Service_id)) {
            $ServiceItem = Services::find($Service_id);
            if ($ServiceItem && $ServiceItem instanceof Services) {
                return view('admin.site.services.edit',compact('ServiceItem'));
            }
        }
    }

    public function update($Service_id)
    {
        $Service_data = [
            'title' => request()->input('title'),
            'shortDescription' => request()->input('shortDescription'),
            'description' => request()->input('description'),
            'video_link' => request()->input('video_link'),
        ];
        $imageinput = request()->file('imageService');
        if ($imageinput!="") {
            $new_image_about_name = request()->file('imageService')->getClientOriginalName();
            $resultUZ = request()->file('imageService')->move(public_path('Management/images/Service'),$new_image_about_name);
        }
        if ($imageinput!="" && $resultUZ instanceof File){
            $Service_data['image'] ="/Management/images/Service/".request()->file('imageService')->getClientOriginalName();
            $Service = Services::find($Service_id);
            $Service->update($Service_data);
            if ($Service) {
                return redirect()->route('admin.service')->with('message', 'successEdit()');
            }
        }
        else{
            $Service = Services::find($Service_id);
            $Service->update($Service_data);
            if ($Service) {
                return redirect()->route('admin.service')->with('message', 'successEdit()');
            }
        }
    }

    public function delete(Request $request)
    {
        $Service = Services::find($request->Service_id);
        $Service->delete();
        return back()->with('message', 'successDelete()');
    }
}

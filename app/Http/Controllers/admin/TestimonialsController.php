<?php

namespace App\Http\Controllers\admin;

use App\Http\Controllers\Controller;
use App\Models\Testimonial;
use Illuminate\Http\Request;
use Symfony\Component\HttpFoundation\File\File;

class TestimonialsController extends Controller
{
    public function index()
    {
        $testimonials = Testimonial::all();
        return view('admin.site.testimonials.all', compact('testimonials'));
    }

    public function add()
    {
        return view('admin.site.testimonials.add');
    }

    public function create(Request $request)
    {
        $Create_testimonials = [
            'title' => request()->input('title'),
            'city' => request()->input('city'),
            'university' => request()->input('university'),
            'lesson' => request()->input('lesson'),
            'shortDescription' => request()->input('shortDescription'),
            'description' => request()->input('description'),
            'lesson' => request()->input('lesson'),
            'city' => request()->input('city'),
            'video_link' => request()->input('video_link'),
        ];

        $imageinput = request()->file('imageInp');
        if ($imageinput!="") {
            $new_image_about_name = request()->file('imageInp')->getClientOriginalName();
            $result = request()->file('imageInp')->move(public_path('Management/images/testimonial'),$new_image_about_name);
        }
        if ($imageinput!="" && $result instanceof File){
            $Create_testimonials['image'] ="/Management/images/testimonial/".request()->file('imageInp')->getClientOriginalName();
            $new_object = Testimonial::create($Create_testimonials);
            if ($new_object) {
                return redirect()->route('admin.testimonial')->with('message', 'successAdd()');
            }
        }
        else{
            $new_object = Testimonial::create($Create_testimonials);
            if ($new_object) {
                return redirect()->route('admin.testimonial')->with('message', 'successAdd()');
            }
        }
    }

    public function edit($Testimonial_id)
    {
        if ($Testimonial_id && ctype_digit($Testimonial_id)) {
            $TestimonialItem = Testimonial::find($Testimonial_id);
            if ($TestimonialItem && $TestimonialItem instanceof Testimonial) {
                return view('admin.site.testimonials.edit', compact('TestimonialItem'));
            }
        }
    }

    public function update($Testimonial_id)
    {
        $Testimonial_data = [
            'title' => request()->input('title'),
            'shortDescription' => request()->input('shortDescription'),
            'description' => request()->input('description'),
            'lesson' => request()->input('lesson'),
            'university' => request()->input('university'),
            'city' => request()->input('city'),
            'video_link' => request()->input('video_link'),

        ];
        $imageinput = request()->file('imageInp');
        if ($imageinput!="") {
            $new_image_name = request()->file('imageInp')->getClientOriginalName();
            $resultUZ = request()->file('imageInp')->move(public_path('Management/images/testimonial'),$new_image_name);
        }
        if ($imageinput!="" && $resultUZ instanceof File){
            $Testimonial_data['image'] ="/Management/images/testimonial/".request()->file('imageInp')->getClientOriginalName();
            $Testimonial = Testimonial::find($Testimonial_id);
            $Testimonial->update($Testimonial_data);
            if ($Testimonial) {
                return redirect()->route('admin.testimonial')->with('message', 'successEdit()');
            }
        }
        else{
            $Testimonial = Testimonial::find($Testimonial_id);
            $Testimonial->update($Testimonial_data);
            if ($Testimonial) {
                return redirect()->route('admin.testimonial')->with('message', 'successEdit()');
            }
        }
    }

    public function delete(Request $request)
    {
        $Testimonial = Testimonial::find($request->Testimonial_id);
        $Testimonial->delete();
        return back()->with('message', 'successDelete()');
    }
}

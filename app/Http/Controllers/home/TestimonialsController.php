<?php

namespace App\Http\Controllers\home;

use App\Http\Controllers\Controller;
use App\Models\Testimonial;
use Illuminate\Http\Request;

class TestimonialsController extends Controller
{
    public function index(){
        $testimonials = Testimonial::all();
        return view('home.site.testimonials.add',compact('testimonials'));
    }
    public function student($Testimonial_title)
    {
        $lastTestimonials = Testimonial::orderBy('id', 'desc')->take(3)->get();
        if ($Testimonial_title) {
            $TestimonialItem = Testimonial::where('title',$Testimonial_title)->first();
            if ($TestimonialItem && $TestimonialItem instanceof Testimonial) {
                return view('home.site.testimonials.student.all', compact('TestimonialItem', 'lastTestimonials'));
            }
        }
    }
}
<?php

namespace App\Http\Controllers\home;

use App\Http\Controllers\Controller;
use App\Models\ContactUs;
use Illuminate\Http\Request;

class ContactUsController extends Controller
{
    public function index()
    {
        return view('home.site.contactUs.all');
    }
    public function createContactMember(Request $request)
    {
        $Contact_data = [
            'name' => request()->input('name'),
            'title' => request()->input('title'),
            'description' => request()->input('description'),

        ];
        $new_Contact_object = ContactUs::create($Contact_data);
        if ($new_Contact_object && $new_Contact_object instanceof ContactUs) {
            return redirect()->back()->with('contactsError','Your message has been successfully registered');
        }
    }
}

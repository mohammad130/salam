<?php

namespace App\Http\Controllers\home;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\WhoWeAre;

class WhoWeAreController extends Controller
{
    public function index()
    {
        $whoWeAreInfs= WhoWeAre::all();
        return view('home.site.aboutUs.whoweare.all',compact('whoWeAreInfs'));
    }
}

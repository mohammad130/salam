<?php

namespace App\Http\Controllers\home\livingIran;

use App\Http\Controllers\Controller;
use App\Models\Culture;
use Illuminate\Http\Request;

class CultureController extends Controller
{
    public function index()
    {
        $cultures=Culture::all();
        return view('home.site.livingIran.culture.all',compact('cultures'));
    }
}

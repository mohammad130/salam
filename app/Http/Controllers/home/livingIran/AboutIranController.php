<?php

namespace App\Http\Controllers\home\livingIran;

use App\Http\Controllers\Controller;
use App\Models\AboutIran;
use Illuminate\Http\Request;

class AboutIranController extends Controller
{
    public function index()
    {
        $aboutIrans=AboutIran::all();
        return view('home.site.livingIran.aboutIran.all',compact('aboutIrans'));
    }
}

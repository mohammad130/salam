<?php

namespace App\Http\Controllers\home\livingIran;

use App\Http\Controllers\Controller;
use App\Models\TourismIran;
use Illuminate\Http\Request;

class TourismIranController extends Controller
{
    public function index()
    {
        $tourisms=TourismIran::all();
        return view('home.site.livingIran.tourismInIran.all',compact('tourisms'));
    }
}

<?php

namespace App\Http\Controllers\home;

use App\Http\Controllers\Controller;
use App\Models\University;
use Illuminate\Http\Request;

class UniversityController extends Controller
{
    public function detail($University_title)
    {
//        $lastUniversities = University::orderBy('id', 'desc')->take(2)->get();
        if ($University_title) {
            $universityItems = University::where("title",$University_title)->first();
            if ($universityItems && $universityItems instanceof University) {
                return view('home.site.university.detail', compact('universityItems'));
            }
        }
    }
    public function index()
    {
        $universities = University::all();
        return view('home.site.university.index',compact('universities'));
    }

}

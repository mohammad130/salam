<?php

namespace App\Http\Controllers\home;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Services;

class ServicesController extends Controller
{
    public function index()
    {
        return view('home.site.services.all');
    }
    public function singleService($service_id)
    {
        if ($service_id && ctype_digit($service_id)) {
            $serviceItems = Services::find($service_id);
            if ($serviceItems && $serviceItems instanceof Services) {
                return view('home.site.services.all', compact('serviceItems'));
            }
        }
    }
}

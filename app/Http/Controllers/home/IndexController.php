<?php

namespace App\Http\Controllers\home;

use App\Http\Controllers\Controller;
use App\Models\Blog;
use App\Models\Slider;
use App\Models\Testimonial;
use App\Models\University;
use App\Models\WhoWeAre;
use Illuminate\Http\Request;
use App\Models\WhyStudying;
use App\Models\Services;
use App\Models\Studyingiran;
use App\Models\WhoWeAreDesk;
use App\Models\AboutDetail;

class IndexController extends Controller
{
    public function index()
    {
        
        $testimonials = Testimonial::all();
        $sliders= Slider::all();
        $whoWeAreInfs= WhoWeAre::all();
        $blogs= Blog::orderBy('id', 'desc')->take(3)->get();
        $universities= University::all();
        $WhyStudyings= WhyStudying::all();
        $services= Services::all();
        $Studyingirans= Studyingiran::all();
        $whoWeAreDesks = WhoWeAreDesk::all();
        $aboutDetials = AboutDetail::all();
        return view('home.site.index.all',compact('sliders','whoWeAreInfs','blogs','testimonials','universities','services','WhyStudyings','Studyingirans','whoWeAreDesks','aboutDetials'));
    }
}

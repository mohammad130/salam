<?php

namespace App\Http\Controllers\home;

use App\Http\Controllers\Controller;
use App\Models\Newsletter;
use Illuminate\Http\Request;

class NewsletterController extends Controller
{
    public function newsletter(Request $request)
    {
        $newsletter_data = [
            'email' => request()->input('email'),

        ];
        $new_newsletter_object = Newsletter::create($newsletter_data);
        if ($new_newsletter_object && $new_newsletter_object instanceof Newsletter) {
            return redirect()->back();
        }
    }
}

<?php

namespace App\Http\Controllers\home;

use App\Http\Controllers\Controller;
use App\Models\Blog;
use Illuminate\Http\Request;

class BlogController extends Controller
{
    public function index()
    {
        $blogs = Blog::orderBy('id', 'desc')->paginate(9);
        return view('home.site.blog.all', compact('blogs'));
    }

    public function singleBlog($blog_title)
    {
        $lastBlogs = Blog::orderBy('id', 'desc')->take(3)->get();
        if ($blog_title) {
            $blogItems = Blog::where('title',$blog_title)->first();
            // dd($blogItems);
            if ($blogItems && $blogItems instanceof Blog) {
                return view('home.site.blog.Detail.all', compact('blogItems', 'lastBlogs'));
            }
        }
    }

   


}

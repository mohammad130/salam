<?php

namespace App\Http\Controllers\home;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\VisionAndMission;

class VAndMController extends Controller
{
    public function index()
    {
        $visionAndMissionInfs= VisionAndMission::all();
        return view('home.site.aboutUs.visionandmission.all',compact('visionAndMissionInfs'));
    }
}

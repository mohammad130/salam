<?php

namespace App\Http\Controllers\home;

use App\Http\Controllers\Controller;
use App\Models\AdmisstionProcess;
use Illuminate\Http\Request;

class AdmissionController extends Controller
{
    public function index()
    {
        $AdmisstionProcesses = AdmisstionProcess::all();
        return view('home.site.admission.all',compact('AdmisstionProcesses'));
    }
}

<?php

namespace App\Http\Controllers\home;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class IranController extends Controller
{

    public function culture()
    {
        return view('home.site.iran.culture.all');
    }
}

<?php

namespace App\Http\Controllers\home;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\HowWeCanHelp;

class HelpController extends Controller
{
    public function index()
    {
        $howWeCanHelpInfs= HowWeCanHelp::all();
        return view('home.site.aboutUs.help.all',compact('howWeCanHelpInfs'));
    }
}

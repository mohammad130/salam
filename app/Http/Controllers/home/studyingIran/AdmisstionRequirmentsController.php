<?php

namespace App\Http\Controllers\home\studyingIran;

use App\Http\Controllers\Controller;
use App\Models\AdmisstionRequirement;
use Illuminate\Http\Request;

class AdmisstionRequirmentsController extends Controller
{
    public function index()
    {
        $admisstionRequirments= AdmisstionRequirement::all();
        return view('home.site.studyingInIran.admisstionRequirements.all',compact('admisstionRequirments'));
    }
}

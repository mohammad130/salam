<?php

namespace App\Http\Controllers\home\studyingIran;

use App\Http\Controllers\Controller;
use App\Models\WhyStudying;
use Illuminate\Http\Request;

class WhyStudyingController extends Controller
{
    public function index()
    {
        $whyStudyings= WhyStudying::all();
        return view('home.site.studyingInIran.whyStudying.all',compact('whyStudyings'));
    }
}

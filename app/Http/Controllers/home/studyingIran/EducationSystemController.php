<?php

namespace App\Http\Controllers\home\studyingIran;

use App\Http\Controllers\Controller;
use App\Models\EducationSystem;
use Illuminate\Http\Request;

class EducationSystemController extends Controller
{
    public function index()
    {
        $educationSystems= EducationSystem::all();
        return view('home.site.studyingInIran.educationSystem.all',compact('educationSystems'));
    }
}

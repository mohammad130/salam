<?php

namespace App\Http\Controllers\home;

use App\Http\Controllers\Controller;
use App\Models\Program;
use Illuminate\Http\Request;


    
    class ProgramController extends Controller
    {
      
        public function detail($Program_title)
        {
    //        $lastPrograms = Program::orderBy('id', 'desc')->take(2)->get();
            if ($Program_title) {
                $programItems = Program::where('title',$Program_title)->first();
                if ($programItems && $programItems instanceof Program) {
                    return view('home.site.program.detail', compact('programItems'));
                }
            }
        }
        public function index()
        {
            $programs = Program::all();

            return view('home.site.program.all',compact('programs'));

        }
    
    }
    
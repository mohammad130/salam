<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class WhoWeAreDesk extends Model
{
    protected $primaryKey = 'id';
    protected $guarded = ['id'];
    protected $table = 'who_we_are_desk';
}

<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class VisionAndMission extends Model
{
    protected $primaryKey = 'id';
    protected $guarded = ['id'];
    protected $table = 'vision_and_mission';
}

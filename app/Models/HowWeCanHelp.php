<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class HowWeCanHelp extends Model
{
    protected $primaryKey = 'id';
    protected $guarded = ['id'];
    protected $table = 'how_we_help_you';
}

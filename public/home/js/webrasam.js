$(document).ready(function () {
	$('.box-cta-whatsapp-wrapper').mousedown(function () {
		$(this).fadeOut();
		$('.img-cta-whatsapp').fadeIn();
	});
	$('.img-cta-whatsapp').mousedown(function () {
		$(this).fadeOut();
		$('.box-cta-whatsapp-wrapper').fadeIn();
	});
});

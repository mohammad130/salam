@extends('home.layout.main')
@section('title')
    Salaam Study Iran/Culture
@stop
@section('content')
    <div class="breadcrumb-area">
        <div class="container h-100">
            <div class="row h-100 align-items-end">
                <div class="col-12">
                    <div class="breadcumb--con">
                        <nav aria-label="breadcrumb">
                            <ol class="breadcrumb">
                                <li class="breadcrumb-item"><a href="#"><i class="fa fa-home"></i> Home</a></li>
                                <li class="breadcrumb-item"><a href="#">Culture</a></li>
                            </ol>
                        </nav>
                        <h2 class="title">Culture</h2>

                    </div>

                </div>
            </div>
        </div>
        <!-- <div class="breadcrumb-bg-curve">
            <img src="/home/img/salaam png/curve-5.png" alt="">
        </div> -->
    </div>
    <section class="blog-details-area section-padding-80">
        <div class="container">
            <div class="row">
                <div class="col-12">
                    <div class="blog-details-content">
                        <div class="post-details-text">

                            @foreach($cultures as $culture)
                            <div class="row justify-content-center">
                               
                                <div class="col-12 blog-ax">
                                    <img class="mb-50" src="{{$culture->image}}" alt="Admission Process"
                                         title="admission process" style="margin: 0 auto;display: block;height:400px;">
                                </div>
                                <div class="col-12 col-lg-10 blog-mtn">
                                   <p> {!!$culture->description!!}</p>
                                </div>
                                <div class="col-12 col-lg-12 ">
                                    <div class="uza-video-area hi-icon-effect-8">
                                        <a href="{{$culture->video_link}}" class="hi-icon video-play-btn">Click to watch the video</a>
                                    </div>
                                </div>
                            </div>
                            @endforeach

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
@stop

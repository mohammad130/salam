@extends('home.layout.main')
@section('title')
    Salaam Study Iran| who we are
@stop
@section('stylesheet')
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.4.0/css/font-awesome.min.css">
    <link rel="stylesheet" href="/admin/textarea/css/froala_editor.css">
    <link rel="stylesheet" href="/admin/textarea/css/plugins/code_view.css">
    <link rel="stylesheet" href="/admin/textarea/css/plugins/colors.css">
    <link rel="stylesheet" href="/admin/textarea/css/plugins/emoticons.css">
    <link rel="stylesheet" href="/admin/textarea/css/plugins/image_manager.css">
    <link rel="stylesheet" href="/admin/textarea/css/plugins/image.css">
    <link rel="stylesheet" href="/admin/textarea/css/plugins/line_breaker.css">
    <link rel="stylesheet" href="/admin/textarea/css/plugins/table.css">
    <link rel="stylesheet" href="/admin/textarea/css/plugins/char_counter.css">
    <link rel="stylesheet" href="/admin/textarea/css/plugins/video.css">
    <link rel="stylesheet" href="/admin/textarea/css/plugins/fullscreen.css">
    <link rel="stylesheet" href="/admin/textarea/css/plugins/file.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/codemirror/5.3.0/codemirror.min.css">
    <link rel="stylesheet" href="/admin/textarea/css/froala_style.css">
    
    <style>
        body {
            /*text-align: center;*/
            overflow: unset !important;
        }

        div#editor {
            width: 81%;
            margin: auto;
            text-align: left;
        }
        
    </style>
@stop
@section('content')
    <div class="breadcrumb-area">
        <div class="container h-100">
            <div class="row h-100 align-items-end">
                <div class="col-12">
                    <div class="breadcumb--con  mb-80">
                        <nav aria-label="breadcrumb">
                            <ol class="breadcrumb">
                                <li class="breadcrumb-item"><a href="#"><i class="fa fa-home"></i> Home</a></li>
                                <li class="breadcrumb-item active" aria-current="page">who we are</li>
                            </ol>
                        </nav>
                    </div>
                    <!-- <h2 class="title">Who We Are</h2> -->
                </div>
            </div>
        </div>
<!-- 
        <div class="breadcrumb-bg-curve">
            <img src="/home/img/salaam png/curve-5.png" alt="">
        </div> -->
    </div>

    <section class="blog-details-area">
        <div class="container">
            <div class="row">
                <div class="col-12">
                    <div class="blog-details-content">

                        <div class="post-details-text">
                            @foreach ($whoWeAreInfs as $whoWeAreInf)
                                <div class="row justify-content-center">
                                    <div class="col-12 col-lg-10">
                                        <div class="post-content text-center mb-80">
                                            <h2 style="color:#00a82d;">{{ $whoWeAreInf->title }}</h2>
                                        </div>
                                    </div>
                                    <div class="blog-ax col-12">
                                        <img class="mb-50" src="{{ $whoWeAreInf->image }}" alt="about us" title="about us" >
                                    </div>
                                    <div class="col-12 col-lg-12 blog-mtn">
                                        <p>{!! $whoWeAreInf->description !!}</p>
                                    </div>
                                    <div class="col-12 col-lg-12 ">
                                        <div class="uza-video-area hi-icon-effect-8">
                                            <a href="{{$whoWeAreInf->video_link}}" class="hi-icon video-play-btn">Click to watch the video</a>
                                        </div>
                                    </div>
                                </div>
                            @endforeach
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>







    </section>

@stop
@section('script')
    <script type="text/javascript"
            src="https://cdnjs.cloudflare.com/ajax/libs/codemirror/5.3.0/codemirror.min.js"></script>
    <script type="text/javascript"
            src="https://cdnjs.cloudflare.com/ajax/libs/codemirror/5.3.0/mode/xml/xml.min.js"></script>
    <script type="text/javascript" src="/admin/textarea/js/froala_editor.min.js"></script>
    <script type="text/javascript" src="/admin/textarea/js/plugins/align.min.js"></script>
    <script type="text/javascript" src="/admin/textarea/js/plugins/code_beautifier.min.js"></script>
    <script type="text/javascript" src="/admin/textarea/js/plugins/code_view.min.js"></script>
    <script type="text/javascript" src="/admin/textarea/js/plugins/colors.min.js"></script>
    <script type="text/javascript" src="/admin/textarea/js/plugins/draggable.min.js"></script>
    <script type="text/javascript" src="/admin/textarea/js/plugins/emoticons.min.js"></script>
    <script type="text/javascript" src="/admin/textarea/js/plugins/font_size.min.js"></script>
    <script type="text/javascript" src="/admin/textarea/js/plugins/font_family.min.js"></script>
    <script type="text/javascript" src="/admin/textarea/js/plugins/image.min.js"></script>
    <script type="text/javascript" src="/admin/textarea/js/plugins/file.min.js"></script>
    <script type="text/javascript" src="/admin/textarea/js/plugins/image_manager.min.js"></script>
    <script type="text/javascript" src="/admin/textarea/js/plugins/line_breaker.min.js"></script>
    <script type="text/javascript" src="/admin/textarea/js/plugins/link.min.js"></script>
    <script type="text/javascript" src="/admin/textarea/js/plugins/lists.min.js"></script>
    <script type="text/javascript" src="/admin/textarea/js/plugins/paragraph_format.min.js"></script>
    <script type="text/javascript" src="/admin/textarea/js/plugins/paragraph_style.min.js"></script>
    <script type="text/javascript" src="/admin/textarea/js/plugins/video.min.js"></script>
    <script type="text/javascript" src="/admin/textarea/js/plugins/table.min.js"></script>
    <script type="text/javascript" src="/admin/textarea/js/plugins/url.min.js"></script>
    <script type="text/javascript" src="/admin/textarea/js/plugins/entities.min.js"></script>
    <script type="text/javascript" src="/admin/textarea/js/plugins/char_counter.min.js"></script>
    <script type="text/javascript" src="/admin/textarea/js/plugins/inline_style.min.js"></script>
    <script type="text/javascript" src="/admin/textarea/js/plugins/save.min.js"></script>
    <script type="text/javascript" src="/admin/textarea/js/plugins/fullscreen.min.js"></script>

    <script>
    (function () {
      new FroalaEditor("#edit", {
        pastePlain: true
      })
    })()
  </script>
@stop

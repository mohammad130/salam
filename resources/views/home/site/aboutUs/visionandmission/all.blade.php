@extends('home.layout.main')
@section('title')
    Salaam Study Iran | vission and mission
@stop

@section('content')
    <div class="breadcrumb-area">
        <div class="container h-100">
            <div class="row h-100 align-items-end">
                <div class="col-12">
                    <div class="breadcumb--con">
                        <nav aria-label="breadcrumb">
                            <ol class="breadcrumb">
                                <li class="breadcrumb-item"><a href="#"><i class="fa fa-home"></i> Home</a></li>
                                <li class="breadcrumb-item active" aria-current="page">vission and mission</li>
                            </ol>
                        </nav>
                    </div>
                    <h2 class="title">Vission And Mission</h2>
                </div>
            </div>
        </div>
<!-- 
        <div class="breadcrumb-bg-curve">
            <img src="/home/img/salaam png/curve-5.png" alt="">
        </div> -->
    </div>

    <section class="uza-about-us-area section-padding-80">
        <div class="container">
            @foreach ($visionAndMissionInfs as $visionAndMissionInf)    
                <div class="row align-items-center">
                    <div class="col-12 col-lg-12">
                        <div class="about-us-thumbnail mb-80 blog-ax">
                            <img src="{{$visionAndMissionInf->image}}" alt="salam study" title="salaam study">

                        </div>
                    </div>
                    <div class="col-12 col-lg-12">
                        <div class="about-us-content mb-80">
                            <div class="about-tab-content">
                                <div class="tab-content" id="mona_modelTabContent">
                                    <div class="tab-pane fade show active" id="tab-1" role="tabpanel"
                                        aria-labelledby="tab1">
                                        <div class="tab-content-text blog-mtn">
                                            <p>{!!$visionAndMissionInf->description!!}</p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-12 col-lg-12 ">
                            <div class="uza-video-area hi-icon-effect-8">
                                <a href="{{$visionAndMissionInf->video_link}}" class="hi-icon video-play-btn">Click to watch the video</a>
                            </div>
                        </div>

                    </div>
                </div>
            @endforeach                    
        </div>

      
    </section>



@stop

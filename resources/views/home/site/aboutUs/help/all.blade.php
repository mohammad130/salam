@extends('home.layout.main')
@section('title')
    Salaam Study Iran | how we can help?
@stop

@section('content')
    <div class="breadcrumb-area">
        <div class="container h-100">
            <div class="row h-100 align-items-end">
                <div class="col-12">
                    <div class="breadcumb--con">
                        <nav aria-label="breadcrumb">
                            <ol class="breadcrumb">
                                <li class="breadcrumb-item"><a href="#"><i class="fa fa-home"></i> Home</a></li>
                                <li class="breadcrumb-item active" aria-current="page">how We Can Help?</li>
                            </ol>
                        </nav>
                    </div>
                    <h2 class="title">How We Can Help?</h2>
                </div>
            </div>
        </div>

        <!-- <div class="breadcrumb-bg-curve">
            <img src="/home/img/salaam png/curve-5.png" alt="">
        </div> -->
    </div>






    <section class="uza-why-choose-us-area">
        <div class="container">
            @foreach ($howWeCanHelpInfs as $howWeCanHelpInf)
            <div class="row align-items-center">
               <div class="col-12 col-lg-12  mt-5 mb-80" >
                    <div class="blog-ax">
                        <img src="{{$howWeCanHelpInf->image}}"  alt="about us" title="about us">
                    </div>
                
                </div>
                <div class="col-12 col-lg-12 ">
                    <div class="choose-us-content mb-80">
                        <div class="section-heading blog-mtn">
                            <!-- <h2>how We Can Help</h2> -->
                            <p>{!!$howWeCanHelpInf->description!!}</p>
                        </div>
                    </div>
                </div>
                <div class="col-12 col-lg-12 ">
                    <div class="uza-video-area hi-icon-effect-8">
                        <a href="{{$howWeCanHelpInf->video_link}}" class="hi-icon video-play-btn">Click to watch the video</a>
                    </div>
                </div>

            </div>
            @endforeach
        </div>
    </section>



@stop

@extends('home.layout.main')
@section('title')
    Salaam Study Iran/university
@stop

@section('content')

    <div class="breadcrumb-area">
        <div class="container h-100">
            <div class="row h-100 align-items-end">
                <div class="col-12">
                    <div class="breadcumb--con">
                        <nav aria-label="breadcrumb">
                            <ol class="breadcrumb">
                                <li class="breadcrumb-item"><a href="#"><i class="fa fa-home"></i>Home</a></li>
                                <li class="breadcrumb-item active" aria-current="page">university</li>
                            </ol>
                        </nav>
                    </div>
                    <h2 class="title">universities</h2>

                </div>
            </div>
        </div>

        <!-- <div class="breadcrumb-bg-curve">
                <img src="/home/img/salaam png/curve-5.png" alt="">
            </div> -->
    </div>


    <div class="uza-blog-area section-padding-80">
        <div class="container">
            <div class="row">
                @foreach ($universities as $university)
                    <div class="col-12 col-lg-4">
                        <div class="single-blog-post bg-img mb-80">

                            <div class="blog-kadr post-uni">
                                <img src="{{$university->image}}" alt="university of salaam study"
                                    title="university of salaam study">

                                <a href="{{route('home.singleUniversity',$university->title)}}" class="post-title">{{$university->title}}</a>
                                <!-- <p>Lorem ipsum dolor sit, amet consectetur adipisicing elit. Quis alias quos id magni. Commodi laborum sequi deserunt cum ex quaerat.</p> -->
                                <a href="{{route('home.singleUniversity',$university->title)}}" class="read-more-btn">Read More <i class="arrow_carrot-2right"></i></a>
                            </div>
                        </div>
                    </div>
                @endforeach
            </div>

        </div>
    </div>

@stop

@extends('home.layout.main')
@section('title')
    Salaam Study Iran/contact us
@stop

@section('content')
    <div class="breadcrumb-area">
        <div class="container h-100">
            <div class="row h-100 align-items-end">
                <div class="col-12">
                    <div class="breadcumb--con">
                        
                        <nav aria-label="breadcrumb">
                            <ol class="breadcrumb">
                                <li class="breadcrumb-item"><a href="#"><i class="fa fa-home"></i> Home</a></li>
                                <li class="breadcrumb-item active" aria-current="page">Contact Us</li>
                            </ol>
                        </nav>
                        <h2 class="title">Contact Us</h2>
                    </div>
                </div>
            </div>
        </div>

    </div>


    <section class="uza-contact-area section-padding-80">
        <div class="container">
            @if(session('contactsError'))
                <div class="alert alert-success text-center">
                    <p>
                        {{session('contactsError')}}
                    </p>
                </div>
            @endif()
            <div class="row justify-content-between">

                <div class="col-12 col-lg-8">
                    <div class="uza-contact-form mb-80">
                        <div class="contact-heading mb-50">
                            <h4>Please fill out the form below</h4>
                        </div>
                        <form action="#" method="post">
                            {{csrf_field()}}
                            <div class="row">
                                <div class="col-lg-6">
                                    <div class="form-group">
                                        <input type="text" class="form-control mb-30" name="name"
                                               placeholder="Name">
                                    </div>
                                </div>
                                <div class="col-lg-6">
                                    <div class="form-group">
                                        <input type="text" class="form-control mb-30" name="title" placeholder="Title">
                                    </div>
                                </div>
                                <div class="col-lg-6">
                                    <div class="form-group">
                                        <input type="text" class="form-control mb-30" name="phone"
                                               placeholder="Phone Number">
                                    </div>
                                </div>
                                <div class="col-lg-6">
                                    <div class="form-group">
                                        <input type="email" class="form-control mb-30" name="email" placeholder="Email">
                                    </div>
                                </div>


                                <div class="col-12">
                                    <div class="form-group">
                                        <textarea class="form-control mb-30" name="description" rows="8" cols="80"
                                                  placeholder="Message"></textarea>
                                    </div>
                                </div>
                                <div class="col-12">
                                    <button type="submit" class="btn uza-btn btn-3 mt-15">Send</button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>

                <div class="col-12 col-lg-3">
                    <div class="contact-sidebar-area mb-80">

                        <div class="single-contact-card mb-50">
                            <h6>Contact Us:</h6>
                            <h5 class="mt-3">+98-910-7113319</h5>
                        </div>

                        <div class="single-contact-card mb-50">
                            <h6>Email:</h6>
                            <h5 class="mt-3">SalaamStudy@gmail.com</h5>
                        </div>

                        <div class="single-contact-card mb-50">
                            <h6>Address:</h6>
                            <h5 class="mt-3">Tehran--Iran</h5>
                        </div>
                        <div class="single-contact-card mb-50">
                            <h6>Follow Us:</h6>       
                            <div class="single-footer-widget mb-80 mt-3">
                                <div class="footer-social-info">
                                <a href="#" class="facebook" data-toggle="tooltip" data-placement="top" title="facebook"><i
                                class="fa fa-facebook"></i></a>
                                <a href="#" class="youtube" data-toggle="tooltip" data-placement="top" title="Instagram"><i
                                class="fa fa-instagram"></i></a>
                      
                                <a href="#" class="instagram" data-toggle="tooltip" data-placement="top" title="linkedin"><i
                                class="fa fa-linkedin"></i></a>
                                <a href="#" class="instagram" data-toggle="tooltip" data-placement="top" title="twitter"><i
                                class="fa fa-twitter"></i></a>
                                <a href="#" class="youtube" data-toggle="tooltip" data-placement="top" title="YouTube"><i
                                class="fa fa-youtube-play"></i></a>          
                        </div>
                        </div>

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
@stop

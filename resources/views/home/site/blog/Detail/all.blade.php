@extends('home.layout.main')
@section('title')
    Salaam Study Iran/Ditails
@stop

@section('content')

    <div class="breadcrumb-area">
        <div class="container h-100">
            <div class="row h-100 align-items-end">
                <div class="col-12">
                    <div class="breadcumb--con mb-100">
                        <nav aria-label="breadcrumb">
                            <ol class="breadcrumb">
                                <li class="breadcrumb-item"><a href="#"><i class="fa fa-home"></i> Home</a></li>
                                <li class="breadcrumb-item"><a href="#">Blog</a></li>
                                <li class="breadcrumb-item active" aria-current="page">
                                    {{old('name',isset($blogItems) ? $blogItems->title : '')}}
                                </li>
                            </ol>
                        </nav>
                    </div>
                    <!-- <h2 class="title" style="margin-top:30px;">Details Of Blog</h2> -->
                </div>
            </div>
        </div>

        <!-- <div class="breadcrumb-bg-curve">
            <img src="/home/img/salaam png/curve-5.png" alt="">
        </div> -->
    </div>


    <section class="blog-details-area " style="margin-bottom:120px;">
        <div class="container">
            <div class="row">
                <div class="col-12">
                    <div class="blog-details-content">

                        <div class="post-details-text">
                            <div class="row justify-content-center">
                                <div class="col-12 col-lg-10">
                                    <div class="post-content text-center">
                                        <h4 class="titrtitle">{{old('name',isset($blogItems) ? $blogItems->title : '')}}</h4>
                                    </div>
                                </div>
                                <div class="blog-ax col-12">
                                    <img class="mb-50" src="{{old('name',isset($blogItems) ? $blogItems->image : '')}}" alt="blog of salaam study" title="blog of salaam study">
                                </div>
                                <div class="blog-mtn col-12 col-lg-10">
                                    <p>{!!old('name',isset($blogItems) ? $blogItems->description : '')!!}</p>

                                    <div class="col-12 col-lg-12 ">

                                        <div class="uza-video-area hi-icon-effect-8">
                                            <a href="{{old('name',isset($blogItems) ? $blogItems->video_link : '')}}" class="hi-icon video-play-btn">Click to watch the video</a>
                                        </div>
                                     </div>
                                    <div class="d-flex align-items-center justify-content-between">


<div class="uza-post-share d-flex align-items-center">
<h6 class="mb-0 mr-3">Share:</h6>
<div class="social-info-">
<a href="#"><i class="fa fa-facebook" aria-hidden="true"></i></a>
<a href="#"><i class="fa fa-twitter" aria-hidden="true"></i></a>
<a href="#"><i class="fa fa-google-plus" aria-hidden="true"></i></a>
<a href="#"><i class="fa fa-linkedin" aria-hidden="true"></i></a>
<a href="#"><i class="fa fa-envelope" aria-hidden="true"></i></a>
</div>
</div>
</div>

                                    <div class="related-news-area">
                                        <h2 class="mb-4">Relatest Post</h2>
                                        <div class="row">
                                        @foreach($lastBlogs as $lastBlog)
                                        <div class="col-12 col-lg-4">
                                            <div class="single-blog-post bg-img mb-80" style="background-image: url({{$lastBlog->image}});">
                        
                                                <div class="blog-kadr post-content">
                                                <img src="{{$lastBlog->image}}" alt="blog of salaam study" title="blog of salaam study">
                        
                                                    <a href="{{route('home.singleBlog',$lastBlog->title)}}" class="post-title">{{$lastBlog->title}}</a>
                                                    <p>{{substr($lastBlog->shortDescription,0,80)}} ...</p>
                                                    <a href="{{route('home.singleBlog',$lastBlog->title)}}" class="read-more-btn">Read More <i class="arrow_carrot-2right"></i></a>
                                                </div>
                                            </div>
                                        </div>
                                        @endforeach
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            

        </div>
        </div>
    </section>

@stop

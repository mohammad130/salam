@extends('home.layout.main')
@section('title')
    Salaam Study Iran/blog
@stop

@section('content')

    <div class="breadcrumb-area">
        <div class="container h-100">
            <div class="row h-100 align-items-end">
                <div class="col-12">
                    <div class="breadcumb--con">
                        <nav aria-label="breadcrumb">
                            <ol class="breadcrumb">
                                <li class="breadcrumb-item"><a href="#"><i class="fa fa-home"></i>Home</a></li>
                                <li class="breadcrumb-item active" aria-current="page">Blog</li>
                            </ol>
                        </nav>
                    </div>
                    <h2 class="title">Blogs</h2>

                </div>
            </div>
        </div>

        <!-- <div class="breadcrumb-bg-curve">
            <img src="/home/img/salaam png/curve-5.png" alt="">
        </div> -->
    </div>


    <div class="uza-blog-area section-padding-80">
        <div class="container">
            <div class="row">

            @foreach($blogs as $blog)
                <div class="col-12 col-lg-4">
                    <div class="single-blog-post bg-img mb-80" style="background-image: url({{$blog->image}});">

                        <div class="kadr-b post-content">
                        <img src="{{$blog->image}}" alt="blog of salaam study" title="blog of salaam study">

                            <a href="{{route('home.singleBlog',$blog->title)}}" class="post-title">{{$blog->title}}</a>
                            <p>{{substr($blog->shortDescription,0,90)}} ...</p>
                            <a href="{{route('home.singleBlog',$blog->title)}}" class="read-more-btn">Read More <i class="arrow_carrot-2right"></i></a>
                        </div>
                    </div>
                </div>
            @endforeach
            </div>
            <div class="pagination_number">
                {{$blogs->links()}}
            </div>
        </div>
    </div>



@stop

@extends('home.layout.main')
@section('title')
    Salaam Study Iran| why studying in Iran
@stop

@section('content')
    <div class="breadcrumb-area">
        <div class="container h-100">
            <div class="row h-100 align-items-end">
                <div class="col-12">
                    <div class="breadcumb--con">
                        <nav aria-label="breadcrumb">
                            <ol class="breadcrumb">
                                <li class="breadcrumb-item"><a href="#"><i class="fa fa-home"></i> Home</a></li>
                                <li class="breadcrumb-item active" aria-current="page">Why Studying In Iran?</li>
                            </ol>
                        </nav>
                    </div>
                    <h2 class="title">Why Studying in Iran?</h2>
                </div>
            </div>
        </div>

        <!-- <div class="breadcrumb-bg-curve">
            <img src="/home/img/salaam png/curve-5.png" alt="">
        </div> -->
    </div>

    <section class="blog-details-area section-padding-80">
        <div class="container">
            <div class="row">
                <div class="col-12">
                    <div class="blog-details-content">

                        <div class="post-details-text">
                            @foreach($whyStudyings as $whyStudying)
                            <div class="row justify-content-center">
                                <!-- <div class="col-12 col-lg-10">
                                    <div class="post-content text-center mb-50">
                                        <h2>{{$whyStudying->title}}</h2>
                                    </div>
                                </div> -->
                                <div class="col-12 blog-ax">
                                    <img class="mb-50" src="{{$whyStudying->image}}" alt="why studying in iran?" title="why studying in iran?" >
                                </div>
                                <div class="col-12 col-lg-10 blog-mtn">
                                    <p>{!!$whyStudying->description!!}</p>
                                </div>
                                <div class="col-12 col-lg-12 ">
                                    <div class="uza-video-area hi-icon-effect-8">
                                        <a href="{{$whyStudying->video_link}}" class="hi-icon video-play-btn">Click to watch the video</a>
                                    </div>
                                </div>
                            </div>
                                @endforeach
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

@stop

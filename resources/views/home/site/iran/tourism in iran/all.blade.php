@extends('home.layout.main')
@section('title')
    Salaam Study Iran/tourism in iran
@stop

@section('content')

    <div class="breadcrumb-area">
        <div class="container h-100">
            <div class="row h-100 align-items-end">
                <div class="col-12">
                    <div class="breadcumb--con">
                        <h2 class="title">Tourism In Iran</h2>
                        <nav aria-label="breadcrumb">
<ol class="breadcrumb">
<li class="breadcrumb-item"><a href="#"><i class="fa fa-home"></i> Home</a></li>
<li class="breadcrumb-item"><a href="#">living in iran</a></li>
<li class="breadcrumb-item active" aria-current="page">Tourism In Iran</li>
</ol>
</nav>
</div>
</div>
</div>
</div>

        <div class="breadcrumb-bg-curve">
<img src="/home/img/salaam png/curve-5.png" alt="">
</div>
</div>


<section class="blog-details-area section-padding-80">
<div class="container">
<div class="row">
<div class="col-12">
<div class="blog-details-content">

<div class="post-details-text">
<div class="row justify-content-center">
<div class="col-12 col-lg-10">
<div class="post-content text-center mb-50">
<h4>Tourism In Iran</h4>
</div>
</div>
<div class="col-12">
<img class="mb-50" src="/home/img/bg-img/14.jpg" alt="tourism in iran" title="tourism in iran">
</div>
<div class="col-12 col-lg-10">
<p>Iran is a large country located in the western Asia. It is bordered to the north by the Caspian Sea and the south by the Persian Gulf. With an area of 1,648,195 square kilometers, Iran is the 17th largest country in the world. Tehran, the capital city of Iran, is the country's largest and most populated city. Isfahan, Shiraz, Tabriz and Mashhad are other major cities of the country.
Persian (Farsi) is the official language of the country and is widely spoken. A large number of people also speak other languages/dialects namely Azeri, Kurdish, Luri, Arabic, Baluchi, Gilaki, Mazandarani/Tabari, and Turkmen.
</p>
<p>
Iran is one of the world’s oldest civilizations, and has been among the world’s most thoughtful and complex civilizations from the very beginning. Iran is the land of four seasons, history and culture, souvenir and authenticity. This is not a tourism slogan, this is the reality inferred from the experience of visitors who have been impressed by Iran’s beauties and amazing attractions. Antiquity and richness of its culture and civilization, the variety of natural and geographical attractions, four - season climate, diverse cultural sites in addition to different tribes with different and fascinating traditions and customs have made Iran as a treasury of tangible and intangible heritage. </p>
<p>Different climates can be found simultaneously in Iran. Some cities have summer weather in winter, or have spring or autumn weather; at the same time in summer you might find some regions covered with snow, icicles or experiencing rain and breeze of spring. Iran is the land of history and culture, not only because of its Pasargad and Persepolis, Chogha Zanbil, Naqsh-e Jahan Square, Yazd and Shiraz, Khuzestan and Isfahan, and its tangible heritage inscribed in the UNESCO World Heritage List; indeed its millennial civilization and thousands historical and archeological monuments and sites demonstrate variety and value of religious and spiritual heritage, rituals, intact traditions of this country as a sign of authenticity and splendor. Iran with 24 UNESCO World Heritage Sites is one of the 10 countries with the most Cultural Heritage Sites in the UNESCO’s World Heritage list.</p>

<div class="d-flex align-items-center justify-content-between">
<div class="uza-post-share d-flex align-items-center">
<h6 class="mb-0 mr-3">Share:</h6>
<div class="social-info-">
<a href="#"><i class="fa fa-facebook" aria-hidden="true"></i></a>
<a href="#"><i class="fa fa-twitter" aria-hidden="true"></i></a>
<a href="#"><i class="fa fa-google-plus" aria-hidden="true"></i></a>
<a href="#"><i class="fa fa-linkedin" aria-hidden="true"></i></a>
<a href="#"><i class="fa fa-envelope" aria-hidden="true"></i></a>
</div>
</div>
</div>

</div>
        </div>
    </section>
@stop

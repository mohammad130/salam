@extends('home.layout.main')
@section('title')
    Salaam Study Iran/ About Iran
@stop

@section('content')


    <div class="breadcrumb-area">
        <div class="container h-100">
            <div class="row h-100 align-items-end">
                <div class="col-12">
                    <div class="breadcumb--con">
                        <h2 class="title">Living In Iran</h2>
                        <nav aria-label="breadcrumb">
                            <ol class="breadcrumb">
                                <li class="breadcrumb-item"><a href="#"><i class="fa fa-home"></i> Home</a></li>
                                <li class="breadcrumb-item active" aria-current="page">Living In Iran</li>
                                <li class="breadcrumb-item active" aria-current="page">About Iran</li>
                            </ol>
                        </nav>
                    </div>
                </div>
            </div>
        </div>

        <div class="breadcrumb-bg-curve">
            <img src="/home/img/salaam png/curve-5.png" alt="">
        </div>
    </div>


    <section class="uza-about-us-area section-padding-80">
        <div class="container">
            <div class="row align-items-center">

                <div class="col-12 col-lg-6">
                    <div class="about-us-thumbnail mb-80">
                        <img src="/home/img/bg-img/10.jpg" alt="about iran"
                             title="این عکس به درباره ی ایران مربوط می باشد">
                    </div>
                </div>

                <div class="col-12 col-lg-6">
                    <div class="section-heading mb-5">
                        <h2>About Iran</h2>
                    </div>
                    <div class="about-us-content mb-80">

                        <div class="about-tab-content">
                            <div class="tab-content" id="mona_modelTabContent">
                                <div class="tab-pane fade show active" id="tab-1" role="tabpanel"
                                     aria-labelledby="tab1">

                                    <div class="tab-content-text">
                                        <p>Iran is one of the oldest civilizations in the world with the history of more
                                            than 4000 years. According to official statistics, Iran is the 18th largest
                                            country and the 17th most populated nation in the world. As Asia’s fourth
                                            largest UNESCO World Heritage Site, Iran is well-known for its art,
                                            architecture, and poetry and every year lots of tourists come Iran to visit
                                            its ancient archaeological and cultural sites. In addition, there are
                                            diverse natural tourist sites around the country that attract lots of
                                            visitors every year. The country is home to many different ethnic, cultural
                                            and religious groups that are offered freedom to practice and carry out
                                            their own beliefs and ways.</p>

                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="about-bg-pattern">
            <img src="/home/img/salaam png/curve-2.png" alt="about iran" title="این تصویر درباره ی ایران می باشد">
        </div>
    </section>

    <section class="blog-details-area section-padding-80">
        <div class="container">
            <div class="row">
                <div class="col-12">
                    <div class="blog-details-content">

                        <div class="post-details-text">
                            <div class="row justify-content-center">
                                <div class="col-12 col-lg-10">
                                    <h4 style="color:black;">In general, studying in Iran leads to an excellent
                                        experience for you and you will leave Iran with lots of good memories.
                                        In following, we provide some facts about Iran:
                                    </h4>
                                    <p>1. Iran is one of the oldest nations in the world. The country’s first great
                                        city, Susa, was built on the central plateau around 3200 B.C.
                                        2. Iran, formerly known as Persia, is situated at the crossroads of Central
                                        Asia, South Asia, and the Arab states of the Middle East. The name “Iran” means
                                        “land of the Aryans.”
                                        3. Iran is a republic in Central Asia, sharing a border with Armenia,
                                        Azerbaijan, the Caspian Sea and Turkmenistan on the north, Afghanistan and
                                        Pakistan on the east, Turkey and Iraq on the west, and the Persian Gulf and the
                                        Gulf of Oman on the south.
                                        4. It has been officially known as the Islamic Republic of Iran since the
                                        overthrow of the Shah in 1979.
                                        5. Tehran is the capital and largest city of Iran. Other large metropolitan
                                        areas include, but are not limited to, Mashhad, Isfahan, Tabriz, Shiraz, Ahwaz,
                                        Yazd and Hamadan.
                                        6. Iran has a population about of 82 million (median age 28) and covers an area
                                        that is 636,372 square miles (1,648,195 square kilometers).
                                        7. Official language of instruction in Iran is Farsi/Persian. English and Arabic
                                        are taught in most schools.
                                        8. According to 2015 estimates, the literacy rates of total population age 15
                                        and over is 86.8% of which 92.1% are male and 82.5% are female.
                                        9. According to 2013 reports, Iran spends 3.7% of GDP on education.
                                        10. Climatic and weather conditions in the country are diverse due to the vast
                                        land area that Iran covers. It is common to refer to Iran as a country of four
                                        seasons. </p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>


    <section class="uza-portfolio-area section-padding-80">
        <div class="container">
            <div class="row">

                <div class="col-12">
                    <div class="section-heading text-center">
                        <h2>Picture Of Iran</h2>
                    </div>
                </div>
            </div>
        </div>
        <div class="container-fluid">
            <div class="row">

                <div class="team-sildes owl-carousel">

                    <div class="single-team-slide">
                        <img src="/home/img/bg-img/t.jpg" alt="iran" title="ایران" style="height:300px">

                        <div class="overlay-effect">
                            <h4>Tehran</h4>
                        </div>
                    </div>

                    <div class="single-team-slide">
                        <img src="/home/img/bg-img/sh.jpg" alt="iran" title="ایران" style="height:300px">

                        <div class="overlay-effect">
                            <h4>Shiraz</h4>
                        </div>

                    </div>

                    <div class="single-team-slide">
                        <img src="/home/img/bg-img/m.jpg" alt="iran" title="ایران" style="height:300px">

                        <div class="overlay-effect">
                            <h4>Mashhad</h4>
                        </div>

                    </div>

                    <div class="single-team-slide">
                        <img src="/home/img/bg-img/i.jpg" alt="iran" title="ایران" style="height:300px">

                        <div class="overlay-effect">
                            <h4>Isfahan</h4>
                        </div>


                    </div>

                    <div class="single-team-slide">
                        <img src="/home/img/bg-img/ma.jpg" alt="iran" title="ایران" style="height:300px">

                        <div class="overlay-effect">
                            <h4>Mazandaran</h4>
                        </div>


                    </div>

                    <div class="single-team-slide">
                        <img src="/home/img/bg-img/q.jpg" alt="iran" title="ایران" style="height:300px">

                        <div class="overlay-effect">
                            <h4>Qeshm</h4>
                        </div>

                    </div>

                    <div class="single-team-slide">
                        <img src="/home/img/bg-img/gol.jpg" alt="iran" title="ایران" style="height:300px">

                        <div class="overlay-effect">
                            <h4>Golestan</h4>
                        </div>


                    </div>

                    <div class="single-team-slide">
                        <img src="/home/img/bg-img/ta.jpg" alt="iran" title="ایران" style="height:300px">

                        <div class="overlay-effect">
                            <h4>Tabriz</h4>
                        </div>


                    </div>
                </div>
            </div>
        </div>


    </section>
@stop

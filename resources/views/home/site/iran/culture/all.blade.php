@extends('home.layout.main')
@section('title')
    Salaam Study Iran/culture
@stop

@section('content')

    <div class="breadcrumb-area">
        <div class="container h-100">
            <div class="row h-100 align-items-end">
                <div class="col-12">
                    <div class="breadcumb--con">
                        <h2 class="title">Culture</h2>
                        <nav aria-label="breadcrumb">
<ol class="breadcrumb">
<li class="breadcrumb-item"><a href="#"><i class="fa fa-home"></i> Home</a></li>
<li class="breadcrumb-item"><a href="#">Living In Iran</a></li>
<li class="breadcrumb-item active" aria-current="page">Culture</li>
</ol>
</nav>
</div>
</div>
</div>
</div>

        <div class="breadcrumb-bg-curve">
<img src="/home/img/salaam png/curve-5.png" alt="">
</div>
</div>


<section class="blog-details-area section-padding-80">
<div class="container">
<div class="row">
<div class="col-12">
<div class="blog-details-content">

<div class="post-details-text">
<div class="row justify-content-center">
<div class="col-12 col-lg-10">
<div class="post-content text-center mb-50">
<h4>Culture</h4>
</div>
</div>
<div class="col-12">
<img class="mb-50" src="/home/img/bg-img/14.jpg" alt="culture" title="culture of iran">
</div>
<div class="col-12 col-lg-10">
<p>Lorem ipsum dolor, sit amet consectetur adipisicing elit. Enim, iure molestiae! Eius sint excepturi quidem animi consectetur commodi libero sunt. Minima recusandae cupiditate, itaque asperiores nisi eum quam adipisci quaerat?</p>
<p>
Lorem ipsum, dolor sit amet consectetur adipisicing elit. Consequuntur cupiditate inventore iure alias? Nihil in unde et accusamus alias consequatur doloremque rerum ipsum atque sunt, eos deserunt ratione, a molestias!</p>
<p>Lorem ipsum dolor sit amet consectetur, adipisicing elit. Reprehenderit perspiciatis voluptate dignissimos repellat quod enim odit maiores aspernatur optio, expedita architecto aperiam. Iusto atque possimus facere quis tempore quos alias excepturi hic voluptatem eius in voluptatum ullam soluta, et error.</p>

<div class="d-flex align-items-center justify-content-between">


<div class="uza-post-share d-flex align-items-center">
<h6 class="mb-0 mr-3">Share:</h6>
<div class="social-info-">
<a href="#"><i class="fa fa-facebook" aria-hidden="true"></i></a>
<a href="#"><i class="fa fa-twitter" aria-hidden="true"></i></a>
<a href="#"><i class="fa fa-google-plus" aria-hidden="true"></i></a>
<a href="#"><i class="fa fa-linkedin" aria-hidden="true"></i></a>
<a href="#"><i class="fa fa-envelope" aria-hidden="true"></i></a>
</div>
</div>
</div>

</div>
        </div>
    </section>
@stop

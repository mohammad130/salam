@extends('home.layout.main')
@section('title')
    Salaam Study Iran|Study in Iran
@stop

@section('preLoader')
    <div id="preloader">
        <div class="wrapper">
            <div class="cssload-loader"></div>
        </div>
    </div>
@stop

@section('content')

    <!-- slider one  -->
    <section class="welcome-area">
        <div class="welcome-slides owl-carousel">


            @foreach ($sliders as $slider)
                <div class="single-welcome-slide">

                    <!-- <div class="background-curve">
                        <img src="/home/img/salaam png/curve-1.png" alt="">
                    </div> -->

                    <div class="welcome-content h-100 mt-5">
                        <div class="container h-100">
                            <div class="row h-100 align-items-center">

                                <div class="col-12 col-md-6">
                                    <div class="welcome-text">
                                        <h2 data-animation="fadeInUp" data-delay="100ms">{{ $slider->title }}</h2>
                                    </div>
                                </div>

                                <div class="col-12 col-md-6">
                                    <div class="welcome-thumbnail" style="padding:55px">
                                        <img class="welcome-s-i" src="{{ $slider->image }}" style="height: auto" alt="salam study"
                                            title="salam study" data-animation="slideInRight" data-delay="100ms">
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            @endforeach
        </div>
    </section>
    <!-- end slider one  -->

<!-- start about us in home  -->
<section class="uza-about-us-area" style="margin-top:50px;">
    <div class="container">
            <div class="row align-items-center">
                <div class="col-12 col-md-12">
                    <div class="about-us-content mb-80">
                        <h2 style="text-align:center;margin:40px 0;">Who We Are?</h2>
                            <ul>
                                @foreach ($whoWeAreDesks as $whoWeAreDesk)
                                <li><a href="#" style="color: #234656;font-size:22px;font-weight:500;"><i class="fa fa-circle mr-1" style="color:#00a82d;font-size:20px;"></i>{{$whoWeAreDesk->text}}</a></li>
                                @endforeach
                            </ul>                     
                    </div>
                </div>
            </div>

    </div>
</section>
<!-- end about us in home  --> 





    <!-- services  -->

    <section class="uza-services-area">
        <div class="container">
            <div class="row">

                <div class="col-12">
                    <div class="section-heading text-center">
                        <h2>- Our Services -</h2>
                    </div>
                </div>
                <div class="row">
                    @foreach ($services as $service)
                        <div class="col-12 col-lg-3 col-md-6">
                            <a href="{{route('home.singleService',$service->id)}}">
                                <div class="single-service-area mb-80">
                                    <div class="service-icon">
                                        <i class="icon_cone_alt"></i>
                                    </div>
                                    <h5>{{ $service->title }}</h5>
                                </div>
                            </a>
                        </div>
                    @endforeach
                </div>
            </div>


    </section>
    <!-- end----services  -->
    <!-- start testinonials  -->
    <div class="section-heading text-center">
        <h2>- What Students Say About Us -</h2>
    </div>
    <div class="clients-feedback-area mt-80 clearfix">
        <div class="container">
            <div class="row">
                <div class="col-12">

                    <div class="testimonial-slides owl-carousel">
                        @foreach ($testimonials as $testimonial)
                            <div class="single-testimonial-slide d-flex align-items-center">

                                <div class="testimonial-thumbnail">
                                <a href="{{ route('home.student', $testimonial->id) }}"> <img src="{{ $testimonial->image }}" alt="testimoniials of salam study"
                                        title="testimonials of salam study"></a>
                                </div>

                                <div class="testimonial-content">
                                    <h4>{!! substr($testimonial->shortDescription, 0, 200) !!} ...</h4>


                                    <div class="author-info">
                                    <a href="{{ route('home.student', $testimonial->id) }}" class="post-title blog-kadr " style="height: 35px; margin-top: 25px">{{ $testimonial->title }}</a>
                                    <br>
                                    <a href="{{ route('home.student', $testimonial->id) }}" class="blog-kadr" style="height: 35px; margin-top: 25px">{{ $testimonial->lesson }}</a>

                                    </div>

                                    <div class="quote-icon"><img src="/home/img/salaam png/quote.png" alt=""></div>
                                </div>
                            </div>
                        @endforeach

                    </div>
                </div>
            </div>
        </div>
        </section>
        <!-- end testimonials  -->






        <!-- start blog -->
        <section class="uza-blog-area" style="padding-top: 40px;">

            <!-- <div class="blog-bg-curve">
                <img src="/home/img/salaam png/curve-4.png" alt="">
            </div> -->


            <div class="container">
                <div class="row">

                    <div class="col-12">
                        <div class="section-heading text-center mt-5">
                            <h2>- Our Latest Blogs -</h2>
                        </div>
                    </div>
                    <div class="row">
                        @foreach ($blogs as $blog)  
                                <div class="col-12 col-lg-4">
                                    <div class="single-blog-post bg-img mb-80"
                                        >
                                        <div class="bg-blogi post-content">
                                            <a href="{{ route('home.singleBlog', $blog->id) }}"
                                                class="post-title">{{ $blog->title }}</a>
                                            <a href="{{ route('home.singleBlog', $blog->id) }}"><img src="{{ $blog->image }}"  alt="blog of salaam study"
                                                title="blog of salaam study">
                                            </a>
                                            <p>{{ substr($blog->shortDescription, 0, 110) }} ..</p>
                                            <a href="{{ route('home.singleBlog', $blog->id) }}" class="read-more-btn">Read
                                                More <i class="arrow_carrot-2right"></i></a>
                                        </div>
                                    </div>
                                </div>
                        @endforeach
                    </div>
                </div>
        </section>
        <!-- end blog  -->


        <div class="col-12">
            <div class="section-heading text-center">
                <h2 style="margin-top:100px;">- Featured Universities -</h2>
            </div>
        </div>
    </div>
    <div class="container-fluid">
        <div class="row">

            <div class="portfolio-sildes owl-carousel">
                @foreach ($universities as $university)
                    <div class="single-portfolio-slide">
                            <img src="{{ $university->image }}" alt="Feauture Univercity In Iran"
                            title="Feauture Univercity In Iran">
                        <h5 style="text-align:center;margin-top:10px;">{{ $university->title }}</h5>
                        <div class="overlay-effect">
                            <h4>{{ $university->title }}</h4>
                            <p>{{ substr($university->shortDescription, 0, 200) }}</p>
                        </div>

                        <div class="view-more-btn">
                            <a href="{{ route('home.singleUniversity', $university->id) }}"><i
                                    class="arrow_right"></i></a>
                        </div>
                    </div>
                @endforeach

            </div>
        </div>
    </div>


    <div class="container">
    </div>

    <!-- <div class="portfolio-bg-curve">
                    <img src="/home/img/salaam png/curve-3.png" alt="">
                </div> -->
    </section>
    <!-- end univercity  -->

    <!-- start program  -->
    <div class="section-heading text-center">
        <h2 style="margin-top:100px;">- programs -</h2>
    </div>
    <section class="ftco-section ftco-no-pb ftco-no-pt ftco-services bg-light" id="services-section">
        <div class="container">

            <div class="row no-gutters">
                <div class="col-md-4 ftco-animate py-5 nav-link-wrap">
                    <div class="nav flex-column nav-pills" id="v-pills-tab" role="tablist" aria-orientation="vertical">
                        <a class="nav-link px-4 active" id="v-pills-1-tab" data-toggle="pill" href="#v-pills-1" role="tab"
                            aria-controls="v-pills-1" aria-selected="true"><span class="mr-3 flaticon-ideas"></span>MBBS
                            </a>
                        <a class="nav-link px-4" id="v-pills-3-tab" data-toggle="pill" href="#v-pills-3" role="tab"
                            aria-controls="v-pills-3" aria-selected="false"><span
                                class="mr-3 flaticon-analysis"></span>Dentistry</a>
                        <a class="nav-link px-4" id="v-pills-4-tab" data-toggle="pill" href="#v-pills-4" role="tab"
                            aria-controls="v-pills-4" aria-selected="false"><span
                                class="mr-3 flaticon-web-design"></span>Pharmacy</a>
                        <a class="nav-link px-4" id="v-pills-4-tab" data-toggle="pill" href="#v-pills-5" role="tab"
                            aria-controls="v-pills-5" aria-selected="false"><span
                                class="mr-3 flaticon-ux-design"></span>Business and Management</a>
                        <a class="nav-link px-4" id="v-pills-5-tab" data-toggle="pill" href="#v-pills-6" role="tab"
                            aria-controls="v-pills-6" aria-selected="false"><span
                                class="mr-3 flaticon-innovation"></span>Engineering</a>
                        <a class="nav-link px-4" id="v-pills-6-tab" data-toggle="pill" href="#v-pills-7" role="tab"
                            aria-controls="v-pills-7" aria-selected="false"><span
                                class="mr-3 flaticon-idea"></span>Low</a>
                        <a class="nav-link px-4" id="v-pills-6-tab" data-toggle="pill" href="#v-pills-8" role="tab"
                            aria-controls="v-pills-8" aria-selected="false"><span class="mr-3 flaticon-idea"></span>Fine Art</a>
                    </div>
                </div>
                <div class="col-md-8 ftco-animate p-4 p-md-5 d-flex align-items-center">
                    <div class="tab-content pl-md-5" id="v-pills-tabContent">
                        <div class="tab-pane fade show active py-5" id="v-pills-1" role="tabpanel"
                            aria-labelledby="v-pills-1-tab">
                            <span class="icon mb-3 d-block flaticon-ideas"></span>
                            <h2 class="mb-4">MBBS</h2>
                            <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Nesciunt voluptate, quibusdam sunt
                                iste dolores consequatur</p>
                            <p>Inventore fugit error iure nisi reiciendis fugiat illo pariatur quam sequi quod iusto facilis
                                officiis nobis sit quis molestias asperiores rem, blanditiis! Commodi exercitationem vitae
                                deserunt qui nihil ea, tempore et quam natus quaerat doloremque.</p>
                            <p><a href="#" class="btn btn-primary px-4 py-3">Learn More</a></p>
                        </div>

                        <div class="tab-pane fade py-5" id="v-pills-3" role="tabpanel" aria-labelledby="v-pills-3-tab">
                            <span class="icon mb-3 d-block flaticon-analysis"></span>
                            <h2 class="mb-4">Dentistry</h2>
                            <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Nesciunt voluptate, quibusdam sunt
                                iste dolores consequatur</p>
                            <p>Inventore fugit error iure nisi reiciendis fugiat illo pariatur quam sequi quod iusto facilis
                                officiis nobis sit quis molestias asperiores rem, blanditiis! Commodi exercitationem vitae
                                deserunt qui nihil ea, tempore et quam natus quaerat doloremque.</p>
                            <p><a href="#" class="btn btn-primary px-4 py-3">Learn More</a></p>
                        </div>
                        <div class="tab-pane fade py-5" id="v-pills-4" role="tabpanel" aria-labelledby="v-pills-4-tab">
                            <span class="icon mb-3 d-block flaticon-web-design"></span>
                            <h2 class="mb-4">Pharmacy</h2>
                            <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Nesciunt voluptate, quibusdam sunt
                                iste dolores consequatur</p>
                            <p>Inventore fugit error iure nisi reiciendis fugiat illo pariatur quam sequi quod iusto facilis
                                officiis nobis sit quis molestias asperiores rem, blanditiis! Commodi exercitationem vitae
                                deserunt qui nihil ea, tempore et quam natus quaerat doloremque.</p>
                            <p><a href="#" class="btn btn-primary px-4 py-3">Learn More</a></p>
                        </div>
                        <div class="tab-pane fade py-5" id="v-pills-5" role="tabpanel" aria-labelledby="v-pills-5-tab">
                            <span class="icon mb-3 d-block flaticon-ux-design"></span>
                            <h2 class="mb-4">Business and Management</h2>
                            <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Nesciunt voluptate, quibusdam sunt
                                iste dolores consequatur</p>
                            <p>Inventore fugit error iure nisi reiciendis fugiat illo pariatur quam sequi quod iusto facilis
                                officiis nobis sit quis molestias asperiores rem, blanditiis! Commodi exercitationem vitae
                                deserunt qui nihil ea, tempore et quam natus quaerat doloremque.</p>
                            <p><a href="#" class="btn btn-primary px-4 py-3">Learn More</a></p>
                        </div>
                        <div class="tab-pane fade py-5" id="v-pills-6" role="tabpanel" aria-labelledby="v-pills-6-tab">
                            <span class="icon mb-3 d-block flaticon-innovation"></span>
                            <h2 class="mb-4">Engineering</h2>
                            <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Nesciunt voluptate, quibusdam sunt
                                iste dolores consequatur</p>
                            <p>Inventore fugit error iure nisi reiciendis fugiat illo pariatur quam sequi quod iusto facilis
                                officiis nobis sit quis molestias asperiores rem, blanditiis! Commodi exercitationem vitae
                                deserunt qui nihil ea, tempore et quam natus quaerat doloremque.</p>
                            <p><a href="#" class="btn btn-primary px-4 py-3">Learn More</a></p>
                        </div>
                        <div class="tab-pane fade py-5" id="v-pills-7" role="tabpanel" aria-labelledby="v-pills-7-tab">
                            <span class="icon mb-3 d-block flaticon-idea"></span>
                            <h2 class="mb-4">Low</h2>
                            <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Nesciunt voluptate, quibusdam sunt
                                iste dolores consequatur</p>
                            <p>Inventore fugit error iure nisi reiciendis fugiat illo pariatur quam sequi quod iusto facilis
                                officiis nobis sit quis molestias asperiores rem, blanditiis! Commodi exercitationem vitae
                                deserunt qui nihil ea, tempore et quam natus quaerat doloremque.</p>
                            <p><a href="#" class="btn btn-primary px-4 py-3">Learn More</a></p>
                        </div>
                        <div class="tab-pane fade py-5" id="v-pills-8" role="tabpanel" aria-labelledby="v-pills-8-tab">
                            <span class="icon mb-3 d-block flaticon-innovation"></span>
                            <h2 class="mb-4">Fine Art</h2>
                            <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Nesciunt voluptate, quibusdam sunt
                                iste dolores consequatur</p>
                            <p>Inventore fugit error iure nisi reiciendis fugiat illo pariatur quam sequi quod iusto facilis
                                officiis nobis sit quis molestias asperiores rem, blanditiis! Commodi exercitationem vitae
                                deserunt qui nihil ea, tempore et quam natus quaerat doloremque.</p>
                            <p><a href="#" class="btn btn-primary px-4 py-3">Learn More</a></p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- end program  -->
    <!-- start study in iran  -->
    <section class="uza-about-us-area" style="margin-top:50px;">

        <div class="container">
            <div class="section-heading text-center">
                <h2 style="margin-top:100px;">- Study In Iran -</h2>
            </div>
            <div class="row align-items-center">
               

                <div class="col-12 col-md-12">
                    <div class="about-us-content mb-80 blog-mtn">
                    @foreach($Studyingirans as $Studyingiran)
                       <p>{!!$Studyingiran->description!!}</p> 
                    @endforeach
                    </div>
                </div>
            </div>


        </div>

        <!-- <div class="about-bg-pattern">
            <img src="/home/img/salaam png/curve-2.png" alt="">
        </div> -->
    </section>
    <!-- end study in iran  -->



@stop

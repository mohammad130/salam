@extends('home.layout.main')
@section('title')
    Salaam Study Iran/Testimonials
@stop

@section('content')

    <div class="breadcrumb-area">
        <div class="container h-100">
            <div class="row h-100 align-items-end">
                <div class="col-12">
                    <div class="breadcumb--con">
                        <nav aria-label="breadcrumb">
                            <ol class="breadcrumb">
                                <li class="breadcrumb-item"><a href="#"><i class="fa fa-home"></i> Home</a></li>
                                <li class="breadcrumb-item active" aria-current="page">tesimonials</li>
                            </ol>
                        </nav>
                    </div>
                    <h2 class="title">What Students Say About Us</h2>
                </div>
            </div>
        </div>

        <!-- <div class="breadcrumb-bg-curve">
                <img src="/home/img/salaam png/curve-5.png" alt="">
            </div> -->
    </div>

    <div class="uza-blog-area section-padding-80">
        <div class="container">
            <div class="row">

                @foreach ($testimonials as $testimonial)
                    <div class="col-12 col-lg-4">
                        <div class="single-blog-post bg-img mb-80">
                            <div class="blog-kadrs post-content">
                                <img src="{{ $testimonial->image }}" style="border-radius:100%;" alt="blog of salaam study"
                                    title="blog of salaam study">
                                <a href="{{ route('home.student', $testimonial->title) }}" class="post-title" style="height: 35px; margin-top: 25px">{{ $testimonial->title }}</a>
                                <h6  style="height: 45px;">{{ $testimonial->lesson }}</h6>
                                <p>{{ substr($testimonial->shortDescription, 0, 90) }} ...</p>
                                <a href="{{ route('home.student', $testimonial->title) }}" class="read-more-btn">Read More
                                    <i class="arrow_carrot-2right"></i></a>
                            </div>
                        </div>
                    </div>
                @endforeach
            </div>

        </div>
    </div>
@stop

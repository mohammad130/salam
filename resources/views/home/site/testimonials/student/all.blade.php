@extends('home.layout.main')
@section('title')
    Salaam Study Iran/testimonials student
@stop

@section('content')


    <div class="breadcrumb-area">
        <div class="container h-100">
            <div class="row h-100 align-items-end">
                <div class="col-12">
                    <div class="breadcumb--con">
                        <nav aria-label="breadcrumb">
                            <ol class="breadcrumb">
                                <li class="breadcrumb-item"><a href="#"><i class="fa fa-home"></i> Home</a></li>
                                <li class="breadcrumb-item"><a href="#">Testimonials</a></li>
                                <li class="breadcrumb-item active" aria-current="page">{{ $TestimonialItem->title }}</li>
                            </ol>
                        </nav>
                    </div>
                    <h2 class="titr"style="">What Students Say About Us</h2>
                </div>
            </div>
        </div>
        <!-- 
    <div class="breadcrumb-bg-curve">
    <img src="img/core-img/curve-5.png" alt="">
    </div> -->
    </div>


    <section class="blog-details-area"   style="margin-bottom:400px;">
        <div class="container">
            <div class="row">
                <div class="col-12">
                    <div class="blog-details-content">

                        <div class="post-details-text">
                            <div class="row justify-content-center">
                                <div class="col-12 col-lg-10">
                                    
                                </div>
                                <div class="col-12" style="text-align:center; ">
                                    <img class="mb-50" src="{{ $TestimonialItem->image }}" alt="student at salaam study" style="margin:30px auto;height:400px;border-radius:100%;">
                                </div>
                                <div class="col-12 col-lg-10" style="text-align:center;">
                                    <h4>{{ $TestimonialItem->title }}</h4>
                                    <h4>{{ $TestimonialItem->lesson}}</h4>
                                    <h4>{{ $TestimonialItem->university }}</h4>
                                    <h4>{{ $TestimonialItem->city }}</h4>


                                    <blockquote class="uza-blockquote d-flex">
                                        <div class="icon">
                                            <i class="icon_quotations" aria-hidden="true"></i>
                                        </div>
                                        <div class="text">
                                            <h6>{!! $TestimonialItem->description !!}</h6>
                                        </div>
                                    </blockquote>
                                    <div class="col-12 col-lg-12 ">
                                        <div class="uza-video-area hi-icon-effect-8">
                                            <a href="{{$TestimonialItem->video_link}}" class="hi-icon video-play-btn">Click to watch the video</a>
                                        </div>
                                    </div>
                                    <div class="uza-blog-area">
        <div class="container">
            <div class="row">
                                    <div class="related-news-area">
                                        <h2 class="mb-4">Other Testimonials</h2>
                                        <div class="row">
                                        @foreach ($lastTestimonials as $lastTestimonial)
                    <div class="col-12 col-lg-4">
                        <div class="single-blog-post bg-img mb-80">
                            <div class="blog-kadrs post-content">
                                <img src="{{ $lastTestimonial->image }}" style="border-radius:100%;" alt="blog of salaam study"
                                    title="blog of salaam study">
                                <a href="{{ route('home.student', $lastTestimonial->id) }}" class="post-title" style="height: 35px; margin-top: 25px">{{ $lastTestimonial->title }}</a>
                                <h6  style="height: 45px;">{{ $lastTestimonial->lesson }}</h6>
                                <p>{{ substr($lastTestimonial->shortDescription, 0, 65) }} ...</p>
                                <a href="{{ route('home.student', $lastTestimonial->id) }}" class="read-more-btn">Read More
                                    <i class="arrow_carrot-2right"></i></a>
                            </div>
                        </div>
                    </div>
                @endforeach
                </div>

</div>
</div>
                                        </div>
                                    </div>

                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
@stop

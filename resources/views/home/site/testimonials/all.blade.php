@extends('home.layout.main')
@section('title')
    Salaam Study Iran/Testimonials
@stop

@section('content')

    <div class="breadcrumb-area">
        <div class="container h-100">
            <div class="row h-100 align-items-end">
                <div class="col-12">
                    <div class="breadcumb--con">
                        <nav aria-label="breadcrumb">
                            <ol class="breadcrumb">
                                <li class="breadcrumb-item"><a href="#"><i class="fa fa-home"></i> Home</a></li>
                                <li class="breadcrumb-item active" aria-current="page">tesimonials</li>
                            </ol>
                        </nav>
                    </div>
                    <h2 class="title">What Students Say About Us</h2>
                </div>
            </div>
        </div>

        <!-- <div class="breadcrumb-bg-curve">
            <img src="/home/img/salaam png/curve-5.png" alt="">
        </div> -->
    </div>

    <section class="welcome-area">
        <div>
            {{$nummmber=1}}
            @foreach($testimonials as $testimonial)

                <div class="single-welcome-slide">
                @if($nummmber%2)
                    <div class="welcome-content h-100">
                        <div class="container h-100">
                            <div class="row h-100 align-items-center" style="direction:ltr">
                                <div class="col-12 col-md-6">
                                    <div class="welcome-thumbnail">
                                        <img src="{{$testimonial->image}}" alt="testimonials" title="tesimonials(what say about us?)">
                                    </div>
                                </div>
                                <div class="col-12 col-md-6">
                                    <div class="welcome-text">
                                        <h3>{{$testimonial->title}}</h3>
                                        <h4>Study at<br>{{$testimonial->university}}</h4>
{{--                                        <h5>We love to create "cool" things on Digital Platforms</h5>--}}
                                        <p>{!!$testimonial->description!!}</p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    @else
                    <div class="welcome-content h-100">
                        <div class="container h-100">
                            <div class="row h-100 align-items-center" style="direction:rtl">
                                <div class="col-12 col-md-6">
                                    <div class="welcome-thumbnail">
                                        <img src="{{$testimonial->image}}" alt="testimonials" title="tesimonials(what say about us?)" style="margin: 0 auto;display: block;">
                                    </div>
                                </div>
                                <div class="col-12 col-md-6">
                                    <div class="welcome-text">
                                        <h3>{{$testimonial->title}}</h3>
                                        <h4>Study at<br>{{$testimonial->university}}</h4>
{{--                                        <h5>We love to create "cool" things on Digital Platforms</h5>--}}
                                        <p>{!!$testimonial->description!!}</p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    @endif
                </div>
                <p style="display:none">{{$nummmber++}}</p>
            @endforeach


{{--            <div class="single-welcome-slide">--}}


{{--                <div class="welcome-content h-100">--}}
{{--                    <div class="container h-100">--}}
{{--                        <div class="row h-100 align-items-center">--}}

{{--                            <div class="col-12 col-md-6">--}}
{{--                                <div class="welcome-text">--}}
{{--                                    <h4>Study at<br>Top Universities in<span> Iran</span></h4>--}}
{{--                                    <h5>We love to create "cool" things on Digital Platforms</h5>--}}
{{--                                    <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Nisi tempora temporibus--}}
{{--                                        ea corrupti maiores voluptatibus nam ad eos. Sunt, doloremque.</p>--}}
{{--                                </div>--}}
{{--                            </div>--}}
{{--                            <div class="col-12 col-md-6">--}}
{{--                                <div class="welcome-thumbnail">--}}
{{--                                    <img src="/home/img/testimonials/T2-Sattar-Testimonial.jpg" alt="testimonials" title="testimonial(what say about us?)">--}}
{{--                                </div>--}}
{{--                            </div>--}}
{{--                        </div>--}}
{{--                    </div>--}}
{{--                </div>--}}
{{--            </div>--}}

        </div>
    </section>


    
    




@stop

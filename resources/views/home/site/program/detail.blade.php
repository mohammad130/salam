@extends('home.layout.main')
@section('title')
    Salaam Study Iran/ Ditails
@stop

@section('content')


    <div class="breadcrumb-area">
        <div class="container h-100">
            <div class="row h-100 align-items-end">
                <div class="col-12">
                    <div class="breadcumb--con">
                        <nav aria-label="breadcrumb">
                            <ol class="breadcrumb">
                                <li class="breadcrumb-item"><a href="#"><i class="fa fa-home"></i> Home</a></li>
                                <li class="breadcrumb-item active" aria-current="page">Programs</li>
                                <li class="breadcrumb-item active" aria-current="page">{{old('name',isset($programItems) ? $programItems->title : '')}}</li>
                            </ol>
                        </nav>
                        <h2 style="text-align:center;color:#00a82d;font-size:50px;">{{old('name',isset($programItems) ? $programItems->title : '')}}</h2>

                    </div>
                <!-- <h2 class="title" style="text-align:center;color:#00a82d; font-size:66px;">featured Universities</h2> -->
                                   
                </div>
            </div>
        </div>

        <!-- <div class="breadcrumb-bg-curve">
            <img src="/home/img/salaam png/curve-5.png" alt="">
        </div> -->
    </div>


    <section class="uza-about-us-area mb-100">
        <div class="container">
            <div class="row align-items-center">

                <div class="col-12 col-lg-12">
               
              
                    <div class="about-us-thumbnail blog-ax">
                        <img src="{{old('name',isset($programItems) ? $programItems->image : '')}}"  alt="program in salaam study in iran" title="program in salaam study in iran" style="margin: 0 auto;display:block;height:500px;">
                    </div>
                </div>

                <div class="col-12 col-lg-12">
                  
                    <div class="about-us-content mb-80">

                        <div class="about-tab-content">
                            <div class="tab-content" id="mona_modelTabContent">
                                <div class="tab-pane fade show active" id="tab-1" role="tabpanel"
                                     aria-labelledby="tab1">

                                    <div class="tab-content-text mt-5 blog-mtn">
                                        <p >{!!old('name',isset($programItems) ? $programItems->description : '')!!}</p>
                                    </div>
                                    <div class="col-12 col-lg-12 ">
                                        <div class="uza-video-area hi-icon-effect-8">
                                            <a href="{{old('name',isset($programItems) ? $programItems->video_link : '')}}" class="hi-icon video-play-btn">Click to watch the video</a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

    </section>

@stop

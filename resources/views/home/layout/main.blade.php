<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="description" content="به سایت ما خوش امدید در این سایت شما می توانید معرفی کاملی از تحصیل در ایران و ثبت نام در یکی از معتبرترین موسسه ها را ببینید">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <title>@yield('title')</title>

    <link rel="icon" href="/home/img/English logo-01.png">

    <link rel="stylesheet" href="/home/styles.css">
    <link rel="stylesheet" href="/home/css/whatsapp.css">
    <link rel="stylesheet" href="/admin/textarea/css/froala_style.css">
    <style>
        .fr-emoticon{
            background-repeat: no-repeat !important;
            font-size: inherit;
            height: 1em;
            width: 1em;
            min-height: 20px;
            min-width: 20px;
            display: inline-block;
            margin: -.1em .1em .1em;
            line-height: 1;
            vertical-align: middle;
        }
        .fr-view,.fr-green {
            color: green !important; 
        }
    </style>
    @yield('stylesheet')
</head>

<body>

@yield('preLoader')




<header class="header-area">

    <div class="main-header-area">
        <div class="classy-nav-container breakpoint-off">
            <div class="top-head">
                <a class="lan" href="#">English</a>
                <a class="lan"  href="#">فارسی</a>
                <a class="lan"  href="#">العربیه</a>
               
               
        <!-- <button class="cons"> -->
            <a class="cons" href="{{route('home.contact')}}">get free consultation</a>
        <!-- </button>  -->
        
        </div>
        <div class="border-line"></div>
            <nav class="classy-navbar justify-content-between" id="uzaNav">

                <a class="nav-brand" href="{{route('home.index')}}"><img src="/home/img/core-img/English/English logo-01.png"
                                                              alt="logo" title="salaam study's logo" style="width:185px;"></a>

                <div class="classy-navbar-toggler">
                    <span class="navbarToggler"><span></span><span></span><span></span></span>
                </div>

                <div class="classy-menu">

                    <div class="classycloseIcon">
                        <div class="cross-wrap"><span class="top"></span><span class="bottom"></span></div>
                    </div>

                    <div class="classynav">
                        <ul id="nav">
                            <li class="current-item"><a href="{{route('home.index')}}">Home</a></li>
                            <li><a href="#">About Us</a>
                            <ul class="dropdown">
                                    <li><a href="{{route('home.whoWeAre')}}">Who We Are?</a></li>
                                    <li><a href="{{route('home.visionAndMision')}}">Vision and Mission </a></li>
                                    <li><a href="{{route('home.howWeHelpYou')}}">How We Help You?</a></li>
                                    <li><a href="{{route('home.contact')}}">Contact Us</a></li>
                                </ul>
                            </li>
                            <li><a href="#">Study In Iran</a>
                                <ul class="dropdown">
                                    <li><a href="{{route('home.WhyStudying')}}">Why Studying In Iran?</a></li>
                                    <li><a href="{{route('home.EducationSystem')}}">Education System Of Iran</a></li>
                                    <li><a href="{{route('home.AdmisstionRequirments')}}">Admission Requirements</a></li>
                                    <li><a href="{{route('home.program')}}">Programs</a>
                                    </li>
                                </ul>
                            </li>
                            <p style="display: none">{{$services=App\Models\Services::all()}}</p>

                            <li><a href="#">Our Services</a>
                                <ul class="dropdown">
                                    @foreach ($services as $service)
                                    <li><a href="{{route('home.singleService',$service->id)}}">{{$service->title}}</a></li>
                                    @endforeach

                                </ul>
                            </li>
                            <li><a href="{{route('home.testimonials')}}">testimonials</a></li>
                            <li><a href="{{route('home.admisstionProcess')}}">Admission Process</a></li>

                          <li><a href="{{route('home.University')}}">Universities</a>

                          </li>
                            <li><a href="{{route('home.Blog')}}">Blog</a></li>

                            <li><a href="#">Living in Iran</a>
                                <ul class="dropdown">
                                    <li><a href="{{route('home.aboutiran')}}">About Iran</a></li>
                                    <li><a href="{{route('home.tourismiran')}}">Tourism in Iran</a></li>
                                    <li><a href="{{route('home.culture')}}">Culture</a></li>
                                </ul>
                            </li>
                        </ul>


                    </div>

                </div>
            </nav>
        </div>
    </div>
</header>


@yield('content')

<!-- call  -->
<!-- <div class="uza-cta-area section-padding-0-80">
    <div class="border-line"></div>
</div>
<div class="container">
    <div class="row align-items-center">
        <div class="col-12 col-lg-8">
            <div class="cta-content mb-80">
                <h2>Interested in studing with us?</h2>
                <h6>Hit the button below or give us a call!</h6>
            </div>
        </div>
        <div class="col-12 col-lg-4">
            <div class="cta-content mb-80">
                <div class="call-now-btn">
                    <a href="#"><span>Call us now:</span> +98-910-7113319</a>
                </div>
            </div>
        </div>
    </div>
</div>
</div> -->
<!-- end call  -->


<!-- emaik  -->
<!-- <section class="uza-newsletter-area">
    <div class="container">
        <div class="row align-items-center justify-content-between">

            <div class="col-12 col-md-6 col-lg-6">
                <div class="nl-content mb-80">
                    <h2>Subscribe to our Newsletter</h2>
                    <p>Subscribe our newsletter gor get notification about new updates, etc...</p>
                </div>
            </div>

            <div class="col-12 col-md-6 col-lg-5">
                <div class="nl-form mb-80">
                    <form action="{{route('home.newsletter')}}" method="post">
                        {{csrf_field()}}
                        <input type="email" name="email" value="" placeholder="Your Email">
                        <button type="submit">Subscribe</button>
                    </form>
                </div>
            </div>
        </div>

      
</section> -->
<!-- end email  -->

<div class="border-line"></div>
    </div>
<!-- footer  -->
<footer class="footer-area section-padding-80-0">
    <div class="container">
        <div class="row justify-content-between">

            <div class="col-12 col-sm-6 col-lg-3">
                <div class="single-footer-widget mb-80">

                    <h4 class="widget-title">Contact Us</h4>
                    <?php 
                    $AboutDetails = App\Models\AboutDetail::all();
                    ?>
                    
                    <div class="footer-content mb-15">
                    <p>Tel(WhatsApp/Telegram): <br>@foreach ($AboutDetails as $AboutDetail)<h3>{{$AboutDetail->Tel}}</h3>@endforeach</p>
                        <p>Email: <br>@foreach ($AboutDetails as $AboutDetail) <a href="#" class="__cf_email__" style="color:#00a82d;">{{$AboutDetail->Email}}</a>@endforeach</p>
                        <p>Office Address: <br> @foreach ($AboutDetails as $AboutDetail)<h6 style="color:#00a82d;">{{$AboutDetail->Office_Address}}</h6>@endforeach</p>

                    </div>
                </div>
            </div>

            <div class="col-12 col-sm-6 col-lg-3">
                <div class="single-footer-widget mb-80">

                    <h4 class="widget-title">Quick Link</h4>

                    <nav>
                        <ul class="our-link">
                            <li><a href="{{route('home.whoWeAre')}}">Who We Are?</a></li>
                            <li><a href="{{route('home.howWeHelpYou')}}">How We Can Help?</a></li>
                            <li><a href="{{route('home.WhyStudying')}}">Why Studing In Iran?</a></li>
                            <li><a href="{{route('home.Blog')}}">Blog</a></li>
                            <li><a href="{{route('home.testimonials')}}">Testemonials</a></li>
                            <li><a href="{{route('home.University')}}">Universities</a></li>
                            <li><a href="{{route('home.program')}}">Programs</a></li>
                            <li><a href="{{route('home.contact')}}">Contact Us</a></li>
                        </ul>
                    </nav>
                </div>
            </div>

            <div class="col-12 col-sm-6 col-lg-3">
                <div class="single-footer-widget mb-80">

                    <a href="{{route('home.program')}}"  class="widget-title progz" >Programs</a>

                    <nav>
                        <ul class="our-link">
                            <li><a href="#">Study MBBS in Iran</a></li>
                            <li><a href="#">Study Dentistry in Iran</a></li>
                            <li><a href="#">Study Pharmacy in Iran</a></li>
                            <li><a href="#">Study Nursing in Iran</a></li>
                            <li><a href="#">Study MBA in Iran</a></li>
                            <li><a href="#">Study Engineering in Iran</a></li>
                            <li><a href="#">Study Law in Iran</a></li>
                            <li><a href="#">Study Fine Arts in Iran</a></li>
                        </ul>
                    </nav>
                </div>
            </div>

            <div class="col-12 col-sm-6 col-lg-3">
                <div class="single-footer-widget mb-80">

                    <h4 class="widget-title">About Us</h4>
                    @foreach ($AboutDetails as $AboutDetail)
                    <p>{{$AboutDetail->About_us}}</p>
                    @endforeach
                    <div class="copywrite-text mb-30">
                    <p style="color:black;font-weight:600;">Follow Us:</p>
                    </div>

                    <div class="footer-social-info">
                        <a href="https://www.facebook.com/SalaamStudy/" class="facebook" data-toggle="tooltip" data-placement="top" title="facebook"><i
                                class="fa fa-facebook"></i></a>
                                <a href="http://instagram.com/salaamstudy/" class="youtube" data-toggle="tooltip" data-placement="top" title="Instagram"><i
                                class="fa fa-instagram"></i></a>
                      
                                <a href="https://www.linkedin.com/company/salaamstudy" class="linkedin" data-toggle="tooltip" data-placement="top" title="linkedin"><i
                                class="fa fa-linkedin"></i></a>
                                <a href="http://twitter.com/SalaamStudy" class="instagram" data-toggle="tooltip" data-placement="top" title="twitter"><i
                                class="fa fa-twitter"></i></a>
                        <a href="https://www.youtube.com/channel/UCNX6GKXKADlSI5sTtmaq5BA" class="youtube" data-toggle="tooltip" data-placement="top" title="YouTube"><i
                                class="fa fa-youtube-play"></i></a>
                       
                               
                    </div>
                </div>
            </div>
        </div>
        <div class="row" style="margin-bottom: 30px;">
            Copyright &copy;
            <script>document.write(new Date().getFullYear());</script>
            All rights reserved by salaam study
        </div>
    </div>
</footer>
<!-- end footer  -->
<section class="cta-whatsapp-wrapper">
			<!-- <img src="/home/img/social/whatsapp.png" alt="لوگوی شبکه اجتماعی واتس آپ"> -->
            <a href="https://wa.me/989107113319" class="whatsapp" data-toggle="tooltip" data-placement="top" title="whatsapp" target="_blank"><i
                                class="fa fa-whatsapp"></i></a>
			
		
		<!-- <div class="box-cta-whatsapp-wrapper">
		<h5>پشتیبانی آنلاین سایت از طریق واتساپ</h5>
			<p>جهت سهولت در امر پاسخگویی به شما بازدیدکنندگان گرامی، با راه اندازی پشتیبانی آنلاین، پاسخگوی سوالات شما عزیزان هستیم.</p>
			<p>هرگونه سوال در خصوص سایت را ا طریق واتساپ با ما در میان گذارید.</p>
			<a href="https://wa.me/989107113319?text=سلام. وقت بخیر!!" target="_blank">جهت ارسال پیام در واتساپ اینجا کلیک کنید</a>
			<span><img src="/home/img/social/multiply.png" alt=""></span>		
		</div> -->
</section>

<!-- scripts  -->
<script src="/home/js/jquery.min.js"></script>

<script src="/home/js/popper.min.js"></script>

<script src="/home/js/bootstrap.min.js"></script>

<script src="/home/js/uza.bundle.js"></script>

<script src="/home/js/default-assets/active.js"></script>
<script type="text/javascript" src="/home/js/jquery.js"></script> 
<script type="text/javascript" src="/home/js/webrasam.js"></script>

<script async src="https://www.googletagmanager.com/gtag/js?id=UA-23581568-13"></script>
<script>
    window.dataLayer = window.dataLayer || [];

    function gtag() {
        dataLayer.push(arguments);
    }

    gtag('js', new Date());

    gtag('config', 'UA-23581568-13');
</script>
@yield('script')
</body>

</html>

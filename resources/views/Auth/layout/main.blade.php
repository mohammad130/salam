<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>  login @yield('title')</title>
    <link rel="stylesheet" href="/Auth/css/style.css">
    
    <link href='https://unpkg.com/boxicons@2.0.7/css/boxicons.min.css' rel='stylesheet'>
    @yield('stylesheet')

</head>

<body>
    <div class="login">
        <div class="login--content">
            <div class="login--img">
                <img src="/Auth/image/English logo-01.png" alt="login page">
            </div>
            <div class="login--forms">
                <form action="{{route('doLogin')}}" method="post" class="login--register block" id="login-in">
                {{ csrf_field() }}
                    <h1 class="login--title">sign in</h1>
                    <div class="login--box">
                        <i class="bx bx-user login-icon"></i>
                        <input type="text" name="username" placeholder="username" class="login--input ">
                    </div>
                    <div class="login--box">
                        <i class="bx bx-lock login-icon"></i>
                        <input type="password" name="password" placeholder="password" class="login--input ">
                    </div>
                
                    <div class="row" style="display:flex; align-items: baseline;margin-top:4px; font-size:14px">
                        <label  for="remember-me">Remember me!</label>
                        <input type="checkbox" id="remember-me" name="remember-me" class="login--forgot">

                    </div>
                    <button type="submit" class="login--button">sign in</button>
                </form>
            </div>
        </div>
    </div>

    <script src="/Auth/js/login.js "></script>
    @yield('script')

</body>

</html>

</html>
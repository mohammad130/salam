@extends('admin.layout.main')

@section('content')
    <script>
        function successAdd() {
            Swal.fire({
                title: "با موفقیت ثبت شد  ",
                type: "success",
                timer: 1500,
                showConfirmButton: !1,
            })
        }
        function successEdit() {
            Swal.fire({
                title: "با موفقیت ویرایش شد  ",
                type: "success",
                timer: 1500,
                showConfirmButton: !1,
            })
        }
        function successDelete() {
            Swal.fire({
                title: "با موفقیت حذف شد  ",
                type: "success",
                timer: 1500,
                showConfirmButton: !1,
            })
        }
        $(document).ready(function () {

            @if(session('message'))

            {{session('message')}}

        @endif
        });

    </script>
    <div class="page-title">
        <div class="title">شرایط پذیرش</div>
        <div class="sub-title">شرایط پذیرش در ایران</div>
    </div>
    <div class="card bg-white">
        <div class="card-header">
            شرایط پذیرش
        </div>
        <div class="card-block">
            <div id="DataTables_Table_0_wrapper" class="dataTables_wrapper form-inline no-footer">
                <div class="row">
                    <div class="col-sm-6">

                        <div class="toolbar"><a id="new" href="{{route('admin.admisstionRequirments.add')}}" class="btn btn-info m-l"  style="margin-bottom:18px;">افزودن</a>
                        </div>
                    </div>

                </div>
                <div class="table-responsive">
                    <table
                        class="table table-bordered table-striped datatable editable-datatable responsive align-middle bordered dataTable no-footer"
                        id="DataTables_Table_0" role="grid" aria-describedby="DataTables_Table_0_info">
                        <thead>
                        <tr role="row">
                            <th class="sorting_asc" tabindex="0" aria-controls="DataTables_Table_0" rowspan="1"
                                colspan="1" aria-sort="ascending" aria-label="Name: activate to sort column descending"
                                style="width: 115px;">عنوان
                            </th>
                            <th class="sorting" tabindex="0" aria-controls="DataTables_Table_0" rowspan="1" colspan="1"
                                aria-label="Position: activate to sort column ascending" style="width: 207px;">توضیح کوتاه
                            </th>
                            <th class="sorting" tabindex="0" aria-controls="DataTables_Table_0" rowspan="1" colspan="1"
                                aria-label="Office: activate to sort column ascending" style="width: 98px;">عکس
                            </th>
                            <th class="sorting" tabindex="0" aria-controls="DataTables_Table_0" rowspan="1" colspan="1"
                                aria-label="Edit: activate to sort column ascending" style="width: 58px;">عملیات
                            </th>
                        </tr>
                        </thead>
                        <tbody>

                        @foreach($AdmisstionRequirementInfs as $AdmisstionRequirementInf)
                            <tr role="row" class="odd">
                                <td class="sorting_1">{{$AdmisstionRequirementInf->title}}</td>
                                <td>{{substr($AdmisstionRequirementInf->shortDescription,0,60)}} ...</td>
{{--                                <td>{!!substr($blog->description,0,40)!!} ...</td>--}}
                                <td><img src="{{$AdmisstionRequirementInf ->image}}" width="90px" alt=""></td>
                                <td>
                                    <a href="{{route('admin.admisstionRequirments.edit',$AdmisstionRequirementInf->id)}}" class="edit">ویرایش</a> |
                                    <a href="{{route('admin.admisstionRequirments.delete.post',$AdmisstionRequirementInf->id)}}" class="delete">حذف</a>                            </td>
                                </td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>
                {{--                <div class="datatable-bottom">--}}
                {{--                    <div class="pull-left">--}}
                {{--                        <div class="dataTables_info" id="DataTables_Table_0_info" role="status" aria-live="polite">--}}
                {{--                            Showing 1 to 5 of 5 entries--}}
                {{--                        </div>--}}
                {{--                    </div>--}}
                {{--                    <div class="pull-right">--}}
                {{--                        <div class="dataTables_paginate paging_bootstrap" id="DataTables_Table_0_paginate">--}}
                {{--                            <ul class="pagination no-margin">--}}
                {{--                                <li class="prev disabled"><a href="#" title="Previous">←</a></li>--}}
                {{--                                <li class="active"><a href="#">1</a></li>--}}
                {{--                                <li class="next disabled"><a href="#" title="Next">→</a></li>--}}
                {{--                            </ul>--}}
                {{--                        </div>--}}
                {{--                    </div>--}}
                {{--                </div>--}}
            </div>
        </div>
    </div>
@stop

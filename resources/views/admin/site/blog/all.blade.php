@extends('admin.layout.main')

@section('content')
    <script src="/admin/scripts/myJq.js"></script>
    <script>
    $(document).ready(function () {
            $(".searchSubmit").click(function () {
                var searchInput = $("#searchInput").val();
                console.log("aefffaefea");
                var count;
                $.get('/admin/blog/search', {
                    searchInput: searchInput
                }, function (data) {
                    console.log(data);

                    var id = data[0]["id"];
                    var title = data[0]["title"];
                    var image = data[0]["image"];
                    var shortDescription= data[0]["shortDescription"];
    
                    console.log(id);
                    //set values in modal
                    $("#title").text(title);
                    $("#shortDescription").text(shortDescription);
                    $("#image").attr('src',image);
                    $("#search-edit").attr('href','/admin/blog/edit/'+id);
                    $("#search-delete").attr('href','/admin/blog/delete/'+id);    

                });
            });
    });
        function successAdd() {
            Swal.fire({
                title: "با موفقیت ثبت شد  ",
                type: "success",
                timer: 1500,
                showConfirmButton: !1,
            })
        }
        function successEdit() {
            Swal.fire({
                title: "با موفقیت ویرایش شد  ",
                type: "success",
                timer: 1500,
                showConfirmButton: !1,
            })
        }
        function successDelete() {
            Swal.fire({
                title: "با موفقیت حذف شد  ",
                type: "success",
                timer: 1500,
                showConfirmButton: !1,
            })
        }
        $(document).ready(function () {

            @if(session('message'))

            {{session('message')}}

        @endif
        });

    </script>
    <div class="page-title">
        <div class="title">وبلاگ</div>
        <div class="sub-title">بخش وبلاگ</div>
    </div>
    <div class="card bg-white">
        <div class="card-header">
            وبلاگ
        </div>
        <div class="card-block">
            <div id="DataTables_Table_0_wrapper" class="dataTables_wrapper form-inline no-footer">
                <div class="row" style="margin-bottom: 5px">
                    <div class="col-sm-6">

                        <div class="toolbar"><a id="new" href="{{route('admin.blog.add')}}" class="btn btn-info m-l"  style="margin-bottom:15px;">افزودن</a>
                        </div>
                    </div>
                    <div class="col-sm-6">
                        <input type="search"
                               class="form-control" style="width: 68%;height: 35px" placeholder="جستجو کنید ..."
                               aria-controls="datatable-buttons" id="searchInput">
                        <button class="btn searchSubmit"
                                style="background-color: #25aad8!important;color: white " id="span-btn"
                                data-toggle="modal" data-target="#search">
                            <i class="fa fa-search"></i>
                        </button>
                    </div>
                </div>
                <div class="table-responsive">
                    <table
                        class="table table-bordered table-striped datatable editable-datatable responsive align-middle bordered dataTable no-footer"
                        id="DataTables_Table_0" role="grid" aria-describedby="DataTables_Table_0_info">
                        <thead>
                        <tr role="row">
                            <th class="sorting_asc" tabindex="0" aria-controls="DataTables_Table_0" rowspan="1"
                                colspan="1" aria-sort="ascending" aria-label="Name: activate to sort column descending"
                                style="width: 115px;">عنوان
                            </th>
                            <th class="sorting" tabindex="0" aria-controls="DataTables_Table_0" rowspan="1" colspan="1"
                                aria-label="Position: activate to sort column ascending" style="width: 207px;">توضیح کوتاه
                            </th>
                            <th class="sorting" tabindex="0" aria-controls="DataTables_Table_0" rowspan="1" colspan="1"
                                aria-label="Office: activate to sort column ascending" style="width: 98px;">عکس
                            </th>
                            <th class="sorting" tabindex="0" aria-controls="DataTables_Table_0" rowspan="1" colspan="1"
                                aria-label="Edit: activate to sort column ascending" style="width: 58px;">عملیات
                            </th>
                        </tr>
                        </thead>
                        <tbody>

                        @foreach($blogs as $blog)
                            <tr role="row" class="odd">
                                <td class="sorting_1">{{$blog->title}}</td>
                                <td>{{substr($blog->shortDescription,0,60)}} ...</td>
{{--                                <td>{!!substr($blog->description,0,40)!!} ...</td>--}}
                                <td><img src="{{$blog->image}}" width="90px" alt=""></td>
                                <td>
                                    <a href="{{route('admin.blog.edit',$blog->id)}}" class="edit">ویرایش</a> |
                                    <a href="{{route('admin.blog.delete.post',$blog->id)}}" class="delete">حذف</a></td>
                                </td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>
                <div>
                    {{$blogs->links('vendor.pagination.bootstrap-4')}}
                </div>
            </div>
        </div>
    </div>
    <!------------------------------------ Modal search -------------------------------->
    <div class="modal fade" id="search" tabindex="-1" role="dialog"
         aria-labelledby="searchLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body" style="">
                    <div class="panel-wrapper">
                        <div class="panel-body">
                            <div class="table-wrap">
                                <div class="table-responsive">
                                    <span> عنوان : </span><span id="title"> </span> </br></br>
                                    <span> توضیح کوتاه : </span><span id="shortDescription"> </span> </br></br>
                                    <span> عکس : </span><img id="image" src="" width="120px" alt=""></br></br>
                                    <span> عملیات : </span><span id="cellNumber_search">
                                    <a href="" class="edit" id="search-edit">ویرایش</a> |
                                    <a href="" class="delete" id="search-delete">حذف</a></td>
                                     </span></br></br>

                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-----------------------end modal search---------------------------->
@stop

@extends('admin.layout.main')

@section('content')

    <div class="page-title">
        <div class="title">خبرنامه</div>
        <div class="sub-title">ایمیل های ثبت شده</div>
    </div>
    <div class="card bg-white">
        <div class="card-header">
            ایمیل های ثبت شده
        </div>
        <div class="card-block">
            <div id="DataTables_Table_0_wrapper" class="dataTables_wrapper form-inline no-footer">

                <div class="table-responsive">
                    <table
                        class="table table-bordered table-striped datatable editable-datatable responsive align-middle bordered dataTable no-footer"
                        id="DataTables_Table_0" role="grid" aria-describedby="DataTables_Table_0_info">
                        <thead>
                        <tr role="row">
                            <th class="sorting_asc" tabindex="0" aria-controls="DataTables_Table_0" rowspan="1"
                                colspan="1" aria-sort="ascending" aria-label="Name: activate to sort column descending"
                                style="width: 115px;">ایمیل
                            </th>
                            <th class="sorting" tabindex="0" aria-controls="DataTables_Table_0" rowspan="1" colspan="1"
                                aria-label="Edit: activate to sort column ascending" style="width: 58px;">عملیات
                            </th>
                        </tr>
                        </thead>
                        <tbody>

                        @foreach($newsletterInfs as $newsletterInf)
                            <tr role="row" class="odd">
                                <td class="sorting_1">{{$newsletterInf->email}}</td>
                                <td>
                                    <a href="{{route('admin.Newsletter.delete',$newsletterInf->id)}}"
                                       class="delete">حذف</a></td>
                                </td>
                            </tr>
                            <!-- Modal -->

                        @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
@stop

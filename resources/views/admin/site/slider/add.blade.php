@extends('admin.layout.main')

@section('content')
    <div class="page-title">
        <div class="title">افزودن</div>
        <div class="sub-title">افزودن اسلایدر جدید</div>
    </div>
    <div class="card bg-white">
        <div class="card-header">
            افزودن اسلایدر
        </div>
        <div class="card-block">
            <div class="row m-a-0">
                <div class="col-lg-12">
                    <form class="form-horizontal" action="" method="post" enctype="multipart/form-data" role="form">
                        {{csrf_field()}}
                        <div class="form-group">
                            <label class="col-sm-2 control-label">عنوان</label>
                            <div class="col-sm-10">
                                    <input type="text" name="title" class="form-control">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-2 control-label">توضیحات</label>
                            <div class="col-sm-10">
                                <textarea class="form-control" name="description" rows="3"></textarea>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-2 control-label">تصویر</label>
                            <div class="col-sm-10">
                                <input type="file" name="imageslider">
                            </div>
                        </div>
                        <div class="form-group">
                            <button type="submit" class="btn btn-primary button-next">ثبت</button>
                            <a href="{{route('admin.slider')}}" class="btn btn-default button-next">لغو</a>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
@stop

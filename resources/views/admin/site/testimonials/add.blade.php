@extends('admin.layout.main')

@section('content')
    <div class="page-title">
        <div class="title">افزودن</div>
        <div class="sub-title">افزودن نظر جدید</div>
    </div>
    <div class="card bg-white">
        <div class="card-header">
            افزودن نظر
        </div>
        <div class="card-block">
            <div class="row m-a-0">
                <div class="col-lg-12">
                    <form class="form-horizontal" action="" enctype="multipart/form-data" method="post" role="form">
                        {{csrf_field()}}
                        <div class="form-group">
                            <label class="col-sm-2 control-label">عنوان</label>
                            <div class="col-sm-10">
                                    <input type="text" name="title" class="form-control">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-2 control-label">شهر</label>
                            <div class="col-sm-10">
                                    <input type="text" name="city" class="form-control">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-2 control-label">دانشگاه</label>
                            <div class="col-sm-10">
                                    <input type="text" name="university" class="form-control">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-2 control-label">رشته</label>
                            <div class="col-sm-10">
                                    <input type="text" name="lesson" class="form-control">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-2 control-label">توضیح کوتاه</label>
                            <div class="col-sm-10">
                                <textarea class="form-control" name="shortDescription" required id=""></textarea>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-2 control-label">توضیحات</label>
                            <div class="col-sm-10">
                                <textarea class="form-control" name="description" rows="3"></textarea>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-2 control-label">تصویر</label>
                            <div class="col-sm-10">
                                <input type="file" name="imageInp">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-2 control-label">لینک ویدیو</label>
                            <div class="col-sm-10">
                                    <input type="text" name="video_link" class="form-control" placeholder="مثال : https://www.youtube.com/watch?v=sSakBz_eYzQ">
                            </div>
                        </div>
                        <div class="form-group">
                            <button type="submit" class="btn btn-primary button-next">ثبت</button>
                            <a href="{{route('admin.testimonial')}}" class="btn btn-default button-next">لغو</a>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
@stop

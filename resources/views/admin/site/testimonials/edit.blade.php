@extends('admin.layout.main')
@section('stylesheet')


    <style>
        body {
            /*text-align: center;*/
            overflow: unset !important;
        }

        div#editor {
            width: 81%;
            margin: auto;
            text-align: left;
        }
    </style>
@stop
@section('content')
    <div class="page-title">
        <div class="title">ویرایش</div>
        <div class="sub-title">ویرایش نظر </div>
    </div>
    <div class="card bg-white">
        <div class="card-header">
            ویرایش نظر
        </div>
        <div class="card-block">
            <div class="row m-a-0">
                <div class="col-lg-12">
                    <form class="form-horizontal" action="" method="post" enctype="multipart/form-data" role="form">
                        {{csrf_field()}}
                        <div class="form-group">
                            <label class="col-sm-2 control-label">عنوان</label>
                            <div class="col-sm-10">
                                <input type="text" name="title" class="form-control" value="{{old('name',isset($TestimonialItem) ? $TestimonialItem->title : '')}}">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-2 control-label">شهر</label>
                            <div class="col-sm-10">
                                    <input type="text" name="city" class="form-control" value="{{old('name',isset($TestimonialItem) ? $TestimonialItem->city : '')}}">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-2 control-label">دانشگاه</label>
                            <div class="col-sm-10">
                                    <input type="text" name="university" class="form-control" value="{{old('name',isset($TestimonialItem) ? $TestimonialItem->university : '')}}">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-2 control-label">رشته</label>
                            <div class="col-sm-10">
                                    <input type="text" name="lesson" class="form-control" value="{{old('name',isset($TestimonialItem) ? $TestimonialItem->lesson : '')}}">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-2 control-label">توضیح کوتاه</label>
                            <div class="col-sm-10">
                                <textarea class="form-control" name="shortDescription" required id="">{{old('name',isset($TestimonialItem) ? $TestimonialItem->shortDescription : '')}}</textarea>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-2 control-label">توضیحات</label>
                            <div class="col-sm-10">
                                <textarea class="form-control" name="description" required rows="3">{{old('name',isset($TestimonialItem) ? $TestimonialItem->description : '')}}</textarea>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-2 control-label">تصویر</label>
                            <div class="col-sm-10">
                                <input type="file" name="imageInp">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-2 control-label">لینک ویدیو</label>
                            <div class="col-sm-10">
                                <input type="text" name="video_link" class="form-control" value="{{old('name',isset($TestimonialItem) ? $TestimonialItem->video_link : '')}}" placeholder="مثال : https://www.youtube.com/watch?v=sSakBz_eYzQ">
                            </div>
                        </div>
                        <div class="form-group">
                            <button type="submit" class="btn btn-primary button-next">ثبت</button>
                            <a href="{{route('admin.testimonial')}}" class="btn btn-default button-next">لغو</a>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
@stop
@section('script')

    <script>
    (function () {
      new FroalaEditor("#edit", {
        pastePlain: true
      })
    })()
  </script>
@stop


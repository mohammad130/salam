@extends('admin.layout.main')
@section('stylesheet')
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.4.0/css/font-awesome.min.css">
    <link rel="stylesheet" href="/admin/textarea/css/froala_editor.css">
    <link rel="stylesheet" href="/admin/textarea/css/froala_style.css">
    <link rel="stylesheet" href="/admin/textarea/css/plugins/code_view.css">
    <link rel="stylesheet" href="/admin/textarea/css/plugins/colors.css">
    <link rel="stylesheet" href="/admin/textarea/css/plugins/emoticons.css">
    <link rel="stylesheet" href="/admin/textarea/css/plugins/image_manager.css">
    <link rel="stylesheet" href="/admin/textarea/css/plugins/image.css">
    <link rel="stylesheet" href="/admin/textarea/css/plugins/line_breaker.css">
    <link rel="stylesheet" href="/admin/textarea/css/plugins/table.css">
    <link rel="stylesheet" href="/admin/textarea/css/plugins/char_counter.css">
    <link rel="stylesheet" href="/admin/textarea/css/plugins/video.css">
    <link rel="stylesheet" href="/admin/textarea/css/plugins/fullscreen.css">
    <link rel="stylesheet" href="/admin/textarea/css/plugins/file.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/codemirror/5.3.0/codemirror.min.css">

    <style>
        body {
            /*text-align: center;*/
            overflow: unset !important;
        }

        div#editor {
            width: 81%;
            margin: auto;
            text-align: left;
        }
    </style>
@stop
@section('content')
    <div class="page-title">
        <div class="title">افزودن</div>
        <div class="sub-title">افزودن اطلاعات</div>
    </div>
    <div class="card bg-white">
        <div class="card-header">
            افزودن اطلاعات
        </div>
        <div class="card-block">
            <div class="row m-a-0">
                <div class="col-lg-12">
                    <form class="form-horizontal" action="" method="post" enctype="multipart/form-data" role="form">
                        {{csrf_field()}}
                        <div class="form-group">
                            <label class="col-sm-2 control-label">عنوان</label>
                            <div class="col-sm-10">
                                    <input type="text" name="title" class="form-control">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-2 control-label">توضیح کوتاه</label>
                            <div class="col-sm-10">
                                <textarea class="form-control" name="shortDescription" required id=""></textarea>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-sm-2 control-label">توضیحات</div>
                            <div id="col-sm-10 editor" style="margin-top: 44px">
                                <textarea id='edit' name="description" required style="margin-top: 30px;"></textarea>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-2 control-label">تصویر</label>
                            <div class="col-sm-10">
                                <input type="file" name="imageInp">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-2 control-label">لینک ویدیو</label>
                            <div class="col-sm-10">
                                    <input type="text" name="video_link" class="form-control" placeholder="مثال : https://www.youtube.com/watch?v=sSakBz_eYzQ">
                            </div>
                        </div>
                        <div class="form-group">
                            <button type="submit" class="btn btn-primary button-next">ثبت</button>
                            <a href="{{route('admin.howWeCanHelp')}}" class="btn btn-default button-next">لغو</a>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
@stop
@section('script')
    <script type="text/javascript"
            src="https://cdnjs.cloudflare.com/ajax/libs/codemirror/5.3.0/codemirror.min.js"></script>
    <script type="text/javascript"
            src="https://cdnjs.cloudflare.com/ajax/libs/codemirror/5.3.0/mode/xml/xml.min.js"></script>
    <script type="text/javascript" src="/admin/textarea/js/froala_editor.min.js"></script>
    <script type="text/javascript" src="/admin/textarea/js/plugins/align.min.js"></script>
    <script type="text/javascript" src="/admin/textarea/js/plugins/code_beautifier.min.js"></script>
    <script type="text/javascript" src="/admin/textarea/js/plugins/code_view.min.js"></script>
    <script type="text/javascript" src="/admin/textarea/js/plugins/colors.min.js"></script>
    <script type="text/javascript" src="/admin/textarea/js/plugins/draggable.min.js"></script>
    <script type="text/javascript" src="/admin/textarea/js/plugins/emoticons.min.js"></script>
    <script type="text/javascript" src="/admin/textarea/js/plugins/font_size.min.js"></script>
    <script type="text/javascript" src="/admin/textarea/js/plugins/font_family.min.js"></script>
    <script type="text/javascript" src="/admin/textarea/js/plugins/image.min.js"></script>
    <script type="text/javascript" src="/admin/textarea/js/plugins/file.min.js"></script>
    <script type="text/javascript" src="/admin/textarea/js/plugins/image_manager.min.js"></script>
    <script type="text/javascript" src="/admin/textarea/js/plugins/line_breaker.min.js"></script>
    <script type="text/javascript" src="/admin/textarea/js/plugins/link.min.js"></script>
    <script type="text/javascript" src="/admin/textarea/js/plugins/lists.min.js"></script>
    <script type="text/javascript" src="/admin/textarea/js/plugins/paragraph_format.min.js"></script>
    <script type="text/javascript" src="/admin/textarea/js/plugins/paragraph_style.min.js"></script>
    <script type="text/javascript" src="/admin/textarea/js/plugins/video.min.js"></script>
    <script type="text/javascript" src="/admin/textarea/js/plugins/table.min.js"></script>
    <script type="text/javascript" src="/admin/textarea/js/plugins/url.min.js"></script>
    <script type="text/javascript" src="/admin/textarea/js/plugins/entities.min.js"></script>
    <script type="text/javascript" src="/admin/textarea/js/plugins/char_counter.min.js"></script>
    <script type="text/javascript" src="/admin/textarea/js/plugins/inline_style.min.js"></script>
    <script type="text/javascript" src="/admin/textarea/js/plugins/save.min.js"></script>
    <script type="text/javascript" src="/admin/textarea/js/plugins/fullscreen.min.js"></script>
    <script>
    (function () {
      new FroalaEditor("#edit", {
        pastePlain: true
      })
    })()
  </script>
 
@stop

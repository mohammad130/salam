@extends('admin.layout.main')

@section('content')
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
    <script>
        $(document).ready(function () {
            $(".details").click(function () {
                var userId = $(this).attr("id");
                $.get('/admin/details/contactUs', {
                    userId: userId
                }, function (data) {

                    //Getting informations from server
                    var name = data["name"];
                    var title = data["title"];
                    var description = data["description"];

                    //set values in modal
                    $("#name").text(name);
                    $("#title").text(title);
                    $("#Description").text(description);
                });
            });
        });
    </script>
    <script>
        function successAdd() {
            Swal.fire({
                title: "با موفقیت ثبت شد  ",
                type: "success",
                timer: 1500,
                showConfirmButton: !1,
            })
        }
        function successEdit() {
            Swal.fire({
                title: "با موفقیت ویرایش شد  ",
                type: "success",
                timer: 1500,
                showConfirmButton: !1,
            })
        }
        function successDelete() {
            Swal.fire({
                title: "با موفقیت حذف شد  ",
                type: "success",
                timer: 1500,
                showConfirmButton: !1,
            })
        }
        $(document).ready(function () {

            @if(session('message'))

            {{session('message')}}

        @endif
        });

    </script>
    <div class="page-title">
        <div class="title">درباره ما</div>
        <div class="sub-title">تماس با ما</div>
    </div>
    <div class="card bg-white">
        <div class="card-header">
          پیام های دریافتی
        </div>
        <div class="card-block">
            <div id="DataTables_Table_0_wrapper" class="dataTables_wrapper form-inline no-footer">

                <div class="table-responsive">
                    <table
                        class="table table-bordered table-striped datatable editable-datatable responsive align-middle bordered dataTable no-footer"
                        id="DataTables_Table_0" role="grid" aria-describedby="DataTables_Table_0_info">
                        <thead>
                        <tr role="row">
                            <th class="sorting_asc" tabindex="0" aria-controls="DataTables_Table_0" rowspan="1"
                                colspan="1" aria-sort="ascending" aria-label="Name: activate to sort column descending"
                                style="width: 115px;">نام و نام خانوادگی
                            </th>
                            <th class="sorting" tabindex="0" aria-controls="DataTables_Table_0" rowspan="1" colspan="1"
                                aria-label="Position: activate to sort column ascending" style="width: 98px;">عنوان
                            </th>
                            <th class="sorting" tabindex="0" aria-controls="DataTables_Table_0" rowspan="1" colspan="1"
                                aria-label="Office: activate to sort column ascending" style="width: 207px;">توضیحات
                            </th>
                            <th class="sorting" tabindex="0" aria-controls="DataTables_Table_0" rowspan="1" colspan="1"
                                aria-label="Edit: activate to sort column ascending" style="width: 58px;">عملیات
                            </th>
                        </tr>
                        </thead>
                        <tbody>

                        @foreach($contactUsInfs as $contactUsInf)
                            <tr role="row" class="odd">
                                <td class="sorting_1">{{$contactUsInf->name}}</td>
                                <td>{{$contactUsInf->title}}</td>
                                <td>{{substr($contactUsInf->description,0,60)}} ...</td>
                                <td>
                                    <a id="{{$contactUsInf->id}}" class="details" data-toggle="modal" data-target="#details">جزئیات
                                    </a> |
                                    <a href="{{route('admin.ContactUs.delete.post',$contactUsInf->id)}}" class="delete">حذف</a>                            </td>
                                </td>
                            </tr>
                            <!-- Modal -->

                        @endforeach
                        </tbody>
                    </table>
                    <div class="modal fade" id="details" role="dialog">
                        <div class="modal-dialog">

                            <!-- Modal content-->
                            <div class="modal-content">
                                <div class="modal-header">
                                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                                    <h4 class="modal-title">جزئیات</h4>
                                </div>
                                <div class="modal-body">
                                    <div class="table-responsive responsive">
                                        <table class="table-striped tbl-modal" style="width: 100%;line-height: 40px;">
                                            <tr>
                                                <th style="width: 30%"> نام و نام خانوادگی</th>
                                                <td id="name" class="p-2"></td>
                                            </tr>
                                            <tr style="margin-top: 10px; background-color: unset">
                                                <th>عنوان</th>
                                                <td id="title" class="p-2" ></td>
                                            </tr>
                                            <tr style="margin-top: 10px; background-color: unset">
                                                <th>توضیحات</th>
                                                <td id="Description" class="p-2" ></td>
                                            </tr>
                                        </table>
                                    </div>
                                </div>
                                <div class="modal-footer">
                                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                </div>
                            </div>

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@stop

@extends('admin.layout.main')

@section('content')
    <div class="page-title">
        <div class="title">ویرایش اطلاعات</div>
        <div class="sub-title">ویرایش اطلاعات ارتباط با ما</div>
    </div>
    <div class="card bg-white">
        <div class="card-header">
            ویرایش اطلاعات
        </div>
        <div class="card-block">
            <div class="row m-a-0">
                <div class="col-lg-12">
                    <form class="form-horizontal" action="{{route('admin.aboutDetails.post',1)}}" method="post" enctype="multipart/form-data" role="form">
                        {{csrf_field()}}
                        <div class="form-group">
                            <label class="col-sm-2 control-label">تلفن</label>
                            <div class="col-sm-10">
                                    <input type="text" name="Tel" class="form-control" value="{{old('name',isset($AboutDetailItem) ? $AboutDetailItem->Tel : '')}}">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-2 control-label">ایمیل</label>
                            <div class="col-sm-10">
                                <input class="form-control" name="Email" required id="" value="{{old('name',isset($AboutDetailItem) ? $AboutDetailItem->Email : '')}}">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-2 control-label">آدرس شرکت</label>
                            <div class="col-sm-10">
                                <input type="text" name="Office_Address" class="form-control" value="{{old('name',isset($AboutDetailItem) ? $AboutDetailItem->Office_Address : '')}}">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-2 control-label">درباره ما</label>
                            <div class="col-sm-10">
                                <input type="text" name="About_us" class="form-control" value="{{old('name',isset($AboutDetailItem) ? $AboutDetailItem->About_us : '')}}">
                            </div>
                        </div>
                        <div class="form-group">
                            <button type="submit" class="btn btn-primary button-next">ثبت</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
@stop


@extends('admin.layout.main')

@section('content')
    <script>
        function successAdd() {
            Swal.fire({
                title: "با موفقیت ثبت شد  ",
                type: "success",
                timer: 1500,
                showConfirmButton: !1,
            })
        }
        function successEdit() {
            Swal.fire({
                title: "با موفقیت ویرایش شد  ",
                type: "success",
                timer: 1500,
                showConfirmButton: !1,
            })
        }
        function successDelete() {
            Swal.fire({
                title: "با موفقیت حذف شد  ",
                type: "success",
                timer: 1500,
                showConfirmButton: !1,
            })
        }
        $(document).ready(function () {

            @if(session('message'))

            {{session('message')}}

        @endif
        });

    </script>
    <div class="page-title">
        <div class="title">درباره ما</div>
        <div class="sub-title">ما کی هستیم</div>
    </div>
    <div class="card bg-white">
        <div class="card-header">
            درباره ما
        </div>
        <div class="card-block">
            <div id="DataTables_Table_0_wrapper" class="dataTables_wrapper form-inline no-footer">
                <div class="row">
                    <div class="col-sm-6">

                        <div class="toolbar"><a id="new" href="{{route('admin.whoWeAreDesk.add')}}" class="btn btn-info m-l"  style="margin-bottom:18px;">افزودن</a>
                        </div>
                    </div>

                </div>
                <div class="table-responsive">
                    <table
                        class="table table-bordered table-striped datatable editable-datatable responsive align-middle bordered dataTable no-footer"
                        id="DataTables_Table_0" role="grid" aria-describedby="DataTables_Table_0_info">
                        <thead>
                        <tr role="row">
                            
                            <th class="sorting" tabindex="0" aria-controls="DataTables_Table_0" rowspan="1" colspan="1"
                                aria-label="Position: activate to sort column ascending" style="width: 207px;">*
                            </th>
                            
                            <th class="sorting" tabindex="0" aria-controls="DataTables_Table_0" rowspan="1" colspan="1"
                                aria-label="Edit: activate to sort column ascending" style="width: 58px;">عملیات
                            </th>
                        </tr>
                        </thead>
                        <tbody>

                        @foreach($whoWeAreInfs as $whoWeAreInf)
                            <tr role="row" class="odd">
                                <td class="sorting_1">{{$whoWeAreInf->text}}</td>
                                <td>
                                    <a href="{{route('admin.whoWeAreDesk.edit',$whoWeAreInf->id)}}" class="edit">ویرایش</a> |
                                    <a href="{{route('admin.whoWeAreDesk.delete.post',$whoWeAreInf->id)}}" class="delete">حذف</a>                            </td>
                                </td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>
                <div>
                    {{$whoWeAreInfs->links('vendor.pagination.bootstrap-4')}}
                </div>
            </div>
        </div>
    </div>
@stop

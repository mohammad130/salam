@extends('admin.layout.main')

@section('stylesheet')

    <style>
        body {
            /*text-align: center;*/
            overflow: unset !important;
        }

        div#editor {
            width: 81%;
            margin: auto;
            text-align: left;
        }
    </style>
@stop
@section('content')
    <div class="page-title">
        <div class="title">افزودن</div>
        <div class="sub-title">افزودن اطلاعات</div>
    </div>
    <div class="card bg-white">
        <div class="card-header">
            افزودن اطلاعات
        </div>
        <div class="card-block">
            <div class="row m-a-0">
                <div class="col-lg-12">
                    <form class="form-horizontal" action="" method="post" enctype="multipart/form-data" role="form">
                        {{csrf_field()}}
                        
                        <div class="form-group">
                            <label class="col-sm-2 control-label">*</label>
                            <div class="col-sm-10">
                                <textarea class="form-control" name="text" required id=""></textarea>
                            </div>
                        </div>
                        
                        <div class="form-group">
                            <button type="submit" class="btn btn-primary button-next">ثبت</button>
                            <a href="{{route('admin.whoWeAre')}}" class="btn btn-default button-next">لغو</a>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
@stop
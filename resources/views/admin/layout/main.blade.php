<!doctype html>
<html class="no-js" lang="">

<head>
    <meta charset="utf-8">
    <title>   سلام!! @yield('title') </title>
    <meta name="description" content="">
    <meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1, maximum-scale=1">
    <!-- page stylesheets -->
    <!-- end page stylesheets -->
    <!-- build:css({.tmp,app}) styles/app.min.css -->
    <link href='https://unpkg.com/boxicons@2.0.7/css/boxicons.min.css' rel='stylesheet'>
    <link rel="stylesheet" href="/admin/styles/style.css">
    <link rel="stylesheet" href="/admin/styles/webfont.css">
    <link rel="stylesheet" href="/admin/styles/climacons-font.css">
    <link rel="stylesheet" href="/admin/vendor/bootstrap/dist/css/bootstrap.css">
    <link rel="stylesheet" href="/admin/styles/font-awesome.css">
    <link rel="stylesheet" href="/admin/styles/card.css">
    <link rel="stylesheet" href="/admin/styles/sli.css">
    <link rel="stylesheet" href="/admin/styles/animate.css">
    <link rel="stylesheet" href="/admin/styles/app.css">
    <link rel="stylesheet" href="/admin/styles/app.skins.css">
    <link rel="stylesheet" href="/admin/styles/sweetalert-2.min.css">
    <script src="/admin/scripts/myJq.js"></script>
@yield('stylesheet')
<!-- endbuild -->
</head>

<body class="page-loading">
<!-- page loading spinner -->
    @yield('pageLoader')
<!-- /page loading spinner -->
<div class="app layout-fixed-header">
    <!-- sidebar panel -->
    <div class="sidebar-panel offscreen-left">
        <div class="brand">
            <!-- toggle small sidebar menu -->
            <a href="javascript:;" class="toggle-apps hidden-xs" data-toggle="quick-launch">
                <i class="icon-grid"></i>
            </a>
            <!-- /toggle small sidebar menu -->
            <!-- toggle offscreen menu -->
            <div class="toggle-offscreen">
                <a href="javascript:;" class="visible-xs hamburger-icon" data-toggle="offscreen" data-move="ltr">
                    <span></span>
                    <span></span>
                    <span></span>
                </a>
            </div>
            <!-- /toggle offscreen menu -->
            <!-- logo -->
            <a class="brand-logo">
                <span>سلام</span>
            </a>
            <img src="/admin/images/English logo-01.png" alt="logo">
            <!-- /logo -->
        </div>
       
        <!-- main navigation -->
        <nav role="navigation">
            <ul class="nav">
                <!-- cards -->
                <li>
                    <a href="javascript:;">
                        <i class="icon-drop"></i>
                        <span>صفحه اصلی</span>
                    </a>
                    <ul class="sub-menu">
                        <li>
                            <a href="{{route('admin.slider')}}">
                                <span>اسلایدر</span>
                            </a>
                        </li>
                        <li>
                            <a href="{{route('admin.whoWeAreDesk')}}">
                                <span>ما کی هستیم</span>
                            </a>
                        </li>
                    </ul>
                </li>
                <!-- /cards -->

                <li>
                    <a href="javascript:;">
                        <i class="icon-info"></i>
                        <span>درباره ما</span>
                    </a>
                    <ul class="sub-menu">
                        <li>
                            <a href="{{route('admin.whoWeAre')}}">
                                <span>ما کی هستیم</span>
                            </a>
                        </li>
                        <li>
                            <a href="{{route('admin.visionAndMission')}}">
                                <span>چشم انداز و ماموریت</span>
                            </a>
                        </li>
                        <li>
                            <a href="{{route('admin.howWeCanHelp')}}">
                                <span>ما چگونه به شما کمک میکنیم</span>
                            </a>
                        </li>
                        <li>
                            <a href="{{route('admin.ContactUs')}}">
                                <span>تماس با ما</span>
                            </a>
                        </li>
                    </ul>
                </li>

                <li>
                    <a href="{{route('admin.blog')}}">
                        <i class="icon-paper-clip"></i>
                        <span>وبلاگ</span>
                    </a>
                </li>
                <li>
                    <a href="{{route('admin.program')}}">
                        <i class="icon-paper-clip"></i>
                        <span>برنامه ها</span>
                    </a>
                </li>
                <li>
                    <a href="{{route('admin.testimonial')}}">
                        <i class="icon-speech"></i>
                        <span>نظرات کاربران</span>
                    </a>
                </li>
                <li>
                    <a href="{{route('admin.university')}}">
                        <i class="icon-notebook"></i>
                        <span>دانشگاه ها</span>
                    </a>
                </li>
                <li>
                    <a href="{{route('admin.admisstionProcess')}}">
                        <i class="icon-loop"></i>
                        <span>مراحل پذیرش</span>
                    </a>
                </li>
                <li>
                    <a href="javascript:;">
                        <i class="icon-home"></i>
                        <span>زندگی در ایران</span>
                    </a>
                    <ul class="sub-menu">
                        <li>
                            <a href="{{route('admin.aboutIran')}}">
                                <span>درباره ایران</span>
                            </a>
                            <a href="{{route('admin.tourismIran')}}">
                                <span>توریسم در ایران</span>
                            </a>
                            <a href="{{route('admin.culture')}}">
                                <span>فرهنگ</span>
                            </a>
                        </li>
                    </ul>
                </li>
                <li>
                    <a href="javascript:;">
                        <i class="icon-docs"></i>
                        <span>تحصیل در ایران</span>
                    </a>
                    <ul class="sub-menu">
                        <li>
                        <a href="{{route('admin.studyingIran')}}">
                                <span>تحصیل در ایران</span>
                            </a>
                            <a href="{{route('admin.whyStudying')}}">
                                <span>چرا مطالعه در ایران</span>
                            </a>
                            <a href="{{route('admin.educationSystem')}}">
                                <span>سیستم آموزشی در ایران</span>
                            </a>
                            <a href="{{route('admin.admisstionRequirments')}}">
                                <span>شرایط پذیرش در ایران</span>
                            </a>
                        </li>
                    </ul>
                </li>
                <li>
                    <a href="{{route('admin.service')}}">
                        <i class="icon-loop"></i>
                        <span>خدمات</span>
                    </a>
                </li>
                <li>
                    <a href="{{route('admin.aboutDetails', 1)}}">
                        <i class="icon-paper-clip"></i>
                        <span>اطلاعات درباره ما</span>
                    </a>
                </li>

            </ul>
        </nav>
        <!-- /main navigation -->
    </div>
    <!-- /sidebar panel -->
    <!-- content panel -->
    <div class="main-panel">
        <!-- top header -->
        <div class="header navbar">
            <div class="brand visible-xs">
                <!-- toggle offscreen menu -->
                <div class="toggle-offscreen">
                    <a href="javascript:;" class="hamburger-icon visible-xs" data-toggle="offscreen" data-move="ltr">
                        <span></span>
                        <span></span>
                        <span></span>
                    </a>
                </div>
                <!-- /toggle offscreen menu -->
                <!-- logo -->
                <a class="brand-logo">
                    <span></span>
                </a>
                <!-- /logo -->
            </div>
           
            <ul class="nav navbar-nav navbar-right hidden-xs">
                <li>
                    <a href="javascript:;" class="ripple" data-toggle="dropdown">
{{--                        <img src="images/avatar.jpg" class="header-avatar img-circle" alt="user" title="user">--}}
                        <span>پنل مدیریت</span>
                        <span class="caret"></span>
                    </a>
                    <ul class="dropdown-menu" style="width: 100%;">
                        <li>
                            <a href="javascript:;">تنظیمات</a>
                        </li>
                        <li>
                            <a href="{{route('logout')}}" href="extras-signin.html">خروج</a>
                        </li>
                    </ul>
                </li>
                <li>
                    <a href="javascript:;" class="ripple" data-toggle="layout-chat-open">
                        <i class="icon-user"></i>
                    </a>
                </li>
            </ul>
        </div>
        <!-- /top header -->
        <!-- main area -->
        <div class="main-content">
            @yield('content')
        </div>
        <!-- /main area -->
    </div>
    <!-- /content panel -->
    <!-- bottom footer -->
    <footer class="content-footer">
        <div style="height: inherit;">
            <p style="position: absolute;bottom:0;">
            تمامی حقوق متعلق به سلام می باشد .©
            </p>
        </div>

    </footer>
    <!-- /bottom footer -->
    <!-- chat -->
    <div class="chat-panel">
        <div class="chat-inner">
            <div class="chat-users">
                <div class="chat-group mb0">
                    <div class="chat-group-header h4">
                        Chat
                    </div>
                </div>
                <div class="chat-group">
                    <div class="chat-group-header">
                        Favourites
                    </div>
                    <a href="javascript:;">
                        <span class="status-online"></span>
                        <span>Catherine Moreno</span>
                    </a>
                    <a href="javascript:;">
                        <span class="status-online"></span>
                        <span>Denise Peterson</span>
                    </a>
                    <a href="javascript:;">
                        <span class="status-away"></span>
                        <span>Charles Wilson</span>
                    </a>
                    <a href="javascript:;">
                        <span class="status-away"></span>
                        <span>Melissa Welch</span>
                    </a>
                    <a href="javascript:;">
                        <span class="status-no-disturb"></span>
                        <span>Vincent Peterson</span>
                    </a>
                    <a href="javascript:;">
                        <span class="status-offline"></span>
                        <span>Pamela Wood</span>
                    </a>
                </div>
                <div class="chat-group">
                    <div class="chat-group-header">
                        Online Friends
                    </div>
                    <a href="javascript:;">
                        <span class="status-online"></span>
                        <span>Tammy Carpenter</span>
                    </a>
                    <a href="javascript:;">
                        <span class="status-away"></span>
                        <span>Emma Sullivan</span>
                    </a>
                    <a href="javascript:;">
                        <span class="status-no-disturb"></span>
                        <span>Andrea Brewer</span>
                    </a>
                    <a href="javascript:;">
                        <span class="status-online"></span>
                        <span>حمید شریفی</span>
                    </a>
                </div>
                <div class="chat-group">
                    <div class="chat-group-header">
                        Offline
                    </div>
                    <a href="javascript:;">
                        <span class="status-offline"></span>
                        <span>Denise Peterson</span>
                    </a>
                    <a href="javascript:;">
                        <span class="status-offline"></span>
                        <span>Jose Rivera</span>
                    </a>
                    <a href="javascript:;">
                        <span class="status-offline"></span>
                        <span>Diana Robertson</span>
                    </a>
                    <a href="javascript:;">
                        <span class="status-offline"></span>
                        <span>William Fields</span>
                    </a>
                    <a href="javascript:;">
                        <span class="status-offline"></span>
                        <span>Emily Stanley</span>
                    </a>
                    <a href="javascript:;">
                        <span class="status-offline"></span>
                        <span>Jack Hunt</span>
                    </a>
                    <a href="javascript:;">
                        <span class="status-offline"></span>
                        <span>Sharon Rice</span>
                    </a>
                    <a href="javascript:;">
                        <span class="status-offline"></span>
                        <span>Mary Holland</span>
                    </a>
                    <a href="javascript:;">
                        <span class="status-offline"></span>
                        <span>Diane Hughes</span>
                    </a>
                    <a href="javascript:;">
                        <span class="status-offline"></span>
                        <span>Steven Smith</span>
                    </a>
                    <a href="javascript:;">
                        <span class="status-offline"></span>
                        <span>Emily Henderson</span>
                    </a>
                    <a href="javascript:;">
                        <span class="status-offline"></span>
                        <span>Wayne Kelly</span>
                    </a>
                    <a href="javascript:;">
                        <span class="status-offline"></span>
                        <span>Jane Garcia</span>
                    </a>
                    <a href="javascript:;">
                        <span class="status-offline"></span>
                        <span>Jose Jimenez</span>
                    </a>
                    <a href="javascript:;">
                        <span class="status-offline"></span>
                        <span>Rachel Burton</span>
                    </a>
                    <a href="javascript:;">
                        <span class="status-offline"></span>
                        <span>Samantha Ruiz</span>
                    </a>
                </div>
            </div>
            <div class="chat-conversation">
                <div class="chat-header">
                    <a class="chat-back" href="javascript:;">
                        <i class="icon-arrow-left"></i>
                    </a>
                    <div class="chat-header-title">
                        Charles Wilson
                    </div>
                </div>
                <div class="chat-conversation-content">
                    <p class="text-center text-muted small text-uppercase bold pb15">
                        Yesterday
                    </p>
                    <div class="chat-conversation-user them">
                        <div class="chat-conversation-message">
                            <p>Hey.</p>
                        </div>
                    </div>
                    <div class="chat-conversation-user them">
                        <div class="chat-conversation-message">
                            <p>How are the wife and kids, Taylor?</p>
                        </div>
                    </div>
                    <div class="chat-conversation-user me">
                        <div class="chat-conversation-message">
                            <p>Pretty good, Samuel.</p>
                        </div>
                    </div>
                    <p class="text-center text-muted small text-uppercase bold pb15">
                        Today
                    </p>
                    <div class="chat-conversation-user them">
                        <div class="chat-conversation-message">
                            <p>Curabitur blandit tempus porttitor.</p>
                        </div>
                    </div>
                    <div class="chat-conversation-user me">
                        <div class="chat-conversation-message">
                            <p>Goodnight!</p>
                        </div>
                    </div>
                    <div class="chat-conversation-user them">
                        <div class="chat-conversation-message">
                            <p>Duis mollis, est non commodo luctus, nisi erat porttitor ligula, eget lacinia odio sem
                                nec elit.</p>
                        </div>
                    </div>
                </div>
                <div class="chat-conversation-footer">
                    <button class="chat-input-tool">
                        <i class="fa fa-camera"></i>
                    </button>
                    <div class="chat-input" contenteditable=""></div>
                    <button class="chat-send">
                        <i class="fa fa-paper-plane"></i>
                    </button>
                </div>
            </div>
        </div>
    </div>
    <!-- /chat -->
</div>
<!-- build:js({.tmp,app}) scripts/app.min.js -->
<script src="/admin/scripts/login.js"></script>
<script src="/admin/scripts/helpers/modernizr.js"></script>
<script src="/admin/vendor/jquery/dist/jquery.js"></script>
<script src="/admin/vendor/bootstrap/dist/js/bootstrap.js"></script>
<script src="/admin/vendor/fastclick/lib/fastclick.js"></script>
<script src="/admin/vendor/perfect-scrollbar/js/perfect-scrollbar.jquery.js"></script>
<script src="/admin/scripts/helpers/smartresize.js"></script>
<script src="/admin/scripts/constants.js"></script>
<script src="/admin/scripts/main.js"></script>
<!-- endbuild -->
<!-- page scripts -->
<script src="/admin/vendor/flot/jquery.flot.js"></script>
<script src="/admin/vendor/flot/jquery.flot.resize.js"></script>
<script src="/admin/vendor/flot/jquery.flot.categories.js"></script>
<script src="/admin/vendor/flot/jquery.flot.stack.js"></script>
<script src="/admin/vendor/flot/jquery.flot.time.js"></script>
<script src="/admin/vendor/flot/jquery.flot.pie.js"></script>
<script src="/admin/vendor/flot-spline/js/jquery.flot.spline.js"></script>
<script src="/admin/vendor/flot.orderbars/js/jquery.flot.orderBars.js"></script>
<!-- end page scripts -->
<!-- initialize page scripts -->
<script src="/admin/scripts/helpers/sameheight.js"></script>
<script src="/admin/scripts/ui/dashboard.js"></script>
<script src="/admin/scripts/sweetalert2.min.js"></script>
<!-- end initialize page scripts -->
@yield('script')
</body>

</html>
